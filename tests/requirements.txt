hyif[full] @ git+https://gitlab.com/hylleraasplatform/hylleraas-interfaces.git@0.1.4
mock==4.0.3
pre-commit==2.17.0
pyarrow>=8.0.0
pytest==7.4.0
pytest-cov==3.0.0
pytest-datafiles==2.0
tables==3.8.0
