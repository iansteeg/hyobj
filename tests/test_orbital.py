import numpy as np

from hyobj import Orbital

# import pytest

# def test_orbital_numpy():
#     """Test orbital generation from numpy array."""
#     bas = 'def2-svp'
#     molecule_string = """
#     O 0 0 0
#     H 0 0 1.795239827225189
#     H 1.693194615993441 0 -0.599043184453037"""
#     ct = [
#         -7.47754415e-05,
#         3.51029935e-02,
#         2.49801981e-01,
#         5.42407191e-02,
#         -8.78251832e-01,
#         1.53109527e-01,
#         -6.70383176e-16,
#         -8.99796116e-01,
#         1.88522001e-01,
#         3.95941855e-03,
#         -6.20380660e-16,
#         -2.35209603e-16,
#         -3.00670951e-02,
#         3.28463367e-02,
#         -2.37050010e-01,
#         1.13030757e-16,
#         -1.36155395e-02,
#         5.25669957e-03,
#         -9.98965368e-16,
#         2.94272328e-01,
#         3.04299126e-01,
#         -7.98978651e-03,
#         -2.18049624e-15,
#         6.11238479e-02,
#     ]
#     orb = np.array(ct)
#     orbs = np.array([orb, orb])
#     myorb = Orbital(basis=bas, molecule=molecule_string, orbital=orbs)

#     assert myorb._atoms == ['O', 'H', 'H']
#     assert myorb.basis_set == 'def2-SVP'
#     assert myorb.nbas == 24
#     np.testing.assert_allclose(
#         myorb.orbitals[0, :],
#         ct,
#         rtol=1e-8,
#         atol=1e-8,
#     )
#     myorb.visualize(1)
#     myorb = Orbital(basis=bas, molecule=molecule_string, orbital=orb)
#     with pytest.raises(ValueError):
#         myorb.visualize()

# def test_visualization():
#     """Test random orbital."""
#     mol_string = '''3

#     O 0 0 0
#     H 0 0 1.795239827225189
#     H 1.693194615993441 0 -0.599043184453037'''

#     orbs = np.random.rand(24, 24)
#     myorb = Orbital(basis='def2-svp', molecule=mol_string, orbital=orbs)

#     myorb.visualize_density(np.eye(24), style='isosurface')
#     myorb.visualize_density(np.eye(24), style='contour')
#     myorb.visualize_density(np.eye(24), style='surface')
#     myorb.visualize_orbital(2)


def test_complex():
    """Test complex orbitals."""
    mol_string = '''3

    O 0 0 0
    H 0 0 1.795239827225189
    H 1.693194615993441 0 -0.599043184453037'''

    corbs = np.random.rand(24, 24) + 1j * np.random.rand(24, 24)
    myorb = Orbital(basis='def2-svp', molecule=mol_string, orbital=corbs)

    myorb.visualize_orbital(
        2,
        options={
            'save_figure': 'complex_orb0.png',
            'plot_separate': True,  # plot real and imaginary parts separately
            'iso': [-0.001, complex(0, 0.05), 0.001,
                    complex(0, -0.05)],
            'color': ['red', 'blue', 'green', 'yellow']
        })

    myorb.visualize_orbital(
        2,
        options={
            ''
            'save_figure': 'complex_orb1.pdf'
        })

    myorb.visualize(
        [1 for _ in range(50)],
        options={
            'frame_duration': 1000,
            # 'iso': [-0.001, complex(0, 0.05), 0.001,
            #                     complex(0, -0.05)],
            #              'color': ['red', 'blue', 'green', 'yellow'],
            # 'save_movie': 'tokoc',
            'camera': {
                'rotate': 360,
                'center': [0.2, 0.2, 0],
            }
        })


def test_density():
    """Test density movie."""
    mol_string = '''3

    O 0 0 0
    H 0 0 1.795239827225189
    H 1.693194615993441 0 -0.599043184453037'''

    # orbs = np.random.rand(24, 24)
    orbs = np.eye(24)
    dens = np.zeros((24, 24))
    n0 = 1
    n1 = 5
    n2 = 12
    for i in range(n0, n1):
        dens[i, i] = 1.0
    diff = np.zeros((24, 24))
    for i in range(n1, n2):
        diff[i, i] = 0.05
    for i in range(n0, n1):
        diff[i, i] = -0.01
    n = 50
    d1 = [dens + i * diff for i in range(n)]
    d1 = [dd / np.linalg.norm(dd) for dd in d1]
    myorb = Orbital(basis='def2-svp', molecule=mol_string, orbital=orbs)
    c_end = [1., 1., 0.5]
    c_start = np.array([-4, -4, 0])
    c_start = c_start / np.linalg.norm(c_start) * np.linalg.norm(c_end) * 2

    myorb.visualize_density(d1, style='isosurface', options={'iso': 0.2})

    # myorb.visualize_density(d1,
    #                         style='contour')


def test_density_movie():
    """Test density movie."""
    mol_string = '''3

    O 0 0 0
    H 0 0 1.795239827225189
    H 1.693194615993441 0 -0.599043184453037'''

    # orbs = np.random.rand(24, 24)
    orbs = np.eye(24)
    dens = np.zeros((24, 24))
    n0 = 1
    n1 = 5
    n2 = 12
    for i in range(n0, n1):
        dens[i, i] = 1.0
    diff = np.zeros((24, 24))
    for i in range(n1, n2):
        diff[i, i] = 0.05
    for i in range(n0, n1):
        diff[i, i] = -0.01
    n = 50
    d1 = [dens + i * diff for i in range(n)]
    d1 = [dd / np.linalg.norm(dd) for dd in d1]
    myorb = Orbital(basis='def2-svp', molecule=mol_string, orbital=orbs)
    c_end = [1., 1., 0.5]
    c_start = np.array([-4, -4, 0])
    c_start = c_start / np.linalg.norm(c_start) * np.linalg.norm(c_end) * 2

    myorb.visualize_density(d1,
                            style='contour',
                            options={'save_movie': 'test0'})
    myorb.visualize_density(d1,
                            style='surface',
                            options={
                                'save_movie': 'test1',
                                'camera': {
                                    'start': c_start,
                                    'end': c_end
                                }
                            })
    myorb.visualize_density(d1,
                            style='surface',
                            options={
                                'save_movie': 'test2',
                                'camera': {
                                    'rotate': True
                                }
                            })


if __name__ == '__main__':

    # test_density_movie()
    test_complex()
    # test_density()

#
