# import os
import copy
import os
from pathlib import Path

import numpy as np
import pandas as pd
import pytest

from hyobj import DataSet, MoleculeLike, Units

# loading fixture dir

pytest.FIXTURE_DIR = os.path.join(os.path.dirname(__file__), 'data')

# def test_reader():
#     energies = np.array([-10, -9, np.nan, -8, -7, -6, -5, -4, -3, -2])
#     forces = np.zeros((10, 9))
#     boxes = np.zeros((10, 9))
#     coordinates = np.zeros((10, 9))
#     atoms = ['O', 'H', 'H']

#     ds = DataSet({
#         'energies': energies,
#         'forces': forces,
#         'boxes': boxes,
#         'coordinates': coordinates,
#         'atoms': [atoms for i in range(10)],
#         'iteration': np.arange(10)
#     })
#     assert ds.data.isna().sum()['energies'] == 1
#     ds.data.dropna(subset=['energies'], inplace=True)
#     assert ds.data.isna().sum()['energies'] == 0


# @pytest.fixture(scope='session', params=['water.csv', 'water.json'])
# def get_data_file(request):
#     """Fixture returning molecule filename."""
#     yield os.path.join(pytest.FIXTURE_DIR, request.param)

# def test_dataset_input(get_data_file):
#     """Test pandas interface."""
#     ds = DataSet(get_data_file)
#     np.testing.assert_almost_equal(float(ds.data.iloc[1, 1]), 0.385159)

# def test_explicit_input():
#     """Test explicit input for DataSet generation."""
#     ds = DataSet({'0': [1, 2, 4], '1': ['a', 'b', 'c']})
#     assert ds.data.shape == (3, 2)
#     ds = DataSet(ds.data)
#     assert ds.data.shape == (3, 2)
#     ds = DataSet([[0, 2], [-2, 3], [5, 4]])
#     assert ds.data.shape == (2, 3)
#     ds = DataSet(ds)
#     assert ds.data.shape == (2, 3)
#     with pytest.raises(ValueError):
#         ds = DataSet('tetwet')
#     with pytest.raises(TypeError):
#         ds = DataSet(np)

#     ds = DataSet({'0': [1, 2, 4], '1': ['a', 'b', 'c'],
#                   '2': 'teststr', '3': 0.1}, dimension='multi')
#     assert ds.data.shape == (1, 4)


# def test_reader_npy():
#     """Test .npy reader."""
#     ds1 = DataSet(Path(pytest.FIXTURE_DIR) / 'coord.npy', dimension='multi')
#     print(ds1)
#     assert ds1.data.shape[0] == 72
#     units = Units()
#     ds2 = DataSet(ds1.data, units=units)
#     assert ds2.data.attrs['units'].length[0] == 'bohr'
#     assert ds2.has_units

#     ds3 = DataSet(ds1.data, units=units)
#     ds3.units = Units('real')
#     ref = np.array(ds3['coord'])
#     assert ds3.data.attrs['units'].length[0] == 'angstrom'
#     print(ds3)
#     ds3.change_units(Units('atomic'))
#     # print(ds3['coord'])
#     np.testing.assert_allclose(ref, ds3['coord'] * 0.529177210903)
#     assert ds3.data.attrs['units'].length[0] == 'bohr'
#     ds3.units = 'real'
#     assert ds3.data.attrs['units'].length[0] == 'angstrom'
#     assert ds3.has_units

#     with pytest.raises(ValueError):
#         ds2 + ds3

#    to_add1 = {'coord': ds1.data['coord'].to_numpy(), 'units': Units('metal')}
#   to_add2 = {'coord': ds1.data['coord'].to_numpy(), 'units': Units('atomic')}

#     with pytest.raises(ValueError):
#         ds2.append(to_add1)
#     ds2.append(to_add2)
#     assert ds2.data.shape[0] == 72 * 2


def test_read_write_with_units():
    """Test reading and writing with units."""
    dataset = DataSet(
        Path(pytest.FIXTURE_DIR) / 'coord.npy',
        dimension='multi',
        units=Units('metal'),
    )

    dataset.write_data('test.pkl')
    dataset2 = DataSet('test.pkl')

    assert dataset2.has_units
    assert dataset.hash == dataset2.hash

    for key in dataset.data.attrs['column_units'].keys():
        assert (
            dataset.data.attrs['column_units'][key]
            == dataset2.data.attrs['column_units'][key]
        )

    dataset2 = DataSet('test.pkl', units='atomic')
    assert dataset.hash != dataset2.hash


# assert dataset.hash == dataset2.hash
# assert dataset2.units == Units('metal')


def test_addition():
    """Test addition of two datasets."""
    d1 = DataSet(
        pd.DataFrame({'col0': [1, 2, 4], 'col1': np.array(['a', 'b', 'c'])})
    )
    d2 = DataSet(pd.DataFrame({'col0': [1, 4, 2], 'col2': ['a', 'c', 'b']}))
    d3 = DataSet(
        pd.DataFrame(
            {
                'col0': [1, 2, 4],
                'col2': ['a', 'b', 'c'],
                'col1': np.array(['a', 'b', 'c']),
            }
        )
    )
    d1 += d2
    assert d1.data.equals(d3.data)


def test_reader_xyz():

    ds = DataSet(Path(pytest.FIXTURE_DIR) / 'coord.npy', dimension='1d')
    assert ds.data.shape == (70200, 1)
    ds = DataSet(Path(pytest.FIXTURE_DIR) / 'coord.npy', dimension='2d')
    assert ds.data.shape == (72, 975)
    ds1 = DataSet(Path(pytest.FIXTURE_DIR), dimension='multi', match='coord.*')
    ds2 = DataSet(Path(pytest.FIXTURE_DIR), dimension='1d', match='energy.*')
    ds = ds1 + ds2
    assert ds.data.shape == (72, 2)
    ds2 = ds[0]
    assert ds2.data.shape == (1, 2)

    np.testing.assert_allclose(ds2[0]['energy'], -1087.364633)
    np.testing.assert_allclose(ds2[0, 'energy'], -1087.364633)
    ds2[0, 'energy'] = -137
    np.testing.assert_allclose(ds2[0, 'energy'], -137)


# def test_input_parser_and_duplicates():
#     """Test using custom parser and finding duplicates."""
#     import hyif

#     mycp2k = hyif.CP2K({})

#     ds1 = DataSet(
#         '/Users/tilmann/Documents/work/hylleraas/hyobj/hyobj/dataset/data/sigbj',
#         match=['*.inp'],
#         parser=mycp2k.InputParser.parse,
#         parse=True,
#     )
#     assert ds1.data.shape == (91, 4)
#     ds2 = DataSet(
#         '/Users/tilmann/Documents/work/hylleraas/hyobj/hyobj/dataset/data/sigbj',
#         match=['*.out'],
#         parser=mycp2k.OutputParser.parse,
#     )
#     assert all(
#         Path(ds1.data['input_file'][i]).stem
#         == Path(ds2.data['output_file'][i]).stem
#         for i in range(91)
#     )
#     ds = ds1 + ds2
#     assert ds.data.shape == (91, 12)
#     assert len(ds.get_duplicates(ds.data['input_file'])) == 0
#     assert (
#         len(ds.get_duplicates(ds.data['unit_cell'],
#     rtol=1e-4, atol=1e-4)) == 8
#     )
#     assert (
#         len(ds.get_duplicates(ds.data['unit_cell'],
#    rtol=1e-8, atol=1e-8)) == 0
#     )
#     n = len(ds.data['atoms'])
#     assert len(ds.get_duplicates(ds.data['atoms'])) == n * (n - 1) // 2


# def test_magic():
#     """Test magic methods of DataSet class."""
#     ds = DataSet(Path(pytest.FIXTURE_DIR) / 'coord.npy', dimension='multi')
#     ds2 = ds+ds
#     assert ds2.data.shape[0]//2 == ds.data.shape[0]


def test_parser():
    """Test reader using custom parser."""
    from hyif import CP2K

    mycp2k = CP2K({'check_version': False})
    ds_dir = Path(pytest.FIXTURE_DIR) / '../../hyobj/dataset/cp2k_ex'
    ds_output = DataSet(
        ds_dir, match='*.out', parser=mycp2k.OutputParser, units=mycp2k.units
    )
    ds_input = DataSet(
        ds_dir, match='*.inp', parser=mycp2k.InputParser, units=mycp2k.units
    )
    assert ds_input.units == mycp2k.units
    assert ds_output.units == mycp2k.units
    ds = ds_input + ds_output
    assert ds.units == mycp2k.units

    coord0 = np.array(ds.data['coordinates'][0]).copy()
    ds.change_units(Units('metal'))
    assert ds.units == Units('metal')
    fac = Units('metal').length[1] / Units('atomic').length[1]
    np.testing.assert_almost_equal(fac, 0.529177210903)
    coord0_newunits = np.array(ds.data['coordinates'][0])
    np.testing.assert_allclose(coord0 * fac, coord0_newunits)

    ds_single = DataSet(
        ds_dir,
        match='*1484774336345cc93cb.out',
        parser=mycp2k.OutputParser,
        units=mycp2k.units,
    )
    assert len(ds_single.data) == 1


def test_reader():
    """Test reader using custom parser."""
    # Create a dictionary with data
    n = 10
    data_10 = {
        'energy': np.zeros((n)),
        'forces': np.zeros((n, 9)),
        'boxes': np.zeros((n, 9)),
        'coordinates': np.zeros((n, 9)),
        'atoms': [['H'] * n] * n,
    }

    dataset_10 = DataSet(data_10)
    assert len(dataset_10.data['energy']) == 10

    n = 1
    data_1 = {
        'energy': np.zeros((n)),
        'forces': np.zeros((n, 9)),
        'boxes': np.zeros((n, 9)),
        'coordinates': np.zeros((n, 9)),
        'atoms': [['H'] * 3] * n,
    }

    dataset_1 = DataSet(data_1)
    assert len(dataset_1.data['energy']) == 1

    # Test with lists
    n = 10
    data_10 = {
        'energy': [0.0] * n,
        'forces': [[0.0] * 12] * n,
        'boxes': [[0] * 9] * n,
        'coordinates': [[0.0] * 12] * n,
        'atoms': [['H'] * 4] * n,
    }

    dataset_10 = DataSet(data_10)
    assert len(dataset_10.data['energy']) == 10
    assert isinstance(dataset_10.data['energy'][0], float)

    inhomogenous_dict = {'atoms': [['H'] * 3, ['H'] * 4]}
    dataset = DataSet(inhomogenous_dict)
    assert len(dataset.data['atoms']) == 2

    ds = DataSet(Path(pytest.FIXTURE_DIR) / 'test_nopbc.xyz')
    ds.standardize_columns()

    assert len(ds.data) == 4
    assert 'coordinates' in ds.data.columns
    assert 'energy_dev' in ds.data.columns
    assert 'forces_dev' in ds.data.columns

    ds = DataSet(Path(pytest.FIXTURE_DIR) / 'test_pbc.xyz')
    ds.standardize_columns()
    # Check that it contains cell
    assert 'box' in ds.data.columns


# def test_molecule_conversion():
#     """Test conversion to MoleculeLike."""
#     from hyif import CP2K
#
#     mycp2k = CP2K({'check_version': False})
#     ds_dir = Path(pytest.FIXTURE_DIR) / '../../hyobj/dataset/cp2k_ex'
#     ds_input = DataSet(
#         ds_dir, match='*.inp', parser=mycp2k.InputParser, units=mycp2k.units
#     )
#
#     for system in ds_input.systems:
#         assert isinstance(system, MoleculeLike)
#         # Check units
#         assert system.units == mycp2k.units


def test_hash():
    """Test hash of DataSet."""

    ds = DataSet({'0': [1, 2, 4], '1': ['a', 'b', 'c']})
    ds2 = copy.deepcopy(ds)
    assert ds.hash == ds2.hash

    ds3 = DataSet({'0': [1, 2, 5], '1': ['a', 'b', 'c']})
    assert ds.hash != ds3.hash


def test_standardize_columns():
    mappings = {
        'force': 'forces',
        'energies': 'energy',
        'box': 'boxes',
        'pbc': 'boxes',
        'unit_cells': 'boxes',
        'unit_cell': 'boxes',
        'cell': 'boxes',
        'coordinate': 'coordinates',
        'coord': 'coordinates',
        'position': 'coordinates',
        'positions': 'coordinates',
        'species': 'atoms',
        'species_label': 'atoms',
        'species_labels': 'atoms',
        'atom': 'atoms',
        'virial': 'virials',
    }
    mappings_sign_switch = {'gradient': 'forces', 'gradients': 'forces'}
    shaped_output = False
    shapes = {
        'forces': (-1, 3),
        'boxes': (3, 3),
        'virials': (3, 3),
        'coordinates': (-1, 3),
    }

    # Create a DataSet object with sample data
    data = {
        'pbc': [10, 11, 12, 10, 11, 12, 10, 11, 12],
        'coord': [25, 26, 27],
        'species': [34, 35, 36],
        'virial': [46, 47, 48, 46, 47, 48, 46, 47, 48],
        'gradient': [-1, -2, -3],
        'energy': -4,
    }
    ds = DataSet(data)

    # Test standardize_columns method
    ds.standardize_columns(
        mappings=mappings,
        mappings_sign_switch=mappings_sign_switch,
        shaped_output=shaped_output,
        shapes=shapes,
    )

    # Assert column names have been standardized
    assert 'forces' in ds.data.columns
    assert 'energy' in ds.data.columns
    assert 'boxes' in ds.data.columns
    assert 'coordinates' in ds.data.columns
    assert 'atoms' in ds.data.columns
    assert 'virials' in ds.data.columns

    # Assert column names have been switched with sign change
    assert 'gradient' not in ds.data.columns
    assert 'gradients' not in ds.data.columns
    assert 'forces' in ds.data.columns

    # Assert column values have been reshaped
    ds.convert_columns_to_numpy()
    assert ds.data['forces'].iloc[0].shape == (1, 3)
    assert ds.data['boxes'].iloc[0].shape == (3, 3)
    assert ds.data['virials'].iloc[0].shape == (3, 3)
    assert ds.data['coordinates'].iloc[0].shape == (1, 3)


def test_dataset_init():
    data = {'0': [1, 2, 4], '1': ['a', 'b', 'c']}
    ds = DataSet(data)
    assert isinstance(ds, DataSet)


def test_dataset_has_units():
    data = {'0': [1, 2, 4], '1': ['a', 'b', 'c']}
    ds = DataSet(data)
    assert not ds.has_units


def test_dataset_change_units():
    data = {'0': [1, 2, 4], '1': ['a', 'b', 'c']}
    ds = DataSet(data)
    units = Units('metal')
    ds.change_units(units)
    assert ds.units == units


def test_dataset_check_units():
    data = {'0': [1, 2, 4], '1': ['a', 'b', 'c']}
    ds = DataSet(data)
    column_units = ds.check_units()
    assert column_units == {'0': None, '1': None}


def test_dataset_rename_columns():
    data = {'0': [1, 2, 4], '1': ['a', 'b', 'c']}
    ds = DataSet(data)
    mappings = {'0': 'energy', '1': 'forces'}
    ds.rename_columns(mappings)
    assert ds.data.columns.tolist() == ['energy', 'forces']


def test_dataset_convert_columns_to_lists():
    data = {'0': [np.array([1, 2, 3]), np.array([4, 5, 6])], '1': ['a', 'b']}
    ds = DataSet(data)
    ds.convert_columns_to_lists()
    assert isinstance(ds.data['0'][0], list)
    assert isinstance(ds.data['0'][1], list)


def test_dataset_convert_columns_to_numpy():
    data = {'0': [[1, 2, 3], [4, 5, 6]], '1': ['a', 'b']}
    ds = DataSet(data)
    ds.convert_columns_to_numpy()
    assert isinstance(ds.data['0'][0], np.ndarray)
    assert isinstance(ds.data['0'][1], np.ndarray)


def test_dataset_standardize_columns():
    data = {
        'force': [np.array([1, 2, 3]), np.array([4, 5, 6])],
        'energies': [1, 2],
    }
    ds = DataSet(data)
    ds.standardize_columns()
    assert 'forces' in ds.data.columns
    assert 'energy' in ds.data.columns


def test_dataset_units():
    data = {'0': [1, 2, 4], '1': ['a', 'b', 'c']}
    ds = DataSet(data)
    assert ds.units is None


def test_dataset_hash():
    data = {'0': [1, 2, 4], '1': ['a', 'b', 'c']}
    ds = DataSet(data)
    assert isinstance(ds.hash, str)


def test_dataset_systems():
    data = {'atoms': [['H'] * 3] * 2, 'coordinates': [[1, 2, 3], [4, 5, 6]]}
    ds = DataSet(data)
    systems = ds.systems
    assert isinstance(systems, list)
    assert isinstance(systems[0], MoleculeLike)


def test_writer():
    from hyif import CP2K

    mycp2k = CP2K({'check_version': False})
    ds_dir = Path(pytest.FIXTURE_DIR) / '../../hyobj/dataset/cp2k_ex'
    ds_output = DataSet(
        ds_dir, match='*.out', parser=mycp2k.OutputParser, units=mycp2k.units
    )
    ds_input = DataSet(
        ds_dir, match='*.inp', parser=mycp2k.InputParser, units=mycp2k.units
    )
    ds = ds_input + ds_output

    ds.write_data('test.pkl')
    ds2 = DataSet('test.pkl')
    assert ds.hash == ds2.hash


# ds.write_data('test.h5', key='data')
# ds2 = DataSet('test.h5')
# assert ds.hash == ds2.hash

# ds.write_data('test.parquet')


def test_numpy_interface():
    """Test numpy interface."""
    from hyobj.dataset.numpy.interface import NumpyInterface

    interface = NumpyInterface()

    data_input = np.array([1, 2, 3, 4, 5])
    output = interface.get_data(data_input, {})
    np.testing.assert_array_equal(output, data_input)

    data_input = np.array([1, 2, 3, 4, 5])
    filename = Path('test.npy')
    np.save(filename, data_input)
    output = interface.get_data(filename, {})
    np.testing.assert_array_equal(output, data_input)
    filename.unlink()  # clean up the test file

    expected_shape = (4,)
    data_input = np.array([1, 2, 3, 4])
    assert interface.get_shape(data_input) == expected_shape
    new_shape = (2, 2)
    new_data = interface.rearrange(data_input, expected_shape, new_shape)
    assert new_data.shape == new_shape

    data_input = 'invalid_input'
    with pytest.raises(NotImplementedError):
        interface.get_data(data_input, {})

    data = np.array([1, 2, 3, 4, 5])
    df = interface.to_pandas(data, '1d', 'test')
    expected_df = pd.DataFrame(pd.Series(data.ravel(), name='test'))
    pd.testing.assert_frame_equal(df, expected_df)

    df = interface.to_pandas(data, None, 'test')
    expected_df = pd.DataFrame(pd.Series(data.ravel(), name='test'))
    pd.testing.assert_frame_equal(df, expected_df)

    data = np.array([[1, 2], [3, 4], [5, 6]])
    df = interface.to_pandas(data, '2d', 'test')
    expected_df = pd.DataFrame(data.ravel().reshape(len(data), -1))
    pd.testing.assert_frame_equal(df, expected_df)

    data = np.array([1, 2, 3, 4, 5])
    df = interface.to_pandas(data, 'multi', 'test')
    expected_df = pd.DataFrame({'test': [data.ravel()]})
    pd.testing.assert_frame_equal(df, expected_df)

    data = np.array([1, 2, 3, 4, 5])
    with pytest.raises(NotImplementedError):
        interface.to_pandas(data, '3d', 'test')


def test_pandas_interface():
    """Test pandas interface."""
    from hyobj.dataset.pandas.interface import PandasInterface

    pandas_interface = PandasInterface()

    data = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
    file_formats = ['.csv', '.json', '.h5', '.pkl', '.parquet']

    for file_format in file_formats:
        filename = Path(f'test{file_format}')
        options = {}
        if file_format == '.csv':
            data.to_csv(filename, index=False)
        elif file_format == '.json':
            data.to_json(filename, orient='split')
            options['read_opt'] = {'orient': 'split'}
        elif file_format == '.h5':
            data.to_hdf(filename, 'key')
        elif file_format == '.pkl':
            data.to_pickle(filename)
        elif file_format == '.parquet':
            data.to_parquet(filename)
        result = pandas_interface.get_data(filename, options)
        pd.testing.assert_frame_equal(
            result.reset_index(drop=True), data.reset_index(drop=True)
        )
        filename.unlink()

    filename = Path('test.txt')
    with pytest.raises(TypeError):
        pandas_interface.get_data(filename, {})

    data = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
    result = pandas_interface.get_shape(data)
    assert result == (3, 2)  # 3 rows, 2 columns

    df1 = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
    df2 = pd.DataFrame({'C': [7, 8, 9], 'D': [10, 11, 12]})
    df_list = [df1, df2]
    options = {'align': 'column'}
    result = pandas_interface.get_axis(df_list, options)
    assert result == 1  # Check if the returned axis is 1
    options = {'align': 'row'}
    result = pandas_interface.get_axis(df_list, options)
    assert result == 0  # Check if the returned axis is 0
    df1 = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
    df2 = pd.DataFrame({'C': [7, 8], 'D': [10, 11]})
    df_list = [df1, df2]
    result = pandas_interface.get_axis(df_list)
    assert result == 0  # Check if the returned axis is 0
    df1 = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
    df2 = pd.DataFrame(
        {'C': [7, 8, 9, 10], 'D': [10, 11, 23, 14], 'E': [10, 11, 23, 14]}
    )
    df_list = [df1, df2]
    with pytest.raises(TypeError):
        pandas_interface.get_axis(df_list)

    df1 = pd.DataFrame({'A': [1, 2, 3]})
    df2 = pd.DataFrame({'B': [4, 5, 6], 'C': [7, 8, 9]})
    df_list = [df1, df2]
    result = pandas_interface.get_axis(df_list)
    assert result == 1  # Check if the returned axis is 1

    with pytest.raises(TypeError):
        pandas_interface.join(['test1', 'test2'])
    df1 = pd.DataFrame({'A': [1, 2, 3], 'B': [4, 5, 6]})
    df2 = pandas_interface.join([df1])
    assert df2.equals(df1)
