import os
from typing import List

import numpy as np
import pytest

# from hylleraas.convert import convert
from hyobj import Constants, Molecule

molecule_string_1 = """
   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.8394416921"""

molecule_string_2 = """3

O -0.000000000000 0.033334630400 -0.099999967000
H 0.000000000000 0.050399679500 -1.039441725200
H 0.000000000000 0.016264690100 0.839441692100
"""

molecule_string_3 = """3

   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.8394416921
   H 0. 0.055996795 -1.0394417252
"""

molecule_string_4 = """O
H 1 0.939597
H 1 0.939597 2 177.999702"""

molecule_string_5 = """4

   O -0. .0333346304 -0.099999967
   H 0. 0.0503996795 -1.0394417252
   H 0.0 0.0162646901 0.8394416921"""

molecule_string_6 = """2

  H 0 0 0
  H 0 0 0.529177249
  """


@pytest.fixture(
    scope='session',
    params=[
        molecule_string_1,
        molecule_string_2,
        molecule_string_3,
        molecule_string_4,
    ],
)
def get_mol_string(request):
    """Ficture returning molecule string."""
    yield request.param


@pytest.fixture(
    scope='session', params=['water.xyz', 'ch3cooh.zmat', 'acetate.xyz']
)
def get_mol_file(request):
    """Fixture returning molecule filename."""
    yield os.path.join(pytest.FIXTURE_DIR, request.param)


def test_units():
    mymol = Molecule(molecule_string_6, units='angstrom')
    np.testing.assert_almost_equal(
        mymol.distance(0, 1), Constants.bohr2angstroms, decimal=7
    )
    mymol2 = Molecule('H 0 0 0 \n H 0 0 1')
    np.testing.assert_allclose(
        mymol.coordinates,
        mymol2.coordinates * Constants.bohr2angstroms,
        rtol=1e-7,
        atol=1e-7,
    )
    mymol3 = Molecule(mymol, units='angstrom')
    np.testing.assert_allclose(
        mymol.coordinates, mymol3.coordinates, rtol=1e-7, atol=1e-7
    )


def test_molecule_magic():
    """Test magic methods associated to Molecule."""
    mymol = Molecule(molecule_string_1)
    for atom, coord in mymol:
        if atom == 'O':
            np.testing.assert_allclose(
                coord,
                np.array([-0.0, 0.0333346304, -0.099999967]),
                rtol=1e-7,
                atol=1e-7,
            )
    hatom = Molecule('H 0 0 0')
    h2 = hatom + hatom.translate([0, 0, 0.7])
    np.testing.assert_almost_equal(h2.distance(0, 1), 0.7, decimal=7)
    with pytest.raises(TypeError):
        hatom + 'w'
    h3 = hatom + [hatom.translate([0, 0, 0.7]), hatom.translate([0, 0, -0.7])]
    np.testing.assert_almost_equal(h3.distance(1, 2), 1.4, decimal=7)
    assert h2 in h3
    with pytest.raises(TypeError):
        hatom - [0, 0, 0]
    h1 = (h3 - h2).translate([0, 0, 0.7])

    np.testing.assert_allclose(
        h1.coordinates, np.array([[0, 0, 0]]), rtol=1e-7, atol=1e-7
    )
    assert h1 == hatom
    assert h3[0] == h1
    with pytest.raises(IndexError):
        h2 - h3


def test_attributes():
    """Test Molecule attributes."""
    mymol = Molecule(molecule_string_1)
    assert mymol.atoms == mymol.elements
    mymol.coordinates = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0]])
    np.testing.assert_allclose(mymol.coordinates[2, :], np.array([0, 1, 0]))
    # assert mymol.properties.get('unit') == 'bohr'
    assert mymol.units.length[0] == 'bohr'
    np.testing.assert_allclose(
        mymol.masses, np.array([15.99491461957, 1.00782503223, 1.00782503223])
    )
    assert mymol.atomic_numbers == [8, 1, 1]


def test_bonds():
    """Test bond functionalities."""
    mymol = Molecule(molecule_string_1)
    assert len(mymol.bonds) == 2
    assert mymol.bonds[0][2] == mymol.distance(0, 1)


#     assert mymol.bond_orders == [[0, 1, 1], [1, 0, 0], [1, 0, 0]]


def test_xyz_string():
    """Test generation of xyz-strings."""
    mymol = Molecule(molecule_string_2)
    assert mymol.xyz_string == molecule_string_2


def test_molecule_converter():
    """Test molecule conversion."""
    import daltonproject as dp

    dp_mol = dp.Molecule(atoms=molecule_string_1, charge=-1)
    mymol = Molecule(dp_mol)
    assert mymol.properties['charge'] == -1


def test_transformations():
    """Test molecule transformations."""
    mymol = Molecule('H 0 0 -1 \n H 0 0 1')
    mymol2 = mymol.translate([0, 0, -2])
    np.testing.assert_almost_equal(
        mymol.distance(0, 1), mymol2.distance(0, 1), decimal=7
    )
    np.testing.assert_allclose(mymol[0].coordinates, mymol2[1].coordinates)
    mymol2 = mymol.rotate([[1, 0, 0], [0, 0, 1], [0, -1, 0]])
    np.testing.assert_allclose(
        mymol2.coordinates, [[0.0, -1.0, 0.0], [0.0, 1.0, 0.0]]
    )


@pytest.fixture(
    scope='session',
    params=['water.xyz', 'water.mol', 'water.pdb', 'water.sdf', 'water.zmat'],
)
def get_input_file(request):
    """Fixture returning molecule filename."""
    yield os.path.join(pytest.FIXTURE_DIR, request.param)


def test_constructor(get_input_file):
    """Test generation of Molecule."""
    mymol = Molecule('O')
    assert mymol.num_atoms == 3
    mymol = Molecule('O', optimize_smiles='MMFF')
    np.testing.assert_almost_equal(
        mymol.angle(1, 0, 2), 103.978051913717, decimal=2
    )
    mymol = Molecule(molecule_string_4)
    np.testing.assert_almost_equal(mymol.angle(1, 0, 2), 177.999702, decimal=2)

    mymol = Molecule([['H', 'H'], [[0, 0, 0], [0, 0, 1]]])
    np.testing.assert_almost_equal(mymol.distance(0, 1), 1.0, decimal=2)
    mymol = Molecule([['H', 0, 0, 0], ['H', 0, 0, 1]])
    np.testing.assert_almost_equal(mymol.distance(0, 1), 1.0, decimal=2)
    mymol = Molecule(['H', 0, 0, 0])
    assert mymol.num_atoms == 1

    mymol = Molecule(get_input_file)
    assert mymol.num_atoms == 3

    with pytest.raises(TypeError):
        Molecule()


def test_get_ase_object():
    """Test distance calculations."""
    h2o = Molecule(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [[0, 0, 0], [0, 0, 1], [0, 1, 0]],
        }
    )
    distances = h2o.get_ase_object().get_all_distances(mic=True)[
        np.triu_indices(len(h2o.atoms), k=1)
    ]
    assert len(distances) == 3
    np.testing.assert_allclose(distances, [1.0, 1.0, 1.41421356])

    distances2 = h2o.get_atomic_distances()
    np.testing.assert_allclose(distances, distances2)


def test_internal_coordinates():
    """Test internal coordinates."""
    mymol = Molecule(
        [['O', 'H', 'H'], np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0]])]
    )
    np.testing.assert_allclose(
        mymol.internal_coordinates['values'],
        np.array(
            [
                0.52917721,
                0.52917721,
                0.74836959,
                1.57079633,
                0.78539816,
                0.78539816,
                0.0,
                0.1763924,
                0.1763924,
                0.0,
                0.0,
                0.0,
            ]
        ),
    )
    np.testing.assert_allclose(
        mymol.get_distances(),
        [[0, 1, 1.0], [0, 2, 1.0], [1, 2, 1.4142135623730951]],
    )
    mymol2 = mymol.constrain(['distance', 0, 1, 1.1])

    np.testing.assert_almost_equal(mymol2.distance(0, 1), 1.1, decimal=5)
    assert (
        '<bound method MoleculeReaders.get_molecule_from_dict'
        in mymol2.__repr__()
    )
    mymol = Molecule('CC(=O)[O-]')

    # Check that the C-C-O-O dihedral is close to 180.0 degrees.
    # The dihedral is periodic in [-180, 180)
    assert np.any(
        np.isclose(
            [mymol.dihedral(0, 1, 2, 3), mymol.dihedral(0, 1, 2, 3)],
            [180.0, -180.0],
            atol=1e-2,
        )
    )


def test_pdb_reader():
    with open('water.pdb', 'w') as fp:
        fp.write(
            """CRYST1   10.000   10.000   10.000  90.00  90.00  90.00"""
            + """P 1           1
ATOM      1  O   HOH     1       0.000   0.000   0.000  1.00  0.00           O
ATOM      2  H1  HOH     1       0.000   0.000   1.000  1.00  0.00           H
ATOM      3  H2  HOH     1       0.000   1.000   0.000  1.00  0.00           H
END
"""
        )
    mymol = Molecule('water.pdb')
    assert 'unit_cell' in mymol.properties.keys()

    with open('water.xyz', 'w') as fp:
        fp.write(
            """3
Lattice="10.0 0.0 0.0 0.0 10.0 0.0 0.0 0.0 10.0" """
            + """Properties=species:S:1:pos:R:3 pbc="T T T"
O 0.0 0.0 0.0
H 0.0 0.0 1.0
H 0.0 1.0 0.0
"""
        )
    mymol = Molecule('water.xyz')
    assert 'unit_cell' in mymol.properties.keys()

    # make a poscar file
    with open('POSCAR', 'w') as fp:
        with open('POSCAR', 'w') as fp:
            fp.write(
                """water
1.0
10.0   0.0   0.0
0.0  10.0   0.0
0.0   0.0  10.0
O   H
1   2
Direct
    0.0000000000000000  0.0000000000000000  0.0000000000000000
    0.0000000000000000  0.0000000000000000  0.9572000000000000
    0.0000000000000000  0.9572000000000000  0.0000000000000000"""
            )

    mymol = Molecule('POSCAR')
    assert 'unit_cell' in mymol.properties.keys()


# def test_molecule_input(get_mol_string, get_mol_file):
#     """Test reader in hygo.Molecule class."""
#     mymol = Molecule(get_mol_file)
#     assert (mymol.atoms[0:3] == ['O', 'H', 'H']
#             or mymol.atoms[0:3] == ['C', 'O', 'O']
#             or mymol.atoms[0:3] == ['C', 'C', 'H'])
#     if mymol.num_atoms == 8:
#         np.testing.assert_almost_equal(mymol.dihedral(0, 1, 2, 5), 0,
# decimal=7)

# assert np.abs(mymol.dihedral(0, 1, 2, 5)) < 1e-4
# mymol.zmat = convert(mymol, 'xyz', 'zmat')
# assert np.abs(mymol.zmat[3][6]) <1e-4

# mymol = Molecule(get_mol_string)
# mymol = Molecule(mymol)
# assert mymol.num_atoms == 3
# assert mymol.elements[0] == 'O'
# assert mymol.atomic_number[1] == 1
# assert np.abs(mymol.angle(0, 1, 2)) < 1e-3
# mymol.set_coordinates(mymol.coordinates)
# assert np.abs(mymol.angle(0, 1, 2)) < 1e-3
# assert np.abs(mymol.distance(0, 1) - 0.9395967395381345) < 1e-4

# distmat = mymol.get_distances(0.0, 0.9)
# print(distmat)
# assert abs(distmat[0][2] - 0.49721318200234255) < 1e-4
# distmat = mymol.get_distances(0.0, 0.2)
# assert distmat == []
# assert abs(distmat[0][0])<1e-4

# def test_molecule_routines():
#     """Test molecule routines."""
#     mymol0 = Molecule(['H', 0, 1, -100])
#     assert mymol0.atoms[0] == 'H'
#     mymol1 = Molecule('H 0 0 0 \n U -1 -1 -5.')
#     mymol2 = Molecule('H 1 1 1 \n O 2 2 2')
#     mymol3 = mymol1 + mymol2
#     assert mymol2 in mymol3
#     assert mymol3.num_atoms == 4
#     mymol4 = mymol1 + [mymol1, mymol2]
#     assert mymol4.num_atoms == 6
#     mymol5 = mymol4 - mymol1
#     assert mymol5.num_atoms == 4
#     mymol5 = mymol4 - [mymol1, mymol2]
#     assert mymol5 == mymol1
#     mymol = Molecule('Hy 0 0 0')
#     mymol2 = Molecule('H 0 0 0')
#     mymol3 = Molecule('H 0 0 0.00000001')
#     assert mymol.num_atoms == 1
#     assert not mymol == 1.0
#     assert not mymol == mymol4
#     assert not mymol == mymol2
#     assert mymol2 == mymol3
#     assert mymol2.masses[0] == 1.00782503223
#     mymol = Molecule.find_molecule_in_namespace({
#         'aamol': mymol2,
#         'abmol': mymol
#     })
#     assert mymol.atoms[0] == 'H'
#     mymol = Molecule.find_molecule_in_namespace({})
#     assert mymol.atoms[0] == 'Hy'
#     assert not mymol2.atom_in_molecule(mymol)
#     # mymol = Molecule(os.path.join(pytest.FIXTURE_DIR, 'dummy.dummy'))
#     mymol = Molecule('H\nH 1 100.', 'zmat')
#     assert mymol.distance(0, 1) == 100.0
#     mymol = Molecule('H 0 0 0 \n H 100 0 0 ', 'xyz')
#     assert mymol.distance(0, 1) == 100.0
#     mymol = Molecule([
#         'C',
#         ['O', 1, 1.216],
#         ['O', 1, 1.363, 2, 122.481],
#         ['H', 3, 0.983, 1, 105.275, 2, -0.0],
#     ])
#     assert mymol.distance(0, 1) == 1.216
#     mymol = Molecule([['H', 0, 0, 0], ['H', 0, 0, 300]])
#     assert mymol.distance(0, 1) == 300
#     mymol = Molecule({
#         'atoms': ['H', 'H'],
#         'coordinates': [[0, 0, 0], [0, -100, 0]]
#     })
#     assert mymol.distance(0, 1) == 100
#     mymol2 = mymol.translate([0, 100, 0])
#     assert np.allclose(mymol2.coordinates[1], [0, 0, 0])
#     mymol2 = mymol.rotate([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
#     assert mymol2 == mymol
#     mymol = Molecule(['C', ['H', 1, 1.216], ['O', 1, 1.363, 2, 122.481]])
#     assert mymol.distance(0, 1) == 1.216

# # @pytest.mark.skipif(os.environ['USER'] != 'tilmann', reason='This test
# # requires an external file...')
# # def test_smiles(get_mol_file):
# #     mymol = Molecule(get_mol_file)
# #     if mymol.num_atoms ==7:
# #         smiles_options={'path_to_executable': '/Users/tilmann/Documents/
# # work/hylleraas/xyz2mol/', 'charge': '-1'}
# #         smiles_check = convert(mymol,'xyz', 'SMILES', smiles_options)
# #         assert smiles_check == '[H]C([H])([H])C(=O)[O-]'

# @pytest.fixture(
#     scope='session',
#     params=[
#         'dummy',
#     ],
# )
# def get_mol_file_exception(request):
#     """Fixture returning molecule filename."""
#     yield os.path.join(pytest.FIXTURE_DIR, request.param)

# @pytest.fixture(scope='session', params=[molecule_string_5])
# def get_mol_string_exception(request):
#     """Ficture returning molecule string."""
#     yield request.param

# def test_molecule_input_exception(get_mol_string, get_mol_string_exception,
#                                   get_mol_file_exception):
#     """Test Exception in reader in hygo.Molecule class."""
#     with pytest.raises(Exception):
#         Molecule('4')
#     with pytest.raises(Exception):
#         Molecule('H 0 0')
#     with pytest.raises(Exception):
#         Molecule(4)
#     with pytest.raises(Exception):
#         Molecule(get_mol_file_exception)
#     with pytest.raises(Exception):
#         Molecule(get_mol_string_exception)

#     mymol = Molecule(get_mol_string)
#     with pytest.raises(Exception):
#         mymol.distance(0, 5)
#     with pytest.raises(Exception):
#         mymol.angle(-1, 0, 5)
#     with pytest.raises(Exception):
#         mymol.dihedral(-1, -2, 0, 2)
#     with pytest.raises(Exception):
#         mymol + 'r'
#     with pytest.raises(Exception):
#         mymol + [mymol, 1.0]
#     with pytest.raises(Exception):
#         mymol - [mymol, mymol]
#     with pytest.raises(Exception):
#         mymol.find_atom_in_molecule(1.0)
#     with pytest.raises(Exception):
#         mymol.atom_in_molecule(1.0)

# def test_molecule_input_routines_exception():
#     """Test exceptions in input routines."""
#     with pytest.raises(Exception):
#         Molecule(os.path.join(pytest.FIXTURE_DIR, 'dummy.dummy2'))
#     with pytest.raises(Exception):
#         Molecule('H 0 0 0', 'something')
#     with pytest.raises(Exception):
#         Molecule({})
#     with pytest.raises(Exception):
#         Molecule({'atoms': ['h']})
#     with pytest.raises(Exception):
#         Molecule([
#             'C',
#             [1, 1, 1.216],
#             ['O', 1, 1.363, 2, 122.481],
#             ['H', 3, 0.983, 1, 105.275, 2, -0.0],
#         ])


def test_molecule_subscripting():
    """Test molecule subscripting and len(molecule)."""
    atoms: List[str] = [
        'N',
        'C',
        'C',
        'O',
        'C',
        'H',
        'H',
        'C',
        'H',
        'H',
        'C',
        'H',
        'H',
        'N',
        'H',
        'C',
        'N',
        'H',
        'H',
        'N',
        'H',
        'H',
        'H',
        'H',
        'H',
        'O',
        'H',
    ]

    coordinates: np.ndarray = np.array(
        [
            [0.00000, 0.00000, 0.00000],
            [1.46003, 0.00000, 0.00000],
            [1.89246, 0.00000, 1.47758],
            [1.08851, 0.00000, 2.40868],
            [1.99928, 1.22260, -0.75734],
            [1.83522, 2.13305, -0.16618],
            [1.43216, 1.35445, -1.68744],
            [3.48197, 1.10773, -1.11128],
            [3.68029, 0.14363, -1.59564],
            [4.09185, 1.13805, -0.20222],
            [3.92335, 2.23414, -2.04011],
            [3.76129, 3.21207, -1.57550],
            [3.36392, 2.19559, -2.97934],
            [5.35577, 2.08179, -2.29489],
            [5.95375, 1.87779, -1.49810],
            [5.95619, 2.21302, -3.50437],
            [7.21720, 2.10728, -3.82522],
            [8.01488, 2.12510, -4.47472],
            [7.91576, 1.88612, -3.10303],
            [5.44962, 2.49036, -4.63398],
            [4.46673, 2.65941, -4.74610],
            [6.07406, 2.53999, -5.42437],
            [-0.46460, 0.26925, 0.86387],
            [1.79266, -0.93155, -0.47311],
            [-0.50800, 0.07669, -0.87653],
            [3.21949, -0.00000, 1.73093],
            [3.91485, -0.00000, 0.92559],
        ],
        dtype=np.float64,
    )
    arginine: Molecule = Molecule({'atoms': atoms, 'coordinates': coordinates})

    N: Molecule = arginine[0]  # noqa: N806
    assert len(N._atoms) == 1
    assert len(N) == 1
    assert N._atoms == ['N']
    assert np.allclose(
        N._coordinates, [np.zeros(shape=(1, 3), dtype=np.float64)]
    )

    for key in ((0, 22, 24), [0, 22, 24]):
        amine: Molecule = arginine[key]
        assert len(amine._atoms) == 3
        assert len(amine) == 3
        assert amine._atoms == ['N', 'H', 'H']
        assert np.allclose(
            amine._coordinates,
            np.array(
                [
                    [0.0, 0.0, 0.0],
                    [-0.46460, 0.26925, 0.86387],
                    [-0.50800, 0.07669, -0.87653],
                ],
                dtype=np.float64,
            ),
        )

    carboxyl: Molecule = arginine[2:4] + arginine[25:27]
    assert len(carboxyl._atoms) == 4
    assert len(carboxyl) == 4
    assert carboxyl._atoms == ['C', 'O', 'O', 'H']
    assert np.allclose(
        carboxyl._coordinates,
        np.array(
            [
                [1.89246, 0.00000, 1.47758],
                [1.08851, 0.00000, 2.40868],
                [3.21949, -0.00000, 1.73093],
                [3.91485, -0.00000, 0.92559],
            ],
            dtype=np.float64,
        ),
    )

    arginine_2: Molecule = arginine[:]
    assert arginine_2 == arginine
    assert len(arginine_2._atoms) == 27
    assert len(arginine_2) == 27
    assert np.allclose(arginine._coordinates, arginine_2._coordinates)

    arginine_3: Molecule = arginine[...]
    assert arginine_3 == arginine
    assert len(arginine_3._atoms) == 27
    assert len(arginine_3) == 27
    assert np.allclose(arginine._coordinates, arginine_3._coordinates)


def test_molecule_subscripting_list():
    """Test molecule subscripting for list type molecule._coordinates."""
    methylamine: Molecule = Molecule(
        {
            'atoms': ['C', 'N', 'H', 'H', 'H', 'H', 'H'],
            'coordinates': [
                [-0.566086, 0.0045240, 0.0033410],
                [0.8343650, 0.1145660, 0.2757660],
                [-1.069517, -0.369985, 0.9330980],
                [-0.739157, -0.734367, -0.820246],
                [-1.057700, 0.9404720, -0.287242],
                [1.2781270, -0.825805, 0.1141090],
                [1.3199690, 0.8705960, -0.218826],
            ],
        }
    )

    methyl: Molecule = methylamine[0, 2, 3, 4]
    assert len(methyl._atoms) == 4
    assert len(methyl) == 4
    assert np.allclose(
        np.array(methyl._coordinates),
        np.array(
            [
                [-0.566086, 0.0045240, 0.0033410],
                [-1.069517, -0.369985, 0.9330980],
                [-0.739157, -0.734367, -0.820246],
                [-1.057700, 0.9404720, -0.287242],
            ],
            dtype=np.float64,
        ),
    )

    amine: Molecule = methylamine[1, 5, 6]
    assert len(amine._atoms) == 3
    assert len(amine) == 3
    assert np.allclose(
        np.array(amine._coordinates),
        np.array(
            [
                [0.8343650, 0.1145660, 0.2757660],
                [1.2781270, -0.825805, 0.1141090],
                [1.3199690, 0.8705960, -0.218826],
            ],
            dtype=np.float64,
        ),
    )

    p_aminophenol: Molecule = Molecule(
        {
            'atoms': [
                'O',
                'N',
                'C',
                'C',
                'C',
                'C',
                'C',
                'C',
                'H',
                'H',
                'H',
                'H',
                'H',
                'H',
                'H',
            ],
            'coordinates': [
                [-2.7620, -0.0002, 0.00000],
                [2.79910, -0.0001, -0.0002],
                [1.38870, 0.00020, 0.00010],
                [0.69150, -1.2079, 0.00020],
                [0.69110, 1.20820, 0.00010],
                [-1.4010, -0.0001, -0.0001],
                [-0.7034, -1.2080, -0.0001],
                [-0.7038, 1.20790, 0.00000],
                [1.22360, -2.1557, 0.00020],
                [1.22290, 2.15610, 0.00000],
                [-1.2349, -2.1559, -0.0001],
                [-1.2417, 2.15210, 0.00000],
                [3.30500, -0.8758, 0.00040],
                [3.30550, 0.87530, 0.00030],
                [-3.0771, -0.9205, 0.00000],
            ],
        }
    )
    benzene: Molecule = p_aminophenol[2, 3, 4, 5, 6, 7]
    assert np.allclose(
        np.array(benzene._coordinates),
        np.array(
            [
                [1.38870, 0.00020, 0.00010],
                [0.69150, -1.2079, 0.00020],
                [0.69110, 1.20820, 0.00010],
                [-1.4010, -0.0001, -0.0001],
                [-0.7034, -1.2080, -0.0001],
                [-0.7038, 1.20790, 0.00000],
            ],
            dtype=np.float64,
        ),
    )
