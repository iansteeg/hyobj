# from hyobj import Molecule, MoleculeLike, PeriodicSystem

# mymol = Molecule('O')
# ps = PeriodicSystem(mymol, pbc=[[10, 0, 0], [0, 11, 0], [0, 0, 12]])
# print(ps)

# # See see https://github.com/python/mypy/issues/9160.
# ps.fractional_coordinates = [[0, 0, 0], [0.1, 0, 0], [-0.1, 0.1,
#                                                       0]]  # type: ignore
# print(ps)
# ps.coordinates = [[0, 0, 0], [0, 0, 1], [0, 1, -1]]  # type: ignore
# print(ps)
# ps.pbc = [[5, 0, 0], [0, 5, 0], [0, 0, 5]]  # type: ignore
# print(ps)

# print(type(ps))
# print(isinstance(ps, MoleculeLike))
# print(hasattr(ps, 'pbc'))

# assert isinstance(ps, MoleculeLike)
# assert not isinstance(ps, Molecule)
# assert isinstance(ps, PeriodicSystem)


# Compare this snippet from tests/test_periodicsystem.py:
import numpy as np
import pytest

from hyobj import Molecule, PeriodicSystem


def test_periodicsystem():
    """Test PeriodicSystem."""
    mymol = Molecule('O')
    ps = PeriodicSystem(mymol, pbc=[[10, 0, 0], [0, 11, 0], [0, 0, 12]])
    assert ps.pbc[0][0] == 10
    assert ps.pbc[1][1] == 11
    assert ps.pbc[2][2] == 12
    assert ps.pbc[0][1] == 0
    assert ps.pbc[0][2] == 0

    assert np.allclose(
        ps.masses, [15.99491461957, 1.00782503223, 1.00782503223]
    )

    ps.fractional_coordinates = [[0, 0, 0], [0.1, 0, 0], [-0.1, 0.1, 0]]
    assert np.allclose(ps.coordinates, [[0, 0, 0], [1, 0, 0], [9, 1.1, 0]])

    ps.coordinates = [[0, 0, 0], [0, 0, 1], [0, 1, -1]]
    assert np.allclose(
        ps.fractional_coordinates,
        [[0, 0, 0], [0, 0, 1 / 12], [0, 1 / 11, 11 / 12]],
    )

    with pytest.raises(ValueError):
        ps.pbc = [[5, 0, 0], [0, 5, 0], [0, 0, 5], [0, 0, 5]]

    with pytest.raises(ValueError):
        ps.pbc = [[5, 0, 0], [0, 5, 0], [0, 0, 5], [0, 0, 5]]

    mymol2 = {
        'atoms': ['O', 'H', 'H'],
        'coordinates': [[0, 0, 0], [0, 0, 1], [0, 1, -1]],
        'pbc': [[5, 0, 0], [0, 5, 0], [0, 0, 5]],
    }
    ps2 = PeriodicSystem(mymol2)
    assert ps2.pbc[0][0] == 5


def test_get_ase_object():
    """Test get_ase_object."""
    h2o = PeriodicSystem(
        {
            'atoms': ['O', 'H', 'H'],
            'coordinates': [[0, 0, 0], [0, 1, 0], [0, 0, 9]],
            'pbc': [[10, 0, 0], [0, 10, 0], [0, 0, 10]],
        }
    )
    ase_obj = h2o.get_ase_object()
    distances = ase_obj.get_all_distances(mic=True)[
        np.triu_indices(len(h2o.atoms), k=1)
    ]
    assert np.allclose(distances, [1, 1, 1.41421356])

    distances2 = h2o.get_atomic_distances()
    assert np.allclose(distances, distances2)


def test_reader():
    """Test reading of pdb file."""
    with open('water.pdb', 'w') as fp:
        fp.write(
            """
CRYST1   10.000   10.000   10.000  90.00  90.00  90.00
ATOM      1  O   HOH     1       0.000   0.000   0.000  1.00  0.00           O
ATOM      2  H1  HOH     1       0.000   0.000   1.000  1.00  0.00           H
ATOM      3  H2  HOH     1       0.000   1.000   0.000  1.00  0.00           H
END
"""
        )
    mymol = PeriodicSystem('water.pdb')
    assert 'unit_cell' in mymol.properties.keys()
    assert 'residue_name' in mymol.properties.keys()
    assert 'residue_idx' in mymol.properties.keys()
