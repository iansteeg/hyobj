import numpy as np

from hyobj import Basis, Molecule, Orbital

basis = 'sto-3g'
C = """C 0.0 0.0 -0.3
"""

mymol = Molecule(C)

mybas = Basis(basis, atoms=mymol.atoms)
assert mybas.name == 'STO-3G'

# print(mybas)
# print(norbs)
# print(len(orbs))
# print(mybas.gtos_spherical, len(mybas.gtos_spherical))
# print(mybas.gtos_cartesian, len(mybas.gtos_cartesian))
# n = 5
# mos = np.eye(n)

# h2_orbitals = Orbital(basis=mybas, molecule=mymol, orbital=mos)
# print(h2_orbitals)
# import hylleraas as hsp
# import numpy as np

basis = 'sto-3g'
C = """C 0.0 0.0 -0.3
"""

mol = Molecule(C)
n = 5
mos = np.eye(n)

h2_orbitals = Orbital(basis=basis, molecule=mol, orbital=mos)
