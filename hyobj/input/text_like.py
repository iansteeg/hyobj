

def wrap_text(func):
    """Decorate for running a function with list input."""
    def wrapper(self, *args, **kwargs):
        """Wrap forr running a function with list input."""
        input_list = self.input.splitlines()
        pos = kwargs.pop('pos', len(input_list))
        if pos > len(input_list) or pos < 0:
            raise ValueError(f'expected 0 <= pos <= {len(input_list)}')
        updated_list = func(self, *args, input_list=input_list, pos=pos,
                            **kwargs)
        self.input = '\n'.join(updated_list)
        return
    return wrapper


class TextLikeInput:
    """Text-like input file."""

    def __init__(self, input: str):
        self.input_str = input

    # @singledispatchmethod
    # @wrap_text
    # def add_kw(self, input_list, kw, pos: Optional[int] = None) -> list:
    #     """Add keyword to input file."""
    #     pass

    # @add_kw.register(list)
    # @wrap_text
    # def _(self, kw, pos: Optional[int] = None,
    #       input_list: Optional[list] = []):
    #     input_list.insert(pos, ' '.join(kw))
    #     return input_list

    # @add_kw.register(str)
    # @wrap_text
    # def _(self, kw, pos: Optional[int] = None,
    #       input_list: Optional[list] = []):
    #     input_list.insert(pos, ' '.join(kw))
    #     return input_list

    # @add_kw.register(dict)
    # @wrap_text
    # def _(self, kw, pos: Optional[int] = None,
    #       input_list: Optional[list] = []):
    #     i = 0
    #     for k, v in kw.items():
    #         input_list.insert(i + pos, ' '.join([str(k), str(v)]))
    #         i += 1
    #     return input_list

    # @add_kw.register(str)
    # def _(self, kw, pos: Optional[int] = None):
    #     input_list = self.input.splitlines()
    #     if pos is None:
    #         pos = len(input_list)
    #     input_list.insert(pos, kw)
    #     self.input = '\n'.join(input_list)

    # @singledispatchmethod
    # def del_kw(self, kw):
    #     pass

    # @del_kw.register(int)
    # def _(self, kw):
    #     input_list = self.input.splitlines()
    #     del input_list[kw]
    #     self.input = '\n'.join(input_list)

    #     lst = LammpsInput._dict_to_list(self.input)
    #     if pos is None:
    #         pos = len(lst)
    #     if abs(pos) > len(lst):
    #         raise ValueError('invalid list position')
    #     if pos < 0:
    #         pos += len(lst)
    #     _ = lst.pop(pos)
    #     self.input = LammpsInput(dict(lst))
    #     return None

    # def replace_kw(self,
    #                k: str,
    #                v: Union[str, list, float, dict],
    #                pos: Optional[int] = 0):
    #     """Replace kw in lammps input.

    #     Parameter
    #     ---------
    #     k : str
    #         keyword
    #     v: Union[str, list, float, dict]
    #         value
    #     pos : int, optional
    #         position, defaults to 0 (first entry found)
    #     """
    #     lst = LammpsInput._dict_to_list(self.input)
    #     ifound = 0
    #     for i in len(lst):
    #         kl, vl = lst[i]
    #         if kl == k:
    #             if ifound == pos:
    #                 lst[i] = (k, v)
    #                 return LammpsInput(dict(lst))
    #             ifound += 1
    #     self.input = LammpsInput(dict(lst))
    #     return None
