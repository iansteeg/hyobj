import numpy as np
from qcelemental import PhysicalConstantsContext

from .abc import FileReader
from .utils import zmat2xyz

FILE_READERS: dict = {
    # '.xyz': 'XYZReader',
    '.mol': 'MOLReader',
    '.sdf': 'MOLReader',
    '.zmat': 'ZMATReader',
    '.pdb': 'AseReader',
    '.xyz': 'AseReader',
    '.ase': 'AseReader',
}

constants = PhysicalConstantsContext('CODATA2018')
bohr2angstrom = constants.bohr2angstroms


class MoleculeReaders:
    """Routines for reading molecular structure from various input formats."""

    @classmethod
    def get_molecule_from_string_smiles(cls, molecule: str) -> dict:
        """Construct molecule from smiles-string using rdkit.

        Parameters
        ----------
        molecule : str
            smiles-string

        Returns
        -------
        dict
            atoms and coordinates (ETKB2 optimized)

        """
        try:
            from rdkit import Chem
            from rdkit.Chem import AllChem  # noqa: F401
        except ImportError:
            raise ImportError('could not find package rdkit')

        mol = Chem.MolFromSmiles(molecule)
        mol = Chem.AddHs(mol)
        Chem.AllChem.EmbedMolecule(mol, randomSeed=0)
        # Chem.AllChem.MMFFOptimizeMolecule(mol)
        lines = Chem.AllChem.MolToXYZBlock(mol).split('\n')
        num_atoms = int(lines[0])
        atoms = []
        coordinates = []
        for j in range(2, 2 + num_atoms):
            atom, x, y, z = lines[j].split()
            atoms.append(atom)
            coordinates.append([float(x), float(y), float(z)])

        return {
            'atoms': atoms,
            'coordinates': np.array(coordinates).astype(np.float64),
            'units': 'angstrom',
        }

    @classmethod
    def get_molecule_from_string_smiles_mmff(cls, molecule: str) -> dict:
        """Construct molecule from smiles-string using rdkit.

        Parameters
        ----------
        molecule : str
            smiles-string

        Returns
        -------
        dict:
            of atoms and coordinates (MMFF optimized)

        """
        try:
            from rdkit import Chem
            from rdkit.Chem import AllChem  # noqa: F401
        except ImportError:
            raise ImportError('could not find package rdkit')

        mol = Chem.MolFromSmiles(molecule)
        mol = Chem.AddHs(mol)
        Chem.AllChem.EmbedMolecule(mol, randomSeed=0)
        Chem.AllChem.MMFFOptimizeMolecule(mol)
        lines = Chem.AllChem.MolToXYZBlock(mol).split('\n')
        num_atoms = int(lines[0])
        atoms = []
        coordinates = []
        for j in range(2, 2 + num_atoms):
            atom, x, y, z = lines[j].split()
            atoms.append(atom)
            coordinates.append([float(x), float(y), float(z)])

        return {
            'atoms': atoms,
            'coordinates': np.array(coordinates).astype(np.float64),
            'units': 'angstrom',
        }

    @classmethod
    def get_molecule_from_string_xyz(cls, molecule: str) -> dict:
        """Construct molecule from xyz-string.

        Parameters
        ----------
        molecule : str
            xyz-string

        Returns
        -------
        dict
            atoms and coordinates

        """
        lines: list = list(molecule.strip().split('\n'))
        if lines[0].isdigit():
            num_atoms = int(lines.pop(0))
            # remove comment line too
            lines.pop(0)
            if len(lines) < num_atoms:
                raise ValueError(
                    f'Missing input of {len(lines)-num_atoms} atoms!'
                )
            # elif len(lines) > num_atoms:
            #     warnings.warn(
            #       f'only {num_atoms} out of {len(lines)} coordinates read.')
        else:
            num_atoms = len(lines)

        atoms: list = []
        coordinates: list = np.zeros((num_atoms, 3))
        atom: str = ''
        xyz: list = []
        for i, line in enumerate(lines):
            if i == num_atoms:
                break
            line = list(filter(None, line.strip().split(' ')))
            atom, *xyz = line
            # if len(xyz) != 3:
            #     raise GenericError('only xyz supported')
            atoms.append(atom)
            coordinates[i] = np.array(list(map(float, xyz)), dtype=float)
        return {
            'atoms': atoms,
            'coordinates': np.array(coordinates).astype(np.float64),
        }

    @classmethod
    def get_molecule_from_string_zmat(cls, molecule: str) -> dict:
        """Construct molecule from zmat-string.

        Parameters
        ----------
        molecule : str
            zmat-string

        Returns
        -------
        dict
            list of atoms and coordinates

        """
        lines = list(molecule.strip().split('\n'))
        zmat = [x.split() for x in lines if x != []]
        # zmat = []
        # for line in lines2:
        #     if line.split() == []:
        #         continue
        #     zmat.append(line.split())
        # if not isinstance(zmat[0][0], str):
        #     raise GenericError(f'Inputstring {molecule} not in zmat format!')
        atoms, coordinates = zmat2xyz(zmat)
        return {'atoms': atoms, 'coordinates': coordinates}

    @classmethod
    def get_molecule_from_list_xyz_0(cls, molecule: list) -> dict:
        """Construct molecule from list.

        Parameters
        ----------
        molecule : list
            list containing atoms and coordinates in format [[a1,a2,..],
            [[x1,y1,z1],[x2,...]]]

        Returns
        -------
        dict
            atoms, coordinates, properties

        """
        if len(molecule) < 2:
            raise ValueError(f'Could not read molecule from input {molecule}')

        atoms, coordinates = molecule[0:2]
        if len(molecule) > 2:
            properties = list(molecule[2:])
        else:
            properties = None

        return {
            'atoms': atoms,
            'coordinates': coordinates,
            'properties': properties,
        }

    @classmethod
    def get_molecule_from_list_xyz_1(cls, molecule: list) -> dict:
        """Construct molecule from list.

        Parameters
        ----------
        molecule : list
            list containing atoms and coordinates in format
            [[a1,x1,y1,z1],[a2,x2,...]]

        Returns
        -------
        dict
            atoms and coordinates

        """
        # single atom case
        if len(molecule) == 4:
            atoms = molecule[0]
            coordinates = molecule[1:]
        else:
            atoms = list(next(zip(*molecule)))
            coordinates = [[x for x in y if x not in atoms] for y in molecule]

        return {
            'atoms': atoms,
            'coordinates': np.array(coordinates).astype(np.float64),
        }

    @classmethod
    def get_molecule_from_dict(cls, molecule: dict) -> dict:
        """Extract molecule from dict.

        Parameters
        ----------
        molecule : dict
            dictionary containing keywords "atoms" and "coordiantes"

        Returns
        -------
        dict
            atoms and coordinates

        """
        unit_cell_keys = ['unit_cell', 'unitcell', 'box', 'boxes', 'pbc']
        for unit_cell_key in molecule.keys():
            if unit_cell_key in unit_cell_keys:
                if 'properties' in molecule.keys():
                    molecule['properties']['unit_cell'] = molecule[
                        unit_cell_key
                    ]
                else:
                    molecule['properties'] = {
                        'unit_cell': molecule[unit_cell_key]
                    }
                molecule.pop(unit_cell_key)
            break

        return molecule

    @classmethod
    def get_properties_from_dict(cls, molecule: dict) -> dict:
        """Extract properties from dict.

        Parameters
        ----------
        molecule : dict
            dictionary containing keyword "properties"

        Returns
        -------
        dict
            property dictionary

        """
        properties: dict = {}
        # properties['unit'] = molecule.get('unit', 'bohr')
        properties.update(molecule.get('properties', {}))
        return properties

    class PDBReader(FileReader):
        """PDB-format reader."""

        def get_molecule_from_file(self, filename: str) -> dict:
            """Construct molecule from pdb-file.

            Parameters
            ----------
            filename : str
                full name of pdb-file

            Returns
            -------
            dict
                atoms and coordinates

            """
            try:
                from rdkit import Chem
                from rdkit.Chem import AllChem  # noqa: F401
            except ImportError:
                raise ImportError('could not find package rdkit')

            mol = Chem.rdmolfiles.MolFromPDBFile(filename, removeHs=False)
            lines = Chem.AllChem.MolToXYZBlock(mol).split('\n')
            num_atoms = int(lines[0])
            atoms = []
            coordinates = []
            for j in range(2, 2 + num_atoms):
                atom, x, y, z = lines[j].split()
                atoms.append(atom)
                coordinates.append([float(x), float(y), float(z)])

            return {
                'atoms': atoms,
                'coordinates': np.array(coordinates).astype(np.float64),
            }

    class MOLReader(FileReader):
        """Mol-format reader."""

        def get_molecule_from_file(self, filename: str) -> dict:
            """Construct molecule from mol-file.

            Parameters
            ----------
            filename : str
                full name of mol-file

            Returns
            -------
            dict
                atoms and coordinates

            """
            # with open(filename, 'r') as myfile:
            #     lines = myfile.read().split('\n')

            # num_atoms = int(lines[3].split()[0])
            # atoms = []
            # coordinates = []
            # for j in range(4, 4 + num_atoms):
            #     x, y, z, atom = lines[j].split()[0:4]
            #     atoms.append(atom)
            #     coordinates.append([float(x), float(y), float(z)])
            try:
                from rdkit import Chem
                from rdkit.Chem import AllChem  # noqa: F401
            except ImportError:
                raise ImportError('could not find package rdkit')

            mol = Chem.rdmolfiles.MolFromMolFile(filename, removeHs=False)
            lines = Chem.AllChem.MolToXYZBlock(mol).split('\n')
            num_atoms = int(lines[0])
            atoms = []
            coordinates = []
            for j in range(2, 2 + num_atoms):
                atom, x, y, z = lines[j].split()
                atoms.append(atom)
                coordinates.append([float(x), float(y), float(z)])
            return {
                'atoms': atoms,
                'coordinates': np.array(coordinates).astype(np.float64),
            }

    class XYZReader(FileReader):
        """xyz-format reader."""

        def get_molecule_from_file(self, filename: str) -> dict:
            """Construct molecule from xyz-file.

            Parameters
            ----------
            filename : str
                full name of xyz-file

            Returns
            -------
            dict
                atoms and coordinates

            """
            with open(filename, 'r') as myfile:
                lines = myfile.read().split('\n')
            try:
                num_atoms = int(lines[0])
            except Exception:
                raise TypeError(f'file {filename} not a proper xyz-file')

            coordinates = []
            atoms = []
            # fmt: off
            for line in lines[2: num_atoms + 2]:
                # fmt: on
                split_line = line.split()
                try:
                    coordinates.append(
                        [float(value) for value in split_line[1:4]]
                    )
                    atoms.append(str(split_line[0]))
                except Exception:
                    raise TypeError(f'file {filename} not a proper xyz-file')
            return {
                'atoms': atoms,
                'coordinates': np.array(coordinates).astype(np.float64),
            }

    class ZMATReader(FileReader):
        """zmat-format reader."""

        def get_molecule_from_file(self, filename: str) -> dict:
            """Construct molecule from zmat-file.

            Parameters
            ----------
            filename : str
                full name of zmat-file

            Returns
            -------
            dict
                atoms and coordinates

            """
            with open(filename, 'r') as myfile:
                lines = myfile.read().split('\n')

            try:
                zmat = [
                    x.split() for x in lines if x != [] and x.split() != []
                ]
            #     lines2 = [x for x in lines if x != []]
            #     if not isinstance(lines[0][0], str):
            #         raise GenericError(
            #             f'file {filename} not a proper zmat-file')
            # zmat = []
            # for line in lines2:
            #     if line.split() == []:
            #         continue
            #     try:
            #         zmat.append(line.split())
            except Exception:
                raise TypeError(f'file {filename} not a proper zmat-file')

            atoms, coordinates = zmat2xyz(zmat)
            return {'atoms': atoms, 'coordinates': coordinates}

    class AseReader(FileReader):
        """ase-format reader."""

        def get_molecule_from_file(self, filename: str, **kwargs) -> dict:
            """Construct molecule from ase-file.

            Parameters
            ----------
            filename : str
                full name of ase-file

            Returns
            -------
            dict
                atoms and coordinates

            """
            try:
                from ase.io import read
            except ImportError:
                raise ImportError('could not find package ase')

            # some kwargs that should be used:

            atoms = read(filename)

            out_dict = {
                'atoms': atoms.get_chemical_symbols(),
                'coordinates': atoms.get_positions(),
                'properties': {},
            }
            if atoms.has('residuenumbers'):
                out_dict['properties'].update(
                    {'residue_idx': atoms.get_array('residuenumbers')}
                )
            if atoms.has('residuenames'):
                out_dict['properties'].update(
                    {'residue_name': atoms.get_array('residuenames')}
                )

            # Check if a unit cell is defined
            if atoms.get_pbc().any():
                # Convert to angstroms
                cell = atoms.get_cell()[:]
                out_dict['properties'].update({'unit_cell': cell})
            if atoms.has('atomtypes'):
                out_dict.update({'atoms': atoms.get_array('atomtypes')})

            if not out_dict['properties']:
                out_dict.pop('properties')

            out_dict['units'] = 'angstrom'

            if 'velocities' in atoms.arrays:
                # Cast a warning
                print('Velocities are not supported in the current format')

            return out_dict
