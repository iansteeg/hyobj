from __future__ import annotations

import hashlib
import warnings
from typing import Any, List, Optional, Union  # EllipsisType in py≥3.10

import numpy as np

try:
    import pandas as pd
except ImportError:
    pd = None
from hytools.logger import get_logger
from qcelemental import PhysicalConstantsContext, periodictable
from qcelemental.exceptions import NotAnElementError

from ..dataset import DataSet
from ..system import System
from ..units import Units
from .abc import ConvertClassInstance, MoleculeLike
from .bonds import Bonds
from .constraints import Constraints
from .constructor import Constructor
from .converter import Converter
from .internal_coordinates import InternalCoordinates
from .molecule_readers import MoleculeReaders
from .utils import view_molecule

constants = PhysicalConstantsContext('CODATA2018')

atom_colors = {
    'H': 'white',
    'He': 'cyan',
    'Li': 'purple',
    'Be': 'darkgreen',
    'B': 'salmon',
    'C': 'black',
    'N': 'blue',
    'O': 'red',
    'F': 'green',
    'Ne': 'cyan',
    'Na': 'blue',
    'Mg': 'forestgreen',
    'Al': 'gray',
    'Si': 'gold',
    'P': 'orange',
    'S': 'yellow',
    'Cl': 'green',
    'Ar': 'cyan',
    'K': 'purple',
    'Ca': 'darkgreen',
    'Fe': 'brown',
    'Cu': 'brown',
    'Se': 'red',
    'Br': 'darkred',
    'Kr': 'cyan',
    'Rb': 'purple',
    'Sr': 'darkgreen',
    'I': 'purple',
    'Xe': 'cyan',
    'Cs': 'purple',
    'Ba': 'darkgreen',
}


class Molecule(
    System,
    MoleculeLike,
    InternalCoordinates,
    Constraints,
    Constructor,
    Converter,
    Bonds,
):
    """Hylleraas Molecule Class.

    Paramters
    ---------
    molecule : Any
        Representation of the molecule.
        Possible input:
            * Filename (xyz, zmat, pdb, cif) containing molecular structure
            * content of xyz-file as a single string separated by
              a newline character
            * content of zmat-file as a single string separated by
              a newline character
            * (a single) list of atoms and coordinates
            * zmat in list format
            * dictionary containing atoms and coordinates
            * :obj:`hylleraas.Molecule` instance and compatible
            * :obj:`daltonproject.Molecule` instance and compatible


    filetype : str, optional
        filetype of structure file (currently, only "xyz", "zmat", "pdb"
        or "cif", "mol" and "Smiles" are supported).
        Defaults to the file extension.
    properties : dict, optional
        dictionary containing additional information about the molecule,
        like charge, spin, symmetry, etc.
    units: str or :obj:`hylleraas.units.Units`, optional
        units for coordinates (either bohr or angstrom). Defaults to bohr.
    optimize_smiles : str
        method for optimizing smiles string. Currently, only MMFF supported.

    Note:
    ----
        * Molecule class objects always create cartesian coordinates.
        * The conversion between zmat and xyz might be problematic for
        highly symmetrical molecules. This can be solved manually by modifying
        the structure a tiny bit.
        * we encountered problems with several pdb files.
        A solution was to re-numbering them using `pdb_reres`_.

         .. _pdb_reres:   https://github.com/haddocking/pdb-tools/

    """

    def __init__(
        self,
        molecule: Any,
        filetype: Optional[str] = None,
        properties: Optional[dict] = None,
        units: Optional[Union[str, Units]] = None,
        **kwargs,
    ):
        if kwargs.get('unit') is not None:
            raise DeprecationWarning('unit is deprecated, use units instead')

        self.logger = get_logger(
            logger_name=kwargs.pop('logger_name', 'Molecule'), **kwargs
        )

        self.debug = kwargs.get('debug', False)
        if self.debug:
            self.logger.setLevel('DEBUG')

        self.filetype = filetype
        self.optimize_smiles = kwargs.get('optimize_smiles', '')

        self.get_input_molecule = self.get_molecule_constructor(
            molecule, **kwargs
        )

        self._mol_input: dict = self.get_input_molecule(molecule)
        self._atoms = self._mol_input.get(
            'atoms', self._mol_input.get('_atoms', [])
        )

        self._coordinates = self._mol_input.get('coordinates', None)
        if self._coordinates is None:
            self._coordinates = self._mol_input.get('_coordinates', None)
        self._coordinates = np.array(self._coordinates)

        mol_units = None
        for key in ['units', '_units']:
            if key in self._mol_input.keys():
                mol_units = Units(self._mol_input.pop(key))

        self._properties = (
            self.get_molecule_properties(molecule)
            if properties is None
            else properties
        )

        try:
            prop = self._mol_input.get('properties', {})
            self._properties.update(prop)
        except TypeError:
            pass
        self._properties.update(kwargs.get('properties', {}))

        if units is not None:
            if isinstance(units, str):
                try:
                    self._units = Units(units)
                except ValueError:
                    if units.lower() in 'bohr':
                        self._units = Units('atomic')
                    elif units.lower() in 'angstroms':
                        self._units = Units('metal')
            elif isinstance(units, Units):
                self._units = units  # type: ignore
            else:
                raise TypeError(
                    'unit must be either str or ' + 'hylleraas.units.Units'
                )

            if mol_units is not None:
                if self._units != mol_units:
                    self._change_units(self._units, mol_units)
                    # self = convert_units(
                    #     self,
                    #     units_to=self._units,
                    #     units_from=mol_units,
                    #     overwrite=True,
                    # )
                    # self._properties['unit'] = self._units
        else:
            # try to infer unit from molecule_constructor
            if mol_units is not None:
                self._units = Units(mol_units)
            else:
                self._units = Units('atomic')

        self.masses = kwargs.get('masses', self._set_masses())
        self.atomic_numbers = kwargs.get(
            'atomic_numbers', self._set_atomic_numbers()
        )

        # if self.unit == 'angstrom':
        #     self._coordinates = self._coordinates / constants.bohr2angstroms
        #     self.unit = 'bohr'
        #     self._properties['unit'] = 'bohr'
        # elif self.unit == 'bohr':
        #     self.unit = 'bohr'
        #     self._properties['unit'] = 'bohr'
        # else:
        #     raise ValueError(
        #         f'unit {self._properties["unit"]} not known! ' +
        #         'Use either angstrom or bohr.')

        # check generated atoms
        # for atom in self._atoms:
        #     if atom not in periodictable.E:
        #         warnings.warn(
        #             f'element {atom} not known! Check input {molecule}')

    def view(self, **kwargs):
        """Alias of hylleraas.view called on the molecule directly."""
        return view_molecule(self, **kwargs)

    def __iter__(self):
        """Generate Iterator."""
        self.current_atom = -1
        return self

    def __next__(self):
        """Next iterable."""
        self.current_atom += 1
        if self.current_atom >= self.num_atoms:
            raise StopIteration
        return (
            self.atoms[self.current_atom],
            self.coordinates[self.current_atom],
        )

    def __repr__(self):
        """Dataclass-like representation."""
        keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(keywords))

    def __add__(self, fragments: Union[Molecule, List[Molecule]]) -> Molecule:
        """Add two (or more) Molecule instances.

        Parameters
        ----------
        fragments : :obj:`hylleraas.Molecule`
            or :obj:`List[hylleraas.Molecule]`
            Molecule fragments to be added to self.Molecule

        Returns
        -------
        :obj:`hylleraas.Molecule`
            new hyllerraas.Molecule object

        """
        if isinstance(fragments, Molecule):
            fragments = [fragments]

        atoms = self.atoms.copy()
        coordinates = self.coordinates.copy()

        for fragment in fragments:
            if not isinstance(fragment, Molecule):
                raise TypeError(
                    'Molecule instance expected, ' + f' got {type(fragment)}'
                )
            for atom, coord in fragment:
                atoms.append(atom)
                coordinates = np.append(coordinates, [coord], axis=0)
        # return supermolecule
        return Molecule(
            {'atoms': atoms, 'coordinates': coordinates, 'properties': {}}
        )

    def __sub__(self, fragments: Union[Molecule, List[Molecule]]) -> Molecule:
        """Substract two (or more) Molecule instances.

        Parameters
        ----------
        fragments : :obj:`hylleraas.Molecule`
            or :obj:`List[hylleraas.Molecule]`
            Molecule fragments to be removed from self.Molecule

        Returns
        -------
        :obj:`hylleraas.Molecule`
            new hyllerraas.Molecule object

        """
        if isinstance(fragments, Molecule):
            fragments = [fragments]

        atoms_del: List[str] = []
        coord_del: List[List[float]] = []
        for fragment in fragments:
            if not isinstance(fragment, Molecule):
                raise TypeError(
                    'Molecule instance expected, ' + f' got {type(fragment)}'
                )
            for atom, coord in fragment:
                atoms_del.append(atom)
                coord_del.append(coord)

        mol_del = Molecule({'atoms': atoms_del, 'coordinates': coord_del})
        if mol_del not in self:
            warnings.warn('could not find fragment in molecule')
        indices = list(range(self.num_atoms))
        for i in range(self.num_atoms):
            found = self.atom_in_molecule(self[i], mol_del)
            if found:
                indices = [j for j in indices if j != i]

        atoms = [self[i]._atoms[0] for i in indices]
        coordinates = [self[i]._coordinates[0] for i in indices]

        if atoms == []:
            raise IndexError('subtraction would yield empty molecule')

        return Molecule(
            {
                'atoms': atoms,
                'coordinates': np.array(coordinates),
                'properties': {},
            }
        )

    def __eq__(self, molecule) -> bool:
        """Compare two Molecule instances.

        Parameters
        ----------
        molecule : :obj:`hylleraas.Molecule`
            or :obj:`List[hylleraas.Molecule]`
            Molecule to be compared to self.Molecule

        Returns
        -------
        bool
            True if molecules are identical (same atoms and coordinates)

        """
        return self.hash == Molecule(molecule).hash

    def __contains__(self, molecule: Molecule) -> bool:
        """Check if Molecule is in self.

        Parameters
        ----------
        molecule : :obj:`hylleraas.Molecule`
            Molecule object to be checked

        Returns
        -------
        bool
            if molecule is in current Molecule instance.

        """
        found = []
        for a, c in molecule:
            atom = Molecule({'atoms': [a], 'coordinates': [c]})
            found.append(self.atom_in_molecule(atom, self))
        return all(found)

    def __getitem__(
        self,
        # EllipsisType in py≥3.10
        indices: Union[slice, tuple[int], list[int], int],
    ) -> Molecule:
        """Create a new Molecule from a subset of the atoms in this Molecule.

        Parameters
        ----------
        indices : int or tuple or list or slice
            Indices to use for the new Molecule.

        Returns
        -------
        A new independent Molecule instance.

        """
        if isinstance(indices, int):
            return Molecule(
                {
                    'atoms': [self._atoms[indices]],
                    'coordinates': [np.array(self._coordinates)[indices, :]],
                }
            )
        elif isinstance(indices, tuple) or isinstance(indices, list):
            return Molecule(
                {
                    'atoms': [self._atoms[k] for k in indices],
                    'coordinates': np.array(self._coordinates)[indices, :],
                }
            )
        elif isinstance(indices, slice):
            return Molecule(
                {
                    'atoms': self._atoms[indices],
                    'coordinates': np.array(self._coordinates)[indices, :],
                }
            )
        elif indices is Ellipsis:
            return Molecule(
                {
                    'atoms': self._atoms,
                    'coordinates': np.array(self._coordinates),
                }
            )
        else:
            raise NotImplementedError(
                f'Could not index molecule with {indices}'
            )

    def __len__(self):
        """Compute the number of atoms contained in this Molecule.

        Returns
        -------
        Number of atoms in this molecule.

        """
        return len(self._atoms)

    @property
    def hash(self):
        """Get hash of molecule."""
        return hashlib.sha256(
            ''.join(self.xyz_string).encode('utf-8')
        ).hexdigest()

    def __hash__(self):
        """Get hash of molecule."""
        return self.hash

    @property
    def atoms(self):
        """Set atoms."""
        return [atom for atom in self._atoms]

    @property
    def elements(self):
        """Set atoms (for compatibility with daltonproject)."""
        return [atom for atom in self._atoms]

    @property
    def units(self):
        """Get units."""
        return self._units

    @units.setter
    def units(self, new_unit):
        """Set units."""
        return self._change_units(new_unit, self._units)

    def _change_units(self, new_unit, old_unit):
        """Change units."""
        self.logger.debug(f'Changing units from {old_unit} to {new_unit}')
        new_unit = Units(new_unit)
        old_unit = Units(old_unit)
        self._coordinates *= new_unit.length[1] / old_unit.length[1]
        self._units = new_unit
        # if 'units' in self._properties.keys():
        #     self._properties['units'] = newunit
        if self._properties:
            for attribute in self._properties.keys():
                if attribute == 'unit_cell':
                    self._properties['unit_cell'] *= (
                        new_unit.length[1] / old_unit.length[1]
                    )
                else:
                    self.logger.warning(
                        'units changed, properties might be inconsistent'
                    )

    @property
    def coordinates(self):
        """Get cartesian coordinates."""
        return np.array(self._coordinates)

    def get_ase_object(self):
        """Get ASE atoms object."""
        try:
            from ase import Atoms
        except ImportError:
            raise ImportError('ase is required for this feature')
        return Atoms(symbols=self.atoms, positions=self.coordinates)

    def get_atomic_distances(self):
        """Get all distances between atoms, excluding diagonal using scipy."""
        ase_obj = self.get_ase_object()
        return ase_obj.get_all_distances(mic=True)[
            np.triu_indices(ase_obj.get_number_of_atoms(), k=1)
        ]

    @coordinates.setter  # type: ignore
    def coordinates(self, newcoord: Union[List, np.array]):
        """Set cartesian coordinates."""
        self._coordinates = np.array(newcoord)

    @property
    def properties(self):
        """Get molecule properties."""
        return self._properties

    @properties.setter
    def properties(self, prop):
        """Set molecule properties."""
        if isinstance(prop, dict):
            self._properties = prop
        else:
            raise TypeError('dictionary expected')

    @property
    def num_atoms(self):
        """Set number of atoms."""
        return len(self._atoms)

    def _set_masses(self):
        """Set atomic masses."""
        try:
            m = [
                periodictable.to_mass(self._atoms[i])
                for i in range(0, len(self._atoms))
            ]
        except NotAnElementError:
            m = [0.0 for i in range(0, len(self._atoms))]
        return m

    def _set_atomic_numbers(self):
        """Set atomic numbers."""
        try:
            nn = [
                periodictable.to_atomic_number(self._atoms[i])
                for i in range(0, len(self._atoms))
            ]
        except NotAnElementError:
            nn = []
        return nn

    @classmethod
    def atomic_colors(cls, atoms):
        """Set atomic colors from list of elements."""
        return [
            atom_colors.get(atoms[i], 'magenta') for i in range(0, len(atoms))
        ]

    @property
    def colors(self):
        """Set atomic colors."""
        return self.atomic_colors(self._atoms)

    @property
    def internal_coordinates(self):
        """Compute delocalized internal coordinates using geometric."""
        return self.get_internal_coordinates()

    def atom_in_molecule(
        self,
        atom: Molecule,
        molecule: Molecule,
        rtol: Optional[float] = 1e-6,
        atol: Optional[float] = 1e-6,
    ) -> bool:
        """Check if atom is in molecule.

        Parameters
        ----------
        atom: :obj:`hylleraas.Molecule`
            atom
        molecule: :obj:`hylleraas.Molecule`
            Molecule
        rtol: float, optional
            relative tolerance used in numpy.allclose().
            defaults to 1e-6
        atol: float, optional
            absolute tolerance used in numpy.allclose().
            defaults to 1e-6

        Returns
        -------
        bool
            True if atom in molecule

        """
        for atom1, coord1 in molecule:
            if atom1 != atom.atoms[0]:
                continue
            if np.allclose(coord1, atom.coordinates, rtol=rtol, atol=atol):
                return True
        return False

    def constrain(self, constraint: list) -> Molecule:
        """Constrain molecular geometry.

        Parameters
        ----------
        constraint: list
            constraint as a list, e.g. ['distance', 0, 1, 1.4],
            ['angle', 1, 0, 2, 104], ['dihedral', 0, 1, 2, 3, 179.0]

        Note
        ----
            distance uses the same unit as self.

        Returns
        -------
        Molecule
           Molecule with constraint coordinates

        """
        return Molecule(self.get_constraint_molecule(constraint))

    @property
    def xyz_string(self) -> str:
        """Generate a xyz string."""
        atoms = self.atoms
        coordinates = self.coordinates
        mol_string = str(len(atoms)) + '\n\n'
        if len(atoms) == 1 and len(coordinates) == 3:
            coordinates = [coordinates]
        for i, atom in enumerate(atoms):
            coord = np.array(coordinates).ravel().reshape(-1, 3)
            mol_string += str(atom).capitalize()
            mol_string += ' {:.12f}'.format(coord[i, 0])
            mol_string += ' {:.12f}'.format(coord[i, 1])
            mol_string += ' {:.12f}'.format(coord[i, 2])
            mol_string += '\n'
        return mol_string

    # def set_coordinates(self, new_coords: Union[list, np.array]) -> None:
    #     """Set coordinates of Molecule.

    #     Parameters
    #     ----------
    #     new_coords : :obj:`Union[list, np.array]`
    #         new coordinates

    #     """
    #     self._coordinates = new_coords

    # @classmethod
    # def find_molecule_in_namespace(cls, namespace: Any) -> Molecule:
    #     """Find Molecule in global namespace.

    #     Returns
    #     -------
    #     :obj:`hylleraas.Molecule`
    #         either **first** molecule found in namespace
    #         or hypothetical "Hylleraas Atom" Hy in the origin

    #     """
    #     for key, val in namespace.items():
    #         if isinstance(val, Molecule):
    #             warnings.warn(f'Molecule {key} in global namespace detected')
    #             return val
    #     return Molecule('Hy 0 0 0')

    def get_molecule_properties(self, input: Any) -> dict:
        """Extract molecule properties from input.

        Parameters
        ----------
        input : :obj:`Any`
            molecular input (str, list, dict, or class instance)

        Returns
        -------
        dict
            properties

        """
        if isinstance(input, dict):
            return MoleculeReaders.get_properties_from_dict(input)
        elif isinstance(input, (str, list)):
            return {}

        converter: ConvertClassInstance = None
        if pd is not None:
            if isinstance(input, (pd.DataFrame, pd.Series, DataSet)):
                converter = Converter.ConvertDataSetMolecule()
                return converter.get_properties_from_class_instance(input)

        try:
            converter = Converter.ConvertHylleraasMolecule()
            properties = converter.get_properties_from_class_instance(input)
        except AttributeError:
            converter = Converter.ConvertDaltonprojectMolecule()
            properties = converter.get_properties_from_class_instance(input)

        return properties

    def translate(self, vec: Union[list, np.array]) -> Molecule:
        """Translate molecule in space.

        Parameters
        ----------
        vec : list or :obj:`np.array`
            translation vector

        """
        new_coordinates = []
        for coordinates in self.coordinates:
            new_coordinates.append(coordinates + np.array(vec))
        return Molecule({'atoms': self.atoms, 'coordinates': new_coordinates})

    def rotate(self, rotmat: Union[list, np.array]) -> Molecule:
        """Rotate molecule in space.

        Parameters
        ----------
        rotmat : list or :obj:`np.array`
            3x3 rotation matrix

        """
        new_coordinates = np.zeros_like(self.coordinates)
        for i, coordinates in enumerate(self.coordinates):
            new_coordinates[i] = np.array(rotmat).dot(coordinates)
        return Molecule({'atoms': self.atoms, 'coordinates': new_coordinates})

    # def set_internal_coordinates(self, )
    # do via constraints in properties (?)


# if __name__ == "__main__":

#     mymol = Molecule("O=Cc1ccc(O)c(OC)c1COc1cc(C=O)ccc1O", filetype="smiles")
#     # print(mymol)
#     for i, j in mymol:
#         print(i, j)
#     # mymol = Molecule('O', filetype='smiles')

# print(mymol.internal_coordinates)

# mymol = Molecule('/Users/tilmann/Documents/work/hylleraas/
# interfaces/xtbtopo.mol')
# print(mymol.internal_coordinates)


# #     mymol = Molecule(molecule_string_1)

# #     mymol = Molecule(molecule_string_2)
# #     mymol = Molecule(molecule_string_3)
# mymol = Molecule('./examples/ch3cooh.zmat', filetype='zmat')
# # print(mymol)
# # print(mymol.angle(1,0,2))
# # print(mymol.internal_coordinates)
# # test = mymol.constrain(['angle', 1, 0, 2, 130.])
# # print(test)
# # print(test.angle(1,0,2))

# print(mymol.bonds)
# print(mymol.bond_orders)

# #     mymol = Molecule('../examples/water.xyz')
# #     mymol = Molecule([mymol.atoms, mymol.coordinates])
# #     mymol = Molecule([[mymol.atoms[i], *mymol.coordinates[i]]
# #                       for i in range(len(mymol.atoms))])
# #     mymol = Molecule({'atoms': mymol.atoms, 'coordinates':
# mymol.coordinates})
# #     mymol = mymol + mymol.translate([100, 0, 0])
# #     print(mymol)
# #     atoms = Molecule([['H', -0.00000e+00,  1.52061e-01, -9.87977e-01],
# # ['H', 1.00000e+02, 5.64570e-01, 4.80297e-01]])
# #     print()
# #     mymol2 = mymol - atoms
# #     print(mymol2.coordinates)
# #     mymol2 = Molecule(mymol)
# #     print(mymol2 == mymol)
# #     print(mymol2 == atoms)
# mymol = Molecule('Fe 0 0 0')
# print(mymol)
# mymol = Molecule('O=Cc1ccc(O)c(OC)c1COc1cc(C=O)ccc1O')
# print(mymol)
# mymol = Molecule('O=Cc1ccc(O)c(OC)c1COc1cc(C=O)ccc1O',
# optimize_smiles='MMFF')
# print(mymol)
