from abc import ABC, abstractmethod
from typing import Any

import numpy as np


class MoleculeLike(ABC):
    """Base class for molecule-like computable objects."""

    # @abstractmethod
    # def set_coordinates(self, new_coords: Union[list, np.ndarray]):
    #     """Set coordinates."""
    #     pass

    @property
    @abstractmethod
    def atoms(self) -> list:
        """Set atom-like objects (list(str))."""

    @property
    @abstractmethod
    def coordinates(self) -> np.ndarray:
        """Set cartesian coordinates."""

    @property
    @abstractmethod
    def num_atoms(self) -> int:
        """Set number of atom-objects (int)."""

    @property
    @abstractmethod
    def units(self) -> Any:
        """Set units."""


class ConvertClassInstance(ABC):
    """Base class for conversion of molecule objects."""

    @abstractmethod
    def get_molecule_from_class_instance(self, molecule: Any):
        """Extract molecule from class instance."""

    @abstractmethod
    def get_properties_from_class_instance(self, molecule: Any):
        """Extract properties from class instance."""


class FileReader(ABC):
    """Base class for methods that read molecular structure from file."""

    @abstractmethod
    def get_molecule_from_file(self, filename: str):
        """Construct molecule from file."""
