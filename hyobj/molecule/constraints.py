import numpy as np

from ..constants import Constants

try:
    import geometric
except ImportError:  # pragma no cover
    geometric = None


class Constraints:
    """Methods for constraining molecular Geometries."""

    def __init__(self):
        """Set subclass attributes."""
        self._properties: dict
        self._coordinates: np.ndarray
        self._atoms: list

    def get_constraint_molecule(self, constraint: list) -> dict:
        """Constrain molecular geometry.

        Parameters
        ----------
        constraint: list
            constraint as a list, e.g. ['distance', 0, 1, 1.4],
            ['angle', 1, 0, 2, 104], ['dihedral', 0, 1, 2, 3, 179.0]

        Note
        ----
            distance uses the same unit as self.

        Returns
        -------
        dict
            dictionary for molecule construction with constraint coordinates

        """
        if geometric is None:
            raise ImportError('could not find package geometric')

        self._constraint = constraint

        # conv_map = {'bohr': constants.bohr2angstroms, 'angstrom': 1.0}

        supported_constraints = ['distance', 'angle', 'dihedral']
        if self._constraint[0] not in supported_constraints:
            raise ValueError(f'supported keywords: {supported_constraints}')

        if self._constraint[0] == 'distance':
            # self._constraint[-1] *= conv_map[self._properties['unit']]
            self._constraint[-1] *= Constants.bohr2angstroms / \
                                     self.units.length[1]  # type: ignore

        geometric_str = f'$set\n {self._constraint[0]} '
        for i in range(1, len(self._constraint) - 1):
            geometric_str += f'{self._constraint[i]+1} '
        geometric_str += f'{self._constraint[-1]} '

        geometric_molecule = geometric.molecule.Molecule()
        geometric_molecule.elem = self._atoms

        geometric_molecule.xyzs = [np.array(self._coordinates)]

        try:
            if hasattr(geometric.optimize, 'parse_constraints'):
                geometric_constraints = geometric.optimize.parse_constraints(
                    geometric_molecule, geometric_str)
            elif hasattr(geometric.optimize, 'ParseConstraints'):
                geometric_constraints = geometric.optimize.ParseConstraints(
                    geometric_molecule, geometric_str)
            else:
                raise NotImplementedError(
                    'Geoemtric could not parse constraints')

            ic = geometric.optimize.DelocalizedInternalCoordinates(
                geometric_molecule)
            ic.addConstraint(
                geometric_constraints[0],
                geometric_constraints[1],
                geometric_molecule.xyzs[0],
            )
            ic.Prims = geometric.optimize.PrimitiveInternalCoordinates(
                geometric_molecule,
                connect=False,
                addcart=False,
                constraints=geometric_constraints[0],
                cvals=geometric_constraints[1],
            )
            ic.build_dlc(geometric_molecule.xyzs[0])
        except Exception:
            raise RuntimeError(
                'geometric could not construct internal coordinates with ' +
                f'constraint {geometric_str}')

        try:
            xyz_new = ic.applyConstraints(
                geometric_molecule.xyzs[0].ravel()).reshape(-1, 3)
        except Exception:
            raise RuntimeError(
                'geometric could not construct constraint cartesian geometry')

        vals = ic.Prims.calculate(xyz_new)
        val = vals[0]
        if any([constraint[0] == 'angle', constraint[0] == 'dihedral']):
            val = val * 180.0 / np.pi

        return {
            'atoms': self._atoms,
            'coordinates': np.array(xyz_new),
            'properties': self._properties,
        }
