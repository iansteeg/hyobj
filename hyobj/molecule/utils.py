from __future__ import annotations

import inspect
from pathlib import Path
from typing import TYPE_CHECKING, Any, Dict, List, Optional, Tuple, Union

import numpy as np

try:
    import py3Dmol
except ImportError:
    found_py3dmol = False
else:
    found_py3dmol = True

if TYPE_CHECKING:
    from hylleraas.molecule import Molecule

# def available_interfaces():
#     """Print available interfaces."""
#     import hyif as interfaces
#     list = [x for x in dir(interfaces) if x.startswith('__') is False]
#     print('\nINTERFACES\n----------')
#     for elem in list:
#         if not elem.isupper():
#             if elem[0].isupper():
#                 sublist = [x for x in dir(getattr(interfaces, elem))
#                            if x.startswith('__') is False]
#                 print(elem, ' : ', sublist)
#     print('\nMAPPINGS\n--------')
#     for elem in list:
#         if elem.isupper():
#             print(elem, ' : ', getattr(interfaces, elem))


def str_in_list_regex(instr: str, matchlist: Union[str, list]) -> bool:
    """Look for instr in matchlist.

    Parameter
    ---------
    instr: str
        str
    matchlist: str or list of str
        list of str, might contain regex

    Returns
    -------
    bool
        if instr matches any element in matchlist

    """
    if isinstance(matchlist, str):
        matchlist = [matchlist]

    match = False
    import re

    for e in matchlist:
        if e == '*':
            return True
        regex = e.replace('*', '.*')
        try:
            pattern = re.compile(regex)
            match = bool(pattern.fullmatch(instr))
        except Exception:
            match = instr == e
        finally:
            if match:
                return True
            else:
                continue

    return False


def match_pattern(matchlist: list, instr: str) -> bool:
    """Look for instr in matchlist."""
    import re
    return any(
        bool(re.compile(e.replace('*', '.*')).fullmatch(instr))
        if '*' in e else instr == e
        for e in matchlist
    )


def is_file(filename: str) -> bool:
    """Check if input string is a file and exists."""
    # if '\n' in filename:
    #     return False

    isfile: bool
    try:
        file = Path(filename)
    except (OSError, TypeError):
        isfile = False
    else:
        try:
            if file.exists():
                isfile = True
            else:
                isfile = False
        except (OSError, TypeError):
            isfile = False

    return isfile


def key_in_arglist(*args, **kwargs) -> bool:
    """Check if key is in arguments."""
    if 'look_for' not in kwargs:
        return False
    search_str = kwargs['look_for']

    if search_str in kwargs:
        return True

    for arg in args:
        if isinstance(arg, dict):
            if search_str in arg:
                return True
        elif inspect.isclass(type(arg)):
            if hasattr(arg, search_str):
                return True
    return False


def del_key_from_arglist(*args, **kwargs) -> bool:
    """Delete key from arguments."""
    if 'look_for' not in kwargs:
        return False
    search_str = kwargs['look_for']

    if search_str in kwargs:
        del kwargs[search_str]
        return True

    for arg in args:
        if isinstance(arg, dict):
            if search_str in arg:
                del arg[search_str]
                return True
        elif inspect.isclass(type(arg)):
            if hasattr(arg, search_str):
                delattr(arg, search_str)
                return True
    return False


def get_val_from_arglist(*args, **kwargs) -> Any:
    """Get value from arguments."""
    if 'look_for' not in kwargs:
        return None
    search_str = kwargs['look_for']

    if search_str in kwargs:
        return kwargs[search_str]
    for arg in args:
        if isinstance(arg, dict):
            if search_str in arg:
                return arg[search_str]
        elif inspect.isclass(type(arg)):
            if hasattr(arg, search_str):
                return getattr(arg, search_str)
    return None


def dict2obj(instance: Any, data: dict) -> bool:
    """Convert dict to class instance attributes.

    Parameters
    ----------
    instance : :obj:`Any`
        class instance
    data : dict
        input dictionary

    """
    if isinstance(data, dict):
        for key in data:
            if not isinstance(data[key], dict):
                instance.__dict__.update({key: data[key]})
            else:
                instance.__dict__.update({key: dict2obj(instance, data[key])})
        return True
    return False


def func2method(func, instance, method_name=None):
    """Convert method to attribute."""
    setattr(instance, method_name or func.__name__, func)


def in_jupyter() -> bool:
    """Check environment.

    Returns
    -------
    bool
        true if the environment is a jupyter notebook

    """
    try:
        from IPython import get_ipython
    except ImportError:
        return False

    ip = get_ipython()
    if ip is None:
        return False
    else:
        # for fixing F821
        _checkhtml: Any = None
        try:
            from IPython.core.interactiveshell import InteractiveShell
        except ImportError:
            return False
        try:
            fmt = InteractiveShell.instance().display_formatter.format
            if len(fmt(_checkhtml, include='text/html')[0]):
                return True
            else:
                return False
        except (AttributeError, RuntimeError, OSError):
            return False


def zmat2dict(zmat: list) -> dict:
    """Convert zmat to dict."""
    atoms, coordinates = zmat2xyz(zmat)
    return {'atoms': atoms, 'coordinates': coordinates}


def zmat2xyz(zmat) -> Tuple[list, np.ndarray]:
    """Compute xyz coordinates from zmat.

    Paramters
    ---------
        zmat : :obj:`Sequence[Union[str, int, float]]`
            zmat as generated by Molecule.read_zmat()

    Returns
    -------
    tuple
        atoms, coordinates: extracted from zmat

    """
    num_atoms = len(zmat)
    xyz = np.zeros([num_atoms, 3])
    atoms = []
    for i1 in range(0, num_atoms):
        atoms.append(zmat[i1][0].lower().title())
        xyz[0] = np.zeros(3)
        if i1 == 1:
            dist = float(zmat[i1][2])
            xyz[i1] = [dist, 0.0, 0.0]
        if i1 == 2:
            i2 = int(zmat[i1][1]) - 1
            dist = float(zmat[i1][2])
            i3 = int(zmat[i1][3]) - 1
            angle = float(zmat[i1][4]) / 180.0 * np.pi
            t = xyz[i3][0] - xyz[i2][0]
            x = dist * np.cos(angle)
            y = dist * np.sin(angle)
            x = xyz[i2][0] + np.sign(t) * x
            y = xyz[i2][1] + np.sign(t) * y
            xyz[i1] = [x, y, 0.0]
        if i1 > 2:
            i2 = int(zmat[i1][1]) - 1
            dist = float(zmat[i1][2])
            i3 = int(zmat[i1][3]) - 1
            angle = float(zmat[i1][4]) / 180.0 * np.pi
            i4 = int(zmat[i1][5]) - 1
            dihedral = float(zmat[i1][6]) / 180.0 * np.pi
            tx = dist * np.cos(angle)
            ty = dist * np.cos(dihedral) * np.sin(angle)
            tz = dist * np.sin(dihedral) * np.sin(angle)
            v1 = xyz[i3] - xyz[i4]
            v2 = xyz[i2] - xyz[i3]
            n2 = np.linalg.norm(v2)
            if n2 > 1.0e-09:
                v2 = v2 / n2
            u1 = np.cross(v1, v2)
            n1 = np.linalg.norm(u1)
            if n1 > 1.0e-09:
                u1 = u1 / n1
            u2 = np.cross(u1, v2)
            d = np.zeros(3)
            d[0] = -tx * v2[0] + ty * u2[0] + tz * u1[0]
            d[1] = -tx * v2[1] + ty * u2[1] + tz * u1[1]
            d[2] = -tx * v2[2] + ty * u2[2] + tz * u1[2]
            xyz[i1] = xyz[i2] + d
    return atoms, np.around(xyz, decimals=6)


def xyz2zmat(atoms: list, coordinates: list) -> list:
    """Compute zmat from atoms and coordinates.

    Returns
    -------
    :obj:`Sequence[Union[str, int, float]]`
        zmat

    """
    zmat = []
    for i1, _ in enumerate(atoms):
        line = []
        line.append(atoms[i1])
        if i1 > 0:
            i2 = max(0, i1 - 3)
            # dist = __class__.distance_ext(xyz[i], xyz[j])
            v1 = np.array(coordinates[i1])
            v2 = np.array(coordinates[i2])
            dist = np.linalg.norm(v1 - v2)
            line.append(i2 + 1)
            line.append(round(dist, 6))

        if i1 > 1:
            i3 = max(1, i1 - 2)
            # angle = __class__.angle_ext(xyz[i], xyz[j], xyz[k])
            v1 = np.array(coordinates[i1]) - np.array(coordinates[i2])
            v2 = np.array(coordinates[i3]) - np.array(coordinates[i2])
            c = np.dot(v1, v2)
            s = np.linalg.norm(np.cross(v1, v2))
            angle = np.arctan2(s, c) * 180.0 / np.pi
            line.append(i3 + 1)
            line.append(round(angle, 6))

        if i1 > 2:
            i4 = max(2, i1 - 1)
            # dihedral = __class__.dihedral_ext(xyz[i], xyz[j], xyz[k],xyz[l])
            u1 = np.array(coordinates[i2]) - np.array(coordinates[i1])
            u2 = np.array(coordinates[i3]) - np.array(coordinates[i2])
            u3 = np.array(coordinates[i4]) - np.array(coordinates[i3])
            v1 = np.cross(u1, u2)
            v2 = -np.cross(u2, u3)
            n1 = np.linalg.norm(v1)
            n2 = np.linalg.norm(v2)
            n3 = np.linalg.norm(u2)
            if n1 < 1.0e-9 or n2 < 1.0e-9 or n3 < 1.0e-09:
                dihedral = 0.0
            else:
                v1 = v1 / n1
                v2 = v2 / n2
                w = np.cross(v1, u2)
                x = np.dot(v1, v2)
                y = np.dot(w, v2) / n3
                dihedral = -(np.arctan2(y, x) + np.pi) * 180.0 / np.pi
                if dihedral < -180.0:
                    dihedral += 360.0

            line.append(i4 + 1)
            line.append(round(dihedral, 6))

        zmat.append(line)
    return zmat


def py3dmole_defaults(options: Optional[dict] = None) -> dict:
    """Set default options for py3dmol."""
    options = {} if options is None else options

    defaults: dict = {}
    defaults['height'] = 400
    defaults['width'] = 640
    defaults['style'] = 'ballstick'
    defaults['labels'] = None
    defaults['show'] = True
    defaults['zoom_to'] = True
    defaults['return_view'] = True
    defaults['background_color'] = '#17d9ff'
    defaults['frames'] = 10
    defaults['amplitude'] = 1
    defaults['steplength'] = 1.0
    defaults['view_object'] = None
    defaults['animate_interval'] = 300
    defaults.update(options)

    return defaults


def view_trajectory(
    molecules: list, options: Optional[dict] = None, **kwargs
) -> py3Dmol.view:
    """Visualize a trajectory using ``py3Dmol``."""
    if not found_py3dmol:
        raise ImportError('could not find packge py3dmol')
    options = py3dmole_defaults(options)
    if options['view_object'] is None:
        view_object = py3Dmol.view(
            height=options['height'], width=options['width']
        )
    else:
        view_object = options['view_object']

    view_object.clear()
    mol_string: str = ''
    for molecule in molecules:

        atoms: List[str] = molecule.atoms
        coordinates: np.ndarray = molecule.coordinates

        mol_string += str(len(atoms)) + '\n\n'
        for i, atom in enumerate(atoms):
            coordinates = np.array(coordinates).ravel().reshape(-1, 3)
            mol_string += atom
            mol_string += ' {:.12f}'.format(coordinates[i, 0])
            mol_string += ' {:.12f}'.format(coordinates[i, 1])
            mol_string += ' {:.12f}'.format(coordinates[i, 2])
            mol_string += '\n'

    if options['style'] == 'ballstick':
        view_object.addModelsAsFrames(mol_string, 'xyz')
        view_object.setStyle(
            {'stick': {'radius': 0.1}, 'sphere': {'scale': 0.25}}
        )
    else:
        view_object.addModel(mol_string, 'xyz')
        view_object.setStyle({options['style']: kwargs})

    if options['labels'] is not None:
        for index, label in options['labels'].items():
            view_object.addLabel(
                label,
                {'backgroundColor': '#2b38ff'},
                {'index': index},
                True,
            )

    view_object.setBackgroundColor(options['background_color'])
    if options['zoom_to']:
        view_object.zoomTo()

    if options['show']:
        view_object.show()
    view_object.animate(
        {'loop': 'forward', 'reps': 0, 'interval': options['animate_interval']}
    )
    view_object.update()

    if options['return_view']:
        return view_object
    else:
        return None


def view_vibration(
    molecule: Molecule,
    mode: Union[np.array, list],
    options: Optional[dict] = None,
    **kwargs,
) -> py3Dmol.view:
    """Visualize a vibration using ``py3Dmol``."""
    if not found_py3dmol:
        raise ImportError('could not find packge py3dmol')

    options = py3dmole_defaults(options)
    mode_view = np.array(mode) * options['steplength']
    mode_view = mode_view.reshape(-1, 3)

    if options['view_object'] is None:
        view_object = py3Dmol.view(
            height=options['height'], width=options['width']
        )
    else:
        view_object = options['view_object']

    view_object.clear()

    atoms: List[str] = molecule._atoms
    coordinates: np.ndarray = molecule._coordinates
    mol_string: str = str(len(atoms)) + '\n\n'
    for i, atom in enumerate(atoms):
        coordinates = np.array(coordinates).ravel().reshape(-1, 3)
        mol_string += atom
        mol_string += ' {:.12f}'.format(coordinates[i, 0])
        mol_string += ' {:.12f}'.format(coordinates[i, 1])
        mol_string += ' {:.12f}'.format(coordinates[i, 2])
        mol_string += ' {:.12f}'.format(mode_view[i, 0])
        mol_string += ' {:.12f}'.format(mode_view[i, 1])
        mol_string += ' {:.12f}'.format(mode_view[i, 2])
        mol_string += '\n'

    if options['style'] == 'ballstick':
        view_object.addModel(
            mol_string,
            'xyz',
            {
                'vibrate': {
                    'frames': options['frames'],
                    'amplitude': options['amplitude'],
                }
            },
        )
        model = view_object.getModel()
        model.setStyle({'stick': {'radius': 0.1}, 'sphere': {'scale': 0.25}})
    else:
        view_object.addModel(
            mol_string,
            'xyz',
            {
                'vibrate': {
                    'frames': options['frames'],
                    'amplitude': options['amplitude'],
                }
            },
        )
        model = view_object.getModel()
        model.setStyle({options['style']: kwargs})

    if options['labels'] is not None:
        for index, label in options['labels'].items():
            view_object.addLabel(
                label,
                {'backgroundColor': '#2b38ff'},
                {'index': index},
                True,
            )

    view_object.setBackgroundColor(options['background_color'])
    if options['zoom_to']:
        view_object.zoomTo()

    if options['show']:
        view_object.show()
    view_object.animate({'loop': 'backAndForth'})
    view_object.update()

    if options['return_view']:
        return view_object
    else:
        return None


def view_molecule(
    molecule: Molecule,
    height: int = 400,
    width: int = 640,
    style: str = 'ballstick',
    labels: Dict[int, str] = None,
    view_object: py3Dmol.view = None,
    show: bool = True,
    zoom_to: bool = True,
    return_view: bool = True,
    background_color: str = '#17d9ff',
    **kwargs,
) -> py3Dmol.view:
    """Visualize a Hylleraas Software Platform Molecule using ``py3Dmol``.

    If the `view_object` parameter is provided, this object is cleared and
    updated with new data. If `view_object` is not given, a new view is
    created.

    Parameters
    ----------
    molecule : hylleraas.Molecule
        Input molecule.
    height : int, optional
        View container height in pixels.
    width : int, optional
        View container width in pixels.
    style : ["ballstick", "line", "cross", "cartoon", "stick", "sphere"],
        optional 3DMol.js visualization style. By default ``"ballstick"``.
    labels : Dict[int, str], optional
        Add labels to selected atom specified by
        ``{<atom_index>: <label>, ...}``.
    view_object : py3Dmol.view, optional
        If provided, `view_object` is ``clear()``-ed and ``update()``-d,
        instead        of creating a new ``py3Dmol.view`` object.
    show : bool, optional
        If ``True``, ``show()`` is called on the ``py3Dmol.view`` object
    zoom_to : bool, optional
        If ``True``, ``zoomTo()`` is called on the ``py3Dmol.view`` object
        prior to displaying.
    return_view : bool, optional
        If ``True``, the ``py3Dmol.view`` object is returned. If ``False``,
        ``None`` is returned.
    background_color : str, optional
        Background color for the ``py3Dmol`` viewer.

    Returns
    -------
    py3Dmol.view or None
        py3DMol view object for visualization the input molecule
        if `return_view` is ``True``, else ``None``

    """
    if not found_py3dmol:
        raise ImportError('could not find packge py3dmol')
    if view_object is None:
        view_object = py3Dmol.view(height=height, width=width)
    view_object.clear()

    atoms: List[str] = molecule._atoms
    coordinates: np.ndarray = molecule._coordinates
    mol_string: str = str(len(atoms)) + '\n\n'
    for i, atom in enumerate(atoms):
        coordinates = np.array(coordinates).ravel().reshape(-1, 3)
        mol_string += atom
        mol_string += ' {:.12f}'.format(coordinates[i, 0])
        mol_string += ' {:.12f}'.format(coordinates[i, 1])
        mol_string += ' {:.12f}'.format(coordinates[i, 2])
        mol_string += '\n'
    if style == 'ballstick':
        view_object.addModel(mol_string, 'xyz')
        model = view_object.getModel()
        model.setStyle({'stick': {'radius': 0.1}, 'sphere': {'scale': 0.25}})
    else:
        view_object.addModel(mol_string, 'xyz')
        model = view_object.getModel()
        model.setStyle({style: kwargs})

    if labels is not None:
        for index, label in labels.items():
            view_object.addLabel(
                label,
                {'backgroundColor': '#2b38ff'},
                {'index': index},
                True,
            )

    view_object.setBackgroundColor(background_color)
    if zoom_to:
        view_object.zoomTo()
    if show:
        view_object.show()
    view_object.update()
    if return_view:
        return view_object
    else:
        return None


# if __name__ == '__main__':

#     available_interfaces()
