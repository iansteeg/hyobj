from pathlib import Path
from typing import Tuple, Union

import numpy as np

try:
    import pandas as pd
except ImportError:  # pragma: no cover
    pd = None

from ..abc import BaseInterface


class NumpyInterface(BaseInterface):
    """Interface to Numpy."""

    def __init__(self):
        self.name = self.__class__.__name__

    def get_data(self, data_input: Union[Path, np.ndarray],
                 options: dict) -> np.ndarray:
        """Get data from numpy array or filename.

        Parameter
        ---------
        data_input: Union[pathlib.path, numpy.ndarray]
            input (filename or array)
        options:dict
            unused

        Returns
        -------
        np.ndarray
            numpy array

        """
        if isinstance(data_input, np.ndarray):
            return data_input
        elif isinstance(data_input, Path):
            filename = Path(data_input)
            if filename.suffix == '.npy':
                return np.load(filename)
        else:
            raise NotImplementedError('Numpy interface can only read form ' +
                                      f'{type(Path)} or {type(np.ndarray)}')

    def get_shape(self, data: np.ndarray):
        """Get shape of numpy array."""
        return data.shape

    def rearrange(
        self,
        data: np.ndarray,
        orig_shape: Tuple[int],
        target_shape: Tuple[int],
    ) -> np.ndarray:
        """Reshape numpy array.

        Parameter
        ---------
        data: np.ndarray
            numpy array
        orig_shape: tuple
            unused
        target_shape: tuple
            shape the array should be converted to

        Returns
        -------
        np.ndarray
            array as from np.reshape

        """
        return np.reshape(data, target_shape)

    def to_pandas(self, data: np.ndarray, dimension: str,
                  name: str) -> pd.DataFrame:
        """Convert array to pandas DataFrame.

        Parameter
        ---------
        data: np.ndarray
            numpy array
        dimension: str
            how to handle multidimensional arrays:
            1d: unroll and store as 1d pandas Series
            2d: unroll and convert to 2d pandas DataFrame
            multi: convert to list of objects (where a pandas DataFrame cell
                    can be a list/np.array)

        Returns
        -------
        pd.DataFrame
            generated DataFrame

        """
        if pd is None:  # pragma: no cover
            raise ImportError('could not import pandas')
        dim = len(self.get_shape(data))
        dimension_map = {1: '1d', 2: '2d'}
        dimension = dimension or dimension_map.get(dim, '2d')

        if dimension.lower() == 'multi':
            length = len(data)
            if dim == 1:
                s = pd.Series([data.ravel()], name=name)
                df = pd.DataFrame(s)
                return df

            _data = data.ravel().reshape(length, -1)
            df = pd.DataFrame({name: [_data[0, :]]})
            for i in range(1, length):
                df2 = pd.DataFrame({name: [_data[i, :]]})

                df = pd.concat([df, df2], ignore_index=True)
            return df
        if dimension.lower() == '1d':
            s = pd.Series(data.ravel(), name=name)
            return pd.DataFrame(s)
        elif dimension.lower() == '2d':
            length = len(data)
            return pd.DataFrame(data.ravel().reshape(length, -1))
        else:
            raise NotImplementedError('only 1d and 2d conversion available')


# class NumpyArray:
#     """Interface to numpy arrays."""

#     def __init__(self, data):
#         self.data = data
#         self.shape = data.shape
#         self.x, self.y = self.get_xy()

#     def dump(self,
#              filename: Optional[str] = None,
#              choose: Optional[list] = None):
#         """Write array to disk."""
#         if filename is None:
#             timestr = time.strftime('%Y-%m-%d-%H-%M-%S')
#             filename = 'dataset_' + timestr
#         path = pathlib.Path(filename + '.npy')

#         if choose is None:
#             with open(path, 'wb') as f:
#                 np.save(f, self.data)
#         else:
#             with open(path, 'wb') as f:
#                 np.save(f, self.data[:, choose])

#     def dump_multiple(
#         self,
#         filename: Optional[str] = None,
#         choose: Optional[Union[dict, list]] = None,
#     ):
#         """Write multiple arrays to disk."""
#         if filename is None:
#             timestr = time.strftime('%Y-%m-%d-%H-%M-%S')
#             filename = 'dataset_' + timestr

#         path = pathlib.Path(filename + '.npz')

#         if choose is None:
#             with open(path, 'wb') as f:
#                 np.savez(f, self.data)
#         else:
#             if isinstance(choose, dict):
#                 print_dict = {}
#                 for k, v in choose.items():
#                     print_dict[v] = self.data[k]

#             elif isinstance(choose, list):
#                 print_dict = {}
#                 for v in np.arange(len(choose)):
#                     print_dict[v] = self.data[choose[v]]

#             np.savez(f, **print_dict)

#     def get_xy(self):
#         """Get X and Y from data."""
#         if len(np.shape(self.data)) == 1:
#             return np.arange(np.shape(self.data)[0]), self.data
#         return (
#             self.data[0],
#            self.data[:, [i for i in range(np.shape(self.data)[1]) if i > 0]],
#         )

#     def split(self,
#               size: Optional[list] = None,
#               strategy: Optional[str] = 'random'):
#         """Partition data."""
#         # possibilities: random, sequential
#         size = [0.7, 0.15, 0.15] if size is None else size
#         size = np.array(size)
#         if np.linalg.norm(size) > 1:
#             size = size / np.linalg.norm(size)

#         dim = len(self.x)
#         assert len(self.x) == len(self.y)

#         idx_pool = list(set(range(dim)))
#         idxs = []
#         sets = []
#         for i in range(len(size)):
#             num = int(dim * size[i])
#             if strategy == 'sequential':
#                 idx = np.random.choice(idx_pool, size=num, replace=False)
#             elif strategy == 'sequential':
#                 idx = np.arange(num)
#             elif strategy == 'alternating':
#               idx = list(itertools.takewhile(lambda x: x % 2 == 0, idx_pool))

#             newx = (self.x[idx] if len(np.shape(self.x)) == 1 else self.x[:,
#                                                                         idx])
#             newy = (self.y[idx] if len(np.shape(self.y)) == 1 else self.y[:,
#                                                                         idx])

#             sets.append(newx)
#             sets.append(newy)
#             idx_pool = list(set(idx_pool) - set(idx))
#             idxs.append(idx)

#         return sets

#     def append_data(self, newdata):
#         """Apend data fo array."""
#         self.data = np.append(self.data, newdata)
