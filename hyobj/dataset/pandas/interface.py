import warnings
from pathlib import Path
from typing import List, Optional, Tuple

import pandas as pd


class PandasInterface:
    """Interface to Pandas."""

    def __init__(self):
        """Initialise Pandas interface."""
        if pd is None:  # pragma: no cover
            raise ImportError('could not import pandas')

    def get_data(self, inp: Path, options: dict) -> pd.DataFrame:
        """Get data.

        Parameter
        ---------
        inp:  pathlib.Path
            path to unput file
        options: dict
            unused

        Returns
        -------
        pandas.DataFrame
            pandas DataFrame

        """
        read_opt = options.get('read_opt', {})
        filename = Path(inp)
        if filename.suffix == '.csv':
            return pd.read_csv(filename, **read_opt)
        elif filename.suffix == '.json':
            return pd.read_json(filename, **read_opt)
        elif filename.suffix == '.h5':
            return pd.read_hdf(filename, **read_opt)
        elif filename.suffix == '.pkl':
            return pd.read_pickle(filename, **read_opt)
        elif filename.suffix == '.parquet':
            return pd.read_parquet(filename, **read_opt)
        else:
            raise TypeError(
                'cannot read data from file  with suffix '
                + f'{filename.suffix}'
            )

    def get_shape(self, df: pd.DataFrame) -> Tuple[int]:
        """Get shape of DataFrame."""
        return df.shape

    def to_pandas(self, data, *args):
        """Convert to pandas."""
        return data

    def get_axis(self, df_list, options: Optional[dict] = None) -> int:
        """Get common axis of dataframes."""
        options = options or {}
        align = options.get('align')
        align = '' if align is None else align
        if 'column' in align.lower():
            return 1
        elif 'row' in align.lower():
            return 0
        shapes = [self.get_shape(df) for df in df_list]
        if all([s == shapes[0] for s in shapes]):
            warnings.warn(
                'identical dimensions,using 1st axis for concanating'
            )
            return 1
        elif all([s[0] == shapes[0][0] for s in shapes]):
            return 1
        elif all([s[1] == shapes[0][1] for s in shapes]):  # type: ignore
            return 0
        else:
            raise TypeError(
                'could not join inhomogenous dataframes'
                + f'shapes: {shapes}'
                + f'dataframes: {df_list}'
            )

    def get_common_columns(self, df_list: List[pd.DataFrame]) -> List[str]:
        """Get common columns."""
        common_columns = set(df_list[0].columns)
        for df in df_list[1:]:
            common_columns = common_columns.intersection(df.columns)
        return list(common_columns)

    def join(self, df_list: List[pd.DataFrame],
             options: Optional[dict] = None):
        """Join to pandas frames. Axes automatically detected."""
        options = options or {}
        if not all([isinstance(df, pd.DataFrame) for df in df_list]):
            raise TypeError('expected list of pandas DataFrames')
        if len(df_list) == 1:
            return df_list[0]
        col_names = [df.columns.values for df in df_list]
        col_names = [item for sublist in col_names for item in sublist]
        col_names0 = df_list[0].columns.values
        if len(set(col_names)) == len(col_names0):
            axis = 0
        else:
            axis = self.get_axis(df_list, options)

        ignore_index = True
        if axis == 1:
            ignore_index = False

        if axis == 1:
            join = 'inner'
        elif axis == 0:
            join = 'outer'

        # find common columns
        common_column = options.get('common_column',
                                    self.get_common_columns(df_list))

        if not isinstance(common_column, list):
            common_column = [common_column]

        # col = common_column[0] if len(common_column) >= 0 else None

        # if col:
        #     #  compare common columns
        #     for df in df_list[1:]:
        #         if not isinstance(df, pd.DataFrame):
        #             continue
        #         if not all(df[col] == df_list[0][col]):
        #             print('WARNING: joining failed, ' +
        #                   f're-shuffling dataframes on {col}')
        #             for i in range(1, len(df_list)):
        #                 df_list[i] = df_list[0][[col]].merge(
        #                     df_list[i], on=col, how='left')
        #             if not all(df[col] == df_list[0][col]):
        #                 raise ValueError(
        #                     'could not join dataframes on common column:' +
        #                     f'common column {col} are not equal')
        if join == 'inner' and len(df_list[0].columns
                                   ) > 1 and len(common_column) > 0:
            col = common_column[0]
            # remove common_column[1:] from df_list[1:]:
            for i in range(1, len(df_list)):
                df_list[i] = df_list[i].drop(columns=common_column[1:])
            merged_df = df_list[-1]
            for df in df_list[:-1]:
                # Merge the current dataframe with the merged dataframe
                # Use 'how=right' to keep every row from the current dataframe
                merged_df = merged_df.merge(df, on=col, how='right')
            return merged_df

        # join = 'inner'
        df = pd.concat(
            df_list, axis=axis, ignore_index=ignore_index, join=join
        )
        return df

    def rearrange(
        self,
        data: pd.DataFrame,
        orig_shape: Tuple[int],
        target_shape: Tuple[int],
    ) -> pd.DataFrame:
        """Rearrange DataFrame."""

# class PandasDataFrame:
#     """Interface class to pandas."""

#     def __init__(self, data):
#         self.data = data

#     def get_columns(self):
#         """List all columns."""
#         return list(self.data.columns)

#     def dump_cell(
#         self,
#         column: str,
#         index: int,
#         filename: Optional[str] = None,
#         filetype: Optional[str] = None,
#     ):
#         """Write cell to disk."""
#         filetype = '.npy' if not filetype else filetype
#         if filetype not in ['.txt', '.npy']:
#             raise NotImplementedError(
#                 'currently, column dump only possible as ' +
#                 'numpy array .npy or text file .txt '
#             )

#         if filename is None:
#             timestr = time.strftime('%Y-%m-%d-%H-%M-%S')
#             filename = column + '_' + str(index) + '_' + timestr + filetype
#         path = pathlib.Path(filename)
#         if path.suffix == '.npy':
#             with open(path, 'wb') as f:
#                 np.save(f, np.array(self.data[column][index]))
#         elif path.suffix == '.txt':
#             with open(path, 'wt') as f:
#                 if isinstance(self.data[column][index], list):
#                     write_str = ''
#                     for elem in self.data[column][index]:
#                         write_str += str(elem) + '\n'

#                 else:
#                     write_str = str(self.data[column][index])
#                 f.write(write_str)

#         return path

#     def dump_column(
#         self,
#         column: str,
#         filename: Optional[str] = None,
#         filetype: Optional[str] = None,
#     ):
#         """Write column to disk."""
#         filetype = '.npy' if not filetype else filetype
#         if filetype != '.npy':
#             raise NotImplementedError(
#                 'currently, column dump only possible as numpy array npy ')

#         if filename is None:
#             timestr = time.strftime('%Y-%m-%d-%H-%M-%S')
#             filename = column + '_' + timestr + '.npy'
#         path = pathlib.Path(filename)

#         with open(path, 'wb') as f:
#             # print(dir())
#             vals = self.data[column].values

#             if vals.dtype == 'object':
#                 array = []
#                 if len(vals.shape) == 1:
#                     for val in vals:
#                         array.append(np.array(val, dtype=np.float64).ravel())
#                 a = np.array(array)
#                 # print(a.shape)
#             else:
#                 a = vals

#             # print( type(a), np.shape(a), a.dtype)
#             # a= np.asarray(*self.data[column].values,dtype=np.float64)
#             # print(dir(a), a.dtype)
#             np.save(f, a, allow_pickle=False)
#             # np.save(f, np.array(self.data[column].values))

#         return path

#     def split(self,
#               size: Optional[list] = None,
#               strategy: Optional[str] = 'random'):
#         """Partition data."""
#         from sklearn.model_selection import train_test_split

#         size = [0.7, 0.15, 0.15] if size is None else size
#         size = np.array(size)

#         if np.linalg.norm(size) > 1:
#             size = size / np.linalg.norm(size)

#         sets: List[pd.DataFrame] = []
#         pool = self.data
#         for i, s in enumerate(size):
#             if i > 0:
#                 s = size[i] / (1.0 - sum(size[0:i - 1]))
#                 s = min(s, 0.9999999999)
#                 s = max(s, 0)

#             if s <= 0:
#                 sets.append([])
#                 continue
#             train, test = train_test_split(pool, train_size=float(s))
#             sets.append(train)
#             pool = test

#         return sets

#     def compare_filenames(self, columns=[], suffix=True, full_path=False):
#         """Compare filenams in df columns."""
#         comp = []
#         for c in columns:
#             col = [pathlib.Path(file) for file in self.data[c]]
#             if not full_path:
#                 col = [pathlib.Path(file).name for file in col]
#             if not suffix:
#                 col = [pathlib.Path(file).stem for file in col]

#             comp.append(col)

#         if all([comp[0] == comp[j] for j in range(1, len(columns))]):
#             return True, 0
#         else:
#             warnings.warn(
#                 'Different filename order, returning permutation list'
#             )
#             perm = []
#             c1 = list(range(len(comp[0])))
#             perm.append(c1)
#             for i in range(1, len(columns)):
#              val = [comp[0].index(comp[i][j]) for j in range(len(comp[i]))]
#                 perm.append(val)

#             # tst = [ comp[1][perm[1][i]] for i in range(len(comp[1]))]
#             # print(tst)
#             return False, perm
#         # return comp

#     def permute_column(self, column, perm):
#         """Permute column data accorint to perm list."""
#         col = [self.data[column][perm[i]] for i in range(len(perm))]
#         self.data[column] = col

#     def adjust_ordering(self, ref_columns, order_columns, **kwargs):
#         """Adjsut ordering of columns according to ref_columns."""
#         test, perm = self.compare_filenames(columns=ref_columns, **kwargs)
#         if not test:
#             for col in order_columns:
#                 self.permute_column(col, perm[1])

#         test, perm = self.compare_filenames(columns=ref_columns, **kwargs)
#         return test

#     # def drop_duplicates(self):
#     #     """Drop duplicateds works also with numpy arrays as columns."""
#     #     df = pd.DataFrame(self.data)
#     #     reformated = []
#     #     for col in self.data.columns:
#     #         # column(array) to array(column)
#     #         vals.df[col].to_numpy()
#     #         # if vals.dtype=='object':
#     #         #
#     #         array = []
#     #         if len(vals.shape) == 1:
#     #             for val in vals:
#     #                 array.append(np.array(val, dtype=np.float64).ravel())
#     #             a = np.array(array)

#     #             # print(a)
#     #             # print(type(a))

#     #             # print(pd.DataFrame(a).drop_duplicates(ignore_index=True))
#     #             a['combine'] = pd.DataFrame(a).drop_duplicates
#     # (ignore_index=True).values.tolist()

#     #             self.data[col] = pd.DataFrame(self.data['combine'])
#     #             # self.data = self.data.rename({'combine':0}, axis=1)
#     #         else:

#     #             pass

#     @classmethod
#     def join_pairwise(cls, df1:pd.DataFrame, df2:pd.DataFrame,
# ptions:dict) -> pd.DataFrame:
#         print('hey, pandas!')
#         align = options.get('align', None)
#         if align == 'columns':
#             df = pd.concat([df1, df2], ignore_index=True)
#         elif align == 'rows':
#             df = df1.join(df2)

#         if list(df1.columns) == list(df2.columns):
#              df = pd.concat([df1, df2], ignore_index=True)

#         s1 = df1.shape
#         s2 = df2.shape
#         if s1[0]==s2[0] and s1[1]!=s2[1]:
#             df = df1.join(df2)
#         elif s1[0]!=s2[0] and s1[1]==s2[1]:
#             df = pd.concat([df1, df2], ignore_index=True)
#         else:
#             raise KeyError(f'could not join dataframes with shapes {s1} ' +
#                            '{s2} -- give align key to options dict')

#         print('joined df', df)
#         # if concat:
#         # if df1.columns != df2.columns:
#         # #     warnings.warn(f'different columns')
#         # print(df1.shape, df2.shape)
#         # print(df1.columns, df2.columns)
#         # print(df1)
#         # print(df2)

#                     # if list(data.columns) == list(ds2.data.columns):
#             #     data = pd.concat([data, ds2.data], ignore_index=True)
#             # else:
#             #     data = data.join(ds2.data)
