from __future__ import annotations

# from dataclasses import dataclass, replace
from pathlib import Path
from typing import Mapping, Sequence, Union

import numpy as np

try:
    import pandas as pd
except ImportError:
    pd = None

if pd is not None:
    input_type = Union[
        np.ndarray, np.generic, pd.DataFrame, Sequence, Mapping, Path
    ]
else:
    input_type = Union[  # type: ignore
        np.ndarray, np.generic, Sequence, Mapping, Path
    ]  # type: ignore

SUPPORTED_FORMATS = ['h5', 'pkl', 'parquet']


class Writer:
    """Collection of tools for dumping and converting data."""

    def write_data_arg(self, dataset, filepath, **kwargs):
        """Get data from input."""
        inferred_format = self.infer_format_from_filepath(filepath)

        if inferred_format == 'h5':
            self.write_hdf(dataset, filepath, **kwargs)
        elif inferred_format == 'pickle' or inferred_format == 'pkl':
            self.write_pickle(dataset, filepath, **kwargs)
        elif inferred_format == 'parquet':
            self.write_parquet(dataset, filepath, **kwargs)
        else:
            raise ValueError(
                f'Unknown format {inferred_format}, the following'
                ' formats are supported: {SUPPORTED_FORMATS}.'
            )

    def write_hdf(self, dataset, filepath, **kwargs):
        """Write data to HDF5 file."""
        # requires another package to be installed, pytables
        try:
            import tables  # noqa: F401
        except ImportError:
            raise ImportError(
                'The pytables package is required to write to HDF5 files.'
            )
        if isinstance(filepath, Path):
            filepath = str(filepath)
        if isinstance(filepath, str):
            self.serialize_units(dataset).to_hdf(filepath, **kwargs)
        else:
            raise TypeError(f'Unknown type: {type(filepath)}')

    def write_pickle(self, dataset, filepath, **kwargs):
        """Write data to pickle file."""
        if isinstance(filepath, Path):
            filepath = str(filepath)
        if isinstance(filepath, str):
            self.serialize_units(dataset).to_pickle(filepath, **kwargs)
        else:
            raise TypeError(f'Unknown type: {type(filepath)}')

    def write_parquet(self, dataset, filepath, **kwargs):
        """Write data to parquet file."""
        if isinstance(filepath, Path):
            filepath = str(filepath)
        if isinstance(filepath, str):
            self.serialize_units(dataset).to_parquet(filepath, **kwargs)
        else:
            raise TypeError(f'Unknown type: {type(filepath)}')

    def infer_format_from_filepath(self, filepath):
        """Infer the format from the filepath."""
        if isinstance(filepath, Path):
            filepath = str(filepath)
        if isinstance(filepath, str):
            return filepath.split('.')[-1]
        else:
            raise TypeError(f'Unknown type: {type(filepath)}')

    def serialize_units(self, dataset):
        """Set units to string."""
        data_out = dataset.data.copy()  # the entire dataset copied here!?
        data_out.attrs['units'] = dataset.units.__str__()
        for k, v in dataset.data.attrs['column_units'].items():
            data_out.attrs[k] = v.__str__()
        return data_out
