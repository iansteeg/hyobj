from abc import ABC, abstractmethod
from pathlib import Path
from typing import Mapping, NoReturn, Sequence, Tuple, Union

import numpy as np
import pandas as pd

input_type = Union[np.ndarray, np.generic, pd.DataFrame,  Sequence,  Mapping,
                   Path]
data_type = Union[np.ndarray, pd.DataFrame, list, dict]


class BaseInterface(ABC):
    """Base interface for backend of the DataSet class."""

    def __call__(self) -> NoReturn:
        """Make interface callable."""

    @abstractmethod
    def get_data(self, inp: input_type, options: dict) -> data_type:
        """Get data."""

    @abstractmethod
    def get_shape(self, data: data_type) -> Tuple[int]:
        """Get shape of data."""

    @abstractmethod
    def to_pandas(self, data: data_type, dimension: str,
                  name: str) -> data_type:
        """Convert to pandas DataFrame."""

    @abstractmethod
    def rearrange(
        self,
        data: data_type,
        orig_shape: Tuple[int],
        target_shape: Tuple[int],
    ) -> data_type:
        """Rearrange data."""
