from __future__ import annotations

import hashlib
import warnings
# from dataclasses import dataclass, replace
from pathlib import Path
from typing import Any, Dict, List, Mapping, Optional, Sequence, Union

import numpy as np

from ..units import Units

try:
    import pandas as pd

    pd.options.mode.copy_on_write = True
except ImportError:
    pd = None

from .numpy import NumpyInterface
from .pandas import PandasInterface
from .reader import Reader
from .writer import Writer

if pd is not None:
    input_type = Union[
        np.ndarray, np.generic, pd.DataFrame, Sequence, Mapping, Path
    ]
else:
    input_type = Union[  # type: ignore
        np.ndarray, np.generic, Sequence, Mapping, Path
    ]  # type: ignore


class DataSet(Reader, Writer):
    """hylleraas data set class."""

    def __init__(
        self,
        data: Optional[input_type] = None,
        # internal_format: Optional[str] = None,
        # parse: Optional[bool] = None,
        # parser: Optional[Callable] = None,
        # match: Optional[str] = None,
        # align: Optional[bool] = None,
        # shape: Optional[Tuple[int]] = None,
        # options: Optional[dict] = {},
        # dimension: Optional[str] = 'multi',
        **kwargs,
    ):
        """Initialize DataSet Class.

        Parameter
        ---------
        data: input_type, optional
            input, can be filename, array, dataframe etc.
        parser: Callable, optional
            optional parser to parse input file(s)
        match: str (or regex), optional
            if data points to a directory, all files are read who match
        align: str, optional
            force alignment ('row' or 'column')
        shape: tuple
            force re-shapeing of numpy arrays
        dimension: str, optional
            force dimension of new array:
                '1d': numpy arrays are entirely unrolled
                '2d': numpy arrays are mapped onto 2d data
                'multi': numpy arrays mapped onto 1d arrays of objects

        options: dict, optional
            further options, will overwrite the above

        """
        if pd is None:
            raise ImportError('pandas is required for DataSet')

        self.debug = kwargs.get('debug', False)
        self._units = self.get_kw('units', data, **kwargs)

        self.data = self.get_data(data, **kwargs)
        if self.data is None:
            raise TypeError(
                'could not generate DataSet from type(s) ' + f'{type(data)}'
            )

        if isinstance(self.data, list):
            if kwargs.get('parse'):
                kwargs['align'] = 'rows'
            self.data = self.combine(self.data, options=kwargs)
        # Check if self.data already has units
        if hasattr(self.data, 'attrs') and 'units' in self.data.attrs:
            # Now verify that they are strings
            if isinstance(self.data.attrs['units'], str):
                self._units = Units(self.data.attrs['units'])
            elif isinstance(self.data.attrs['units'], Units):
                self._units = self.data.attrs['units']
            else:
                raise ValueError('Unknown units format.')

            # column_units = self.check_units()
            # TODO: Set also column_units, without infering them
            column_units = self.data.attrs.get(
                'column_units', self.check_units()
            )
            # # Loop over and convert string to Units
            # for key, value in column_units.items():
            #     column_units[key] = Units(value)
            self.properties = {
                'units': self._units,
                'column_units': column_units,
            }
            # Change the units if specified in kwargs
            if 'units' in kwargs:
                self.change_units(kwargs['units'])
        else:
            self.properties = {
                'units': self._units,
                'column_units': self.check_units(),
            }

    @property
    def has_units(self) -> bool:
        """Check if DataSet has units."""
        return self._units is not None

    def change_units(self, new_units) -> None:
        """Change units of DataSet."""
        self.units = new_units  # type: ignore

    def get_dataset_id_by_size(self, key: str) -> List:
        """Get dataset id by size."""
        # check the shape of each row for key

        def get_shape_lambda(x):
            if isinstance(x, list):
                return len(x)
            elif isinstance(x, np.ndarray):
                return x.shape
            else:
                raise ValueError('could not get shape of ' + f'{type(x)}')

        shapes = self.data[key].apply(get_shape_lambda)
        # now get the unique shapes
        unique_shapes = shapes.unique()

        # we loop over shapes and get the indices in unique_shapes
        dataset_ids = []
        for shape in shapes:
            dataset_ids.append(np.where(unique_shapes == shape)[0][0])

        # turn into a string dataset_{id}
        dataset_ids = [f'dataset_{dataset_id}' for dataset_id in dataset_ids]

        return dataset_ids

    def set_dataset_id_by_size(self, key: str) -> None:
        """Set dataset id by size."""
        dataset_ids = self.get_dataset_id_by_size(key)
        self.data['dataset_id'] = dataset_ids

    def check_units(self) -> dict:
        """Check units."""
        if self._units is not None:
            self._units = Units(style=self._units)
            if self.debug:
                print(
                    '\nWARNING: using experimental feature '
                    + 'pandas.DataFrame.attrs to store units '
                    + 'consider using pandas.MultiIndex instead\n'
                )
            self.data.attrs['units'] = self.units
        elif self.debug:
            print('\nWARNING: no units set\n')

        column_units: dict = self.infer_column_units(self.data, self._units)
        self.data.attrs['column_units'] = column_units
        if self.debug:
            print('\n Units inferred from column names:\n', '-' * 40)
            for col, unit in column_units.items():
                print(f'{col}: {unit}')
                print(
                    '\nif this is not correct, please set the units '
                    + 'manually by setting\n'
                    + "\tself.properties['column_units'][<column>] = "
                    + 'str(hyobj.Units({<name>: {"unit": <unitname>,'
                    + '                          "value": <value>,'
                    + '                          "symbol": <symbol>}}))\n'
                    + '\nwhere <name> is the unit descriptor (e.g. length, '
                    + 'mass, energy, hessian, gradient) and <valuec> is the '
                    + 'conversion factor to the base unit '
                    + "(hyobj.Units('atomic'))\n"
                )

        return column_units

    def infer_column_units(self, data: pd.DataFrame, units: Units) -> dict:
        """Infer units of columns."""
        if units is None:
            return {col: None for col in data.columns}

        column_units: dict = {}
        for col in data.columns:
            try:
                name = Units.get_quantity_name(col)
            except ValueError:
                column_units[col] = None
                continue
            q = Units.get_quantity(name, units)
            if q is None:
                column_units[col] = None
                continue
            column_units[col] = str(Units({name: q}))
        return column_units

    def rename_columns(self, mappings: Dict):
        """Rename columns."""
        # rename pandas columns
        self.data.rename(columns=mappings, inplace=True)
        for key, value in mappings.items():
            # rename column units
            self.properties['column_units'][
                value
            ] = self.properties[  # type: ignore
                'column_units'
            ].pop(  # type: ignore
                key  # type: ignore
            )  # type: ignore

    def convert_columns_to_lists(self, **kwargs):
        """Convert columns to lists."""
        for col in self.data.columns:
            if isinstance(self.data[col][0], (np.ndarray)):
                self.data[col] = self.data[col].apply(lambda x: x.tolist())

    def convert_columns_to_numpy(self, **kwargs):
        """Convert columns to numpy arrays."""
        for col in self.data.columns:
            if isinstance(self.data[col][0], (list)):
                self.data[col] = self.data[col].apply(lambda x: np.array(x))

    def standardize_columns(self, **kwargs):
        """Standardize column names."""
        mappings = kwargs.get(
            'mappings',
            {
                'force': 'forces',
                'energies': 'energy',
                'pbc': 'box',
                'unit_cells': 'box',
                'unit_cell': 'box',
                'cell': 'box',
                'cells': 'box',
                'coordinate': 'coordinates',
                'coord': 'coordinates',
                'coords': 'coordinates',
                'position': 'coordinates',
                'positions': 'coordinates',
                'species': 'atoms',
                'species_label': 'atoms',
                'species_labels': 'atoms',
                'atom': 'atoms',
                'virials': 'virial',
            },
        )
        if 'mappings_sign_switch' in kwargs:
            mappings_sign_switch = kwargs['mappings_sign_switch']
        else:
            mappings_sign_switch = {
                'gradient': 'forces',
                'gradients': 'forces',
            }

        error_msg = 'Column name {} already exists, please rename manually.'

        for key, value in mappings.items():
            if key in self.data.columns:
                if value in self.data.columns:
                    raise ValueError(error_msg.format(value))
                self.rename_columns({key: value})

        for key, value in mappings_sign_switch.items():
            if key in self.data.columns:
                if value in self.data.columns:
                    raise ValueError(error_msg.format(value))
                self.rename_columns({key: value})
                # Check the if scalar, list or a numpy array, conserve the type
                if isinstance(self.data[value][0], (list)):
                    self.data[value] = self.data[value].apply(
                        lambda x: (-1 * np.array(x)).tolist()
                    )
                elif isinstance(self.data[value][0], (np.ndarray)):
                    self.data[value] = self.data[value].apply(
                        lambda x: (-1 * np.array(x))
                    )
                else:
                    self.data[value] = self.data[value].apply(lambda x: -1 * x)

        shaped_output = kwargs.pop('shaped_output', False)
        if 'shapes' in kwargs:
            shaped_output = True
        if shaped_output:
            standard_shapes = {
                'forces': (-1, 3),
                'box': (3, 3),
                'virial': (3, 3),
                'coordinates': (-1, 3),
            }
            shapes = kwargs.pop('shapes', standard_shapes)
            for column, shape in shapes.items():
                if column in self.data.columns:
                    try:
                        if isinstance(self.data[column][0], (list)):
                            self.data[column] = self.data[column].apply(
                                lambda x: np.array(x).reshape(shape).tolist()
                            )
                        elif isinstance(self.data[column][0], (np.ndarray)):
                            lambda x: x.reshape(shape)
                    except ValueError:
                        error_msg = f'Cannot reshape {column} of to {shape}.\n'
                        if shape[0] != -1:
                            error_msg += 'Should first dimension be -1?'
                        raise ValueError(error_msg)

    @property
    def units(self):
        """Get units of DataSet."""
        return self._units

    # @property
    # def hash(self) -> str:
    #     """Get hash of DataSet."""
    #     # TODO: Address potential memory issues with large datasets

    #     # Create a hash object
    #     hash_object = hashlib.sha256()

    #     # Convert the dataframe to a byte-string
    #     data_copy = self.data.copy()
    #     attributes_to_exclude = ['units', 'column_units']
    #     for attribute in attributes_to_exclude:
    #         data_copy.attrs.pop(attribute, None)

    #     df_bytes = data_copy.to_string().encode()

    #     # Hash the byte-string
    #     hash_object.update(df_bytes)

    #     # Get the hexadecimal representation of the hash
    #     hash_hex = hash_object.hexdigest()
    #     return hash_hex

    @property
    def hash(self) -> str:
        """Get hash of DataSet."""
        # TODO: Address potential memory issues with large datasets

        # Create a hash object
        hash_object = hashlib.sha256()

        # Convert the dataframe to a byte-string
        data = self.data.copy()
        attributes_to_exclude = ['units', 'column_units']
        for attribute in attributes_to_exclude:
            data.attrs.pop(attribute, None)

        def convert_to_hashable(col):
            if col.dtype == float:
                # Convert floats to strings with a fixed precision
                return col.apply(lambda x: f'{x:.10f}')
            elif col.dtype == object:
                # Convert lists, numpy arrays, and other objects
                # to a string representation
                return col.apply(
                    lambda x: (
                        repr(x)
                        if isinstance(x, (list, np.ndarray))
                        else str(x)
                    )
                )
            else:
                # Convert other types to string
                return col.astype(str)

        for _, row in data.apply(convert_to_hashable, axis=0).iterrows():
            for element in row:
                hash_object.update(str(element).encode('utf-8'))

        return hash_object.hexdigest()

    @property
    def systems(self) -> List:
        """Get molecules from DataSet."""
        from hyobj import Molecule, PeriodicSystem

        unit_cell_keys = ['unit_cell', 'cell', 'cells', 'box', 'boxes', 'pbc']
        coordinates_keys = [
            'coordinates',
            'coords',
            'coord',
            'pos',
            'positions',
            'position',
        ]
        atoms_keys = [
            'atoms',
            'atom',
            'species',
            'species_labels',
            'species_label',
        ]

        columns = self.data.columns
        atoms_key = None
        for key in atoms_keys:
            if key in columns:
                atoms_key = key
                break
        if atoms_key is None:
            raise ValueError('could not find atoms key')

        coordinates_key = None
        for key in coordinates_keys:
            if key in columns:
                coordinates_key = key
                break
        if coordinates_key is None:
            raise ValueError('could not find coordinates key')

        unit_cell_key = None
        for key in unit_cell_keys:
            if key in columns:
                unit_cell_key = key
                break

        systems = []
        for _, row in self.data.iterrows():
            coordinates = row[coordinates_key]
            atoms = row[atoms_key]
            if unit_cell_key is not None:
                unit_cell = row[unit_cell_key]
                systems.append(
                    PeriodicSystem(
                        {
                            'atoms': atoms,
                            'coordinates': coordinates,
                            'pbc': unit_cell,
                        },
                        units=self._units,
                    )
                )  # type: ignore
            else:
                systems.append(
                    Molecule(  # type: ignore
                        {'atoms': atoms, 'coordinates': coordinates},
                        units=self._units,
                    )
                )
        return systems

    @units.setter  # type: ignore
    def units(self, value):
        """Set units of DataSet."""
        # TODO: use UnitsTools instead
        # cols = self.data.columns
        value = Units(value)
        if self.debug:
            print('DEBUG: new units', value)

        prop = getattr(self, 'properties', {})
        if self.debug:
            print('DEBUG: prop', prop)
        if prop.get('column_units') is None:
            if self.debug:
                print('DEBUG: column_units is none, inferring...')
            self.properties = prop
            self.properties['column_units'] = self.check_units()
            # self.properties['units'] = value
            # self._units = value
            # return

        for col in self.data.columns:
            col_unit = self.properties['column_units'].get(col)
            if col_unit is None:
                if self.debug:
                    print('DEBUG: col_unit is none, hopping over...')
                continue
            u = Units(col_unit)
            name = [
                k for k in u.__dict__.keys() if k not in ['style', 'debug']
            ][0]
            u_new = Units({name: getattr(value, name)})

            if self.debug:
                print('DEBUG: old unit and name', u, name)
                print('DEBUG: new units', u_new)

            val_old = getattr(u, name).value
            val_new = getattr(value, name).value
            if self.debug:
                print('DEBUG:', val_old, val_new)

            fac = val_new / val_old
            self.data.loc[:, col] = self._column_multiply(self.data[col], fac)
            self.properties['column_units'][col] = str(u_new)

        self._units = value
        self.properties.update({'units': value})
        self.data.attrs['units'] = self.properties['units']
        self.data.attrs['column_units'] = self.properties['column_units']

    def _column_multiply(self, obj: Any, factor: float) -> None:
        """Multiply column by factor."""
        try:
            obj *= factor
        except TypeError:  # pragma: no cover
            try:
                obj = np.array(obj) * factor
            except TypeError as e:
                if isinstance(obj, pd.Series):
                    for i, v in obj.items():
                        if isinstance(v, list):
                            val = np.array(v) * factor
                            obj._set_value(i, val.tolist())
                    return obj
                raise NotImplementedError(
                    f'Cannot convert units of {obj} '
                    + f'with type {type(obj)}'
                ) from e
        else:
            return obj

    def get_kw(self, kw: str, data: input_type, **kwargs) -> Any:
        """Get value from data."""
        if kwargs.get(kw) is not None:
            return kwargs.get(kw)
        if isinstance(data, DataSet):
            return getattr(data, kw, None)
        elif isinstance(data, dict):
            return data.pop(kw, None)
        return None

    def append(self, data: input_type, options: dict = None) -> None:
        """Append data to existing data set.

        Parameter
        ---------
        data: input_type
            input, can be filename, array, dataframe etc.
        options: dict, optional
            further options, will overwrite the above

        """
        # print('this is the input data:', data, type(data))
        # print('current data', self.data)
        options = options or {}
        data_list = []
        if isinstance(data, pd.DataFrame):
            if hasattr(data, 'attrs'):
                if data.attrs['units'] != self._units:
                    raise ValueError('Units do not match')
            data_list = [self.data, data]
        elif isinstance(data, DataSet):
            # print('DAtaset found', type(data.data), type(data))
            if data.units != self._units:
                raise ValueError('Units do not match')
            data_list = [self.data, data.data]
        else:
            try:
                new_data = DataSet(data)
            except TypeError:
                raise TypeError(
                    'could generate DataSet from type(s) ' + f'{type(data)}'
                )
            else:
                if new_data._units != self._units:
                    raise ValueError(
                        f'Units do not match, {new_data._units}'
                        + f' != {self._units}'
                    )
                data_list = [self.data, new_data.data]

        if not all([isinstance(d, pd.DataFrame) for d in data_list]):
            raise TypeError(
                'could not append data of type(s) '
                + f'{[type(d) for d in data_list]}'
            )
        df = DataSet.combine_cls(
            [d for d in data_list if not d.empty], options=options
        )
        self.data = df
        return None

    @classmethod
    def combine_cls(
        cls, data_list: list, options: Optional[dict] = None
    ) -> pd.DataFrame:
        """Combine a list of DataFrames. Interface to pandas.concat."""
        interface = PandasInterface()
        options = options or {}

        if any(isinstance(i, list) for i in data_list):
            return interface.join(
                [d for sublist in data_list for d in sublist if not d.empty],
                options,
            )
        else:
            return interface.join(
                [d for d in data_list if not d.empty], options
            )

    def combine(
        self, data_list: list, options: Optional[dict] = None
    ) -> pd.DataFrame:
        """Combine a list of DataFrames. Interface to pandas.concat."""
        interface = PandasInterface()
        options = options or {}

        if any(isinstance(i, list) for i in data_list):
            return interface.join(
                [d for sublist in data_list for d in sublist], options
            )
        else:
            return interface.join(data_list, options)

    def get_data_old(self, inp: input_type, options: dict) -> pd.DataFrame:
        """Get data from input args.

        Parameter
        ---------
        inp: input_type
            input, can be filename, array, dataframe, dataset object etc.

        options: dict
            options

        Returns
        -------
        pd.DataFrame
            data

        """
        if isinstance(inp, (str, Path)):
            return self.get_data_from_str(str(inp), options)  # type: ignore

        elif isinstance(inp, self.__class__):
            return inp.data
        elif isinstance(inp, dict):
            if all([isinstance(v, (list, np.ndarray)) for v in inp.values()]):
                s = set([len(v) for v in inp.values()])
                if len(s) == 1:
                    return self.get_data_from_dictlist(inp, list(s)[0])

            df_list = []
            numpy_if = NumpyInterface()
            pandas_if = PandasInterface()

            for k, v in inp.items():
                if isinstance(v, (list, np.ndarray)):
                    df = numpy_if.to_pandas(
                        data=np.array(v),
                        name=k,
                        dimension=options.get('dimension', 'multi'),
                    )
                elif isinstance(v, (str, int, float)):
                    # df = pd.DataFrame({k: [v]})
                    df = pd.Series(v, name=k).to_frame()
                else:
                    warnings.warn(f'could not read data from {type(v)}')
                    continue
                df_list.append(df)
            return pandas_if.join(df_list)

        try:
            data = pd.DataFrame(inp)
        except ValueError:
            raise TypeError('could not read data from ' + f' {type(inp)}')
        else:
            return data

    def get_data_from_dictlist(self, inp, shape, **kwargs):
        """Get data from dict of lists."""

    #     pass
    #     print('shape', shape)
    #     df = pd.DataFrame()
    #     for k, v in input.items():
    #         _data = np.array(v).ravel().reshape(shape, -1)
    #         df1 = pd.DataFrame({k: [_data[0, :]]})
    #         for i in range(1, length):
    #             df2 = pd.DataFrame({k: [_data[i, :]]})

    #             df1 = pd.concat([df1, df2], ignore_index=True)
    #         print(df1)
    #         df = pd.concat([df, df1], ignore_index=True)

    #         #     if dim == 1:
    #         # s = pd.Series([data.ravel()], name=name)
    #         # df = pd.DataFrame(s)
    #         # return df

    #         # _data = data.ravel().reshape(length, -1)
    #         # df = pd.DataFrame({name: [_data[0, :]]})
    #         # for i in range(1, length):
    #         #     df2 = pd.DataFrame({name: [_data[i, :]]})

    #         #     df = pd.concat([df, df2], ignore_index=True)
    #     print('construc', df)
    #     return df
    @property
    def columns(self):
        """Columns of data."""
        if self.data_type == 'pandas':
            return self.data.columns
        else:
            return None

    @property
    def shape(self):
        """Shape of data."""
        if self.data_type == 'pandas':
            return self.data.shape
        else:
            return None

    def __repr__(self):
        """Dataclass-like representation."""
        keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(keywords))

    def __iter__(self):
        """Generate Iterator."""
        self.current_idx = -1
        return self

    def __next__(self):
        """Next iterable."""
        self.current_idx += 1
        if self.current_idx >= len(self):
            raise StopIteration
        return DataSet(self[self.current_idx])

    def to_ase_list(self):
        """Create a list of ase atoms objects from dataset."""
        import ase
        from ase.calculators.singlepoint import SinglePointCalculator

        self.standardize_columns(shaped_output=True)
        ase_list = []
        for i in range(len(self)):
            df = self.data.iloc[i]
            box = None if 'box' not in self.data.columns else df['box']
            if box is not None and np.shape(box) == (9,):
                box = np.array(box).reshape(3, 3)
            forces = (
                None if 'forces' not in self.data.columns else df['forces']
            )

            # Musy have stress in dataset
            stress = (
                None if 'stress' not in self.data.columns else df['stress']
            )

            energy = (
                None if 'energy' not in self.data.columns else df['energy']
            )
            symbols = df.atoms
            positions = df.coordinates
            kwargs = {}
            if box is not None:
                kwargs['cell'] = box
                kwargs['pbc'] = [True, True, True]
            else:
                kwargs['pbc'] = [False, False, False]
            atoms = ase.Atoms(symbols=symbols, positions=positions, **kwargs)

            if 'atom_types' in self.data.columns:
                atoms.arrays['atom_types'] = df['atom_types']

            if stress is not None and np.shape(stress) == (9,):
                stress = np.array(stress).reshape(3, 3)

            if stress is not None and np.shape(stress) == (3, 3):
                stress = np.array(stress)
                stress = np.array(
                    [
                        stress[0, 0],
                        stress[1, 1],
                        stress[2, 2],
                        stress[1, 2],
                        stress[2, 0],
                        stress[0, 1],
                    ]
                )
            atoms.calc = SinglePointCalculator(
                atoms, forces=forces, stress=stress, energy=energy
            )
            ase_list.append(atoms)
        return ase_list

    def to_extxyz_string(self):
        """Create extxyz string from dataset."""
        import tempfile

        import ase.io

        f = tempfile.TemporaryFile(mode='w+', suffix='xyz')
        ase.io.write(f, self.to_ase_list(), format='extxyz')
        f.seek(0)
        return f.read()

    # def bind_methods(self, _instance):
    #     """Bind class methods to DataSet."""
    #     self.__dict__.update(_instance.__dict__)
    #     # bind classmethods
    #     _method_list_str = [
    #         method for method in dir(_instance)
    #         if method.startswith('_') is False
    #     ]
    #     _method_list = [
    #         getattr(_instance, method) for method in _method_list_str
    #     ]
    #     for method in _method_list:
    #         # print('METHOD TO BIND', method)
    #         if not callable(method):
    #             continue
    #         name = (method.fget.__name__
    #                 if isinstance(method, property) else method.__name__)
    #         try:
    #             setattr(self, name, method)
    #         except Exception:
    #             raise Exception(
    #                 f'Could not bind method {method} with name {name}')

    # def check(self, *objs):
    #     """Compare data type of objeces."""
    #     def _all_bases(o):
    #         for b in o.__bases__:
    #             if b is not object:
    #                 yield b
    #             yield from _all_bases(b)

    #     s = [(i.__class__, *_all_bases(i.__class__)) for i in objs]
    #     return len(set(*s[:1]).intersection(*s[1:])) > 0

    def __add__(self, ds2):
        """Add two DataSet instances."""
        if self._units != ds2._units:
            raise ValueError(
                'units must be the same! '
                + f'original units: {self._units}\n'
                + f'units to add: {ds2._units}'
            )
        interface = PandasInterface()
        options = {}
        if self.data.empty:
            return ds2
        if ds2.data.empty:
            return self
        col1 = list(self.data.columns)
        col2 = list(ds2.data.columns)
        if len(col1) == len(col2):
            if all([col1[i] == col2[i] for i in range(len(col1))]):
                options['align'] = 'rows'
        else:
            options['align'] = 'columns'

        data = interface.join([self.data, ds2.data], options)
        return DataSet(data, units=self._units)

    def __iadd__(self, ds2):
        """Add two DataSet instances."""
        ds = self + ds2
        self.data = ds.data
        return self

    def __len__(self) -> int:
        """Length of dataframe."""
        return len(self.data.index)

    def __setitem__(self, indices: tuple, newvalue: Any) -> None:
        """Set value of DataSet.

        Parameters
        ----------
        indices : tuple
            Indices to use setter.
        newvalue: Any
            new value

        """
        if len(indices) != 2:
            raise ValueError('two indices expected')
        if not isinstance(indices[0], int):
            raise TypeError('row index must be int')
        if not isinstance(indices[1], (int, str)):
            raise TypeError('column index must be int or str')

        if isinstance(indices[1], str):
            self.data.at[indices[0], indices[1]] = newvalue
        elif isinstance(indices[1], int):
            self.data.iat[indices[0], indices[1]] = newvalue

    def __getitem__(self, indices: Union[int, str, tuple, list]) -> DataSet:
        """Create a new DataSet from a subset of the atoms in this DataSet.

        Parameters
        ----------
        indices : int or tuple or list or slice
            Indices to use for the new DataSet.

        Returns
        -------
        A new independent DataSet instance.

        """
        if isinstance(indices, int):
            df = self.data.iloc[indices].to_frame().T
            return DataSet(df, units=self._units, debug=self.debug)
        elif isinstance(indices, slice):
            df = self.data.iloc[indices]
            return DataSet(df, units=self._units, debug=self.debug)
        elif isinstance(indices, str):
            return self.data[indices].values[0]
        elif isinstance(indices, (tuple, list)):
            if not isinstance(indices[0], int):
                raise TypeError('row index must be int')
            if not isinstance(indices[1], (int, str)):
                raise TypeError('column index must be int or str')
            if isinstance(indices[1], str):
                return self.data.at[indices[0], indices[1]]
            elif isinstance(indices[1], int):
                return self.data.iat[indices[0], indices[1]]

        else:
            raise TypeError('use pandas functionalities on data attribute')

    def get_duplicates(
        self,
        col: pd.Series,
        rtol: Optional[float] = 1e-05,
        atol: Optional[float] = 1e-08,
    ) -> list:
        """Get duplicates in column.

        Parameter
        ---------
        col: pandas.Series
            column
        rtol: float, optional
            rtol as used by numpy.allclose, default 1e-05
        atol: float, optional
            atol as used by numpy.allclose, default 1e-08

        Returns
        -------
        list
            list of (tuples) of duplicates

        """
        if col.dtype == 'object':
            if all(isinstance(col[i], str) for i in range(len(col))):
                s = pd.Series(col, dtype=pd.StringDtype()).duplicated(
                    keep='first'
                )
                duplicates = s[s].index.values
            elif all(isinstance(col[i], np.ndarray) for i in range(len(col))):
                # alist = []
                # for i in range(len(col)):
                #     alist.append(np.array(col[i], dtype=np.float64).ravel())
                alist = [
                    np.array(item, dtype=np.float64).ravel() for item in col
                ]
                # duplicates = []
                # for i in range(len(col)):
                #     for j in range(i + 1, len(col)):
                #         is_duplicate = np.allclose(
                #             alist[i], alist[j], rtol=rtol, atol=atol
                #         )
                #         if is_duplicate:
                #             duplicates.append((i, j))
                duplicates = [
                    (i, j)
                    for i in range(len(col))
                    for j in range(i + 1, len(col))
                    if np.allclose(alist[i], alist[j], rtol=rtol, atol=atol)
                ]

            elif all(isinstance(col[i], list) for i in range(len(col))):
                # duplicates = []
                # for i in range(len(col)):
                #     for j in range(i + 1, len(col)):
                #         is_duplicate = all(
                #             [
                #                 col[i][k] == col[j][k]
                #                 for k in range(len(col[i]))
                #             ]
                #         )
                #         if is_duplicate:
                #             duplicates.append((i, j))
                duplicates = [
                    (i, j)
                    for i in range(len(col))
                    for j in range(i + 1, len(col))
                    if all(col[i][k] == col[j][k] for k in range(len(col[i])))
                ]

            else:
                raise TypeError(
                    'duplicate comparison currently only allowed for '
                    + f'{type(str), } {type(list)} and {type(np.ndarray)}'
                )

        return duplicates

    def write_data(self, filepath, **kwargs):
        """Write data to file."""
        self.write_data_arg(self, filepath, **kwargs)
