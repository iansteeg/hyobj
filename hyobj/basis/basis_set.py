import warnings
from itertools import chain, combinations_with_replacement
from pathlib import Path
from typing import Any, Optional

import numpy as np

try:
    from more_itertools import distinct_permutations
except ImportError:
    distinct_permutations = None
try:
    from qcelemental import periodictable
except ImportError:
    periodictable = None
try:
    from scipy.special import factorial
except ImportError:
    factorial = None

# from sympy import *
try:
    from sympy import (KroneckerDelta, binomial, exp, integrate, lambdify, oo,
                       pi, sqrt, symbols)
    from sympy.functions.elementary.integers import floor
except ImportError:
    sympy = None
    KroneckerDelta = None
    binomial = None
    exp = None
    integrate = None
    lambdify = None
    oo = None
    pi = None
    sqrt = None
    symbols = None
    floor = None
else:
    sympy = True

from ..molecule import Molecule

try:
    import basis_set_exchange as bse
except ImportError:
    bse = None
else:
    bse.memo.memoize_enabled = False


class Basis:
    """Main Basis class."""

    def __init__(
        self,
        basis_set: str,
        atoms: Optional[Any] = None,
        options: Optional[dict] = None,
    ):
        """Initialise Basis class."""
        atoms = atoms or []
        if bse is None:
            raise ImportError('basis_set_exchange not installed')
        if periodictable is None:
            raise ImportError('qcelemental not installed')
        if distinct_permutations is None:
            raise ImportError('more_itertools not installed')
        if sympy is None:
            raise ImportError('sympy not installed')
        if factorial is None:
            raise ImportError('scipy not installed')

        self.options: dict = {}
        # self.options: dict = {'p-order': 'yzx'}
        # self.options: dict = {'p-order': 'xyz'}
        if options:
            self.options.update(options)

        if Path(basis_set).exists():
            try:
                parser = self.options.get('parser',
                                          bse.read_formatted_basis_file)
            except Exception:
                raise RuntimeError(f'could not get parser for {basis_set}')

            self.basis_dict = parser(str(Path(basis_set)))
        else:
            try:
                self.basis_dict = bse.get_basis(basis_set,
                                                uncontract_spdf=True)
            except Exception:
                raise RuntimeError(f'could not get basis {basis_set}')
        self.name = self.basis_dict['name']

        if atoms is None:
            self.atoms = [
                periodictable.to_symbol(e)
                for e in self.basis_dict['elements'].keys()
            ]
        else:
            self.atoms = atoms

        assert self.check_atoms(atoms, self.atoms)

        self.lmax_cartesian, self.lmax_spherical = self.get_lmax()

        self.gtos_spherical = self.construct_gtos_spherical(
            self.lmax_spherical)
        self.gtos_cartesian = self.construct_gtos_cartesian(
            self.lmax_cartesian)

    def __repr__(self):
        """Dataclass-like representation."""
        keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(keywords))

    def check_atoms(self, atoms, atoms_bas):
        """Check all atoms have a basis."""
        if atoms:
            if isinstance(atoms, list):
                atoms = list(set(atoms))
            elif isinstance(atoms, Molecule):
                atoms = list(set(atoms.atoms))
            else:
                try:
                    atoms = list(set(atoms.elements))
                except Exception:
                    atoms = []

        for atom in atoms:
            if atom not in atoms_bas:
                warnings.warn(
                    f'could not find atom {atom} in basis {self.name}')
                return False

        return True

    # def gen_angmom_order_cartesian(self, ll: int) -> dict:
    #     """Generate dfitionary of ml components in given order."""
    #     pord = self.options.get('p-order')
    #     if ll == 0:
    #         d = {ll: [0]}
    #     elif ll == 1:
    #         # if pord is not None:
    #         #     if pord == 'yzx':
    #         #         d = {ll: list(range(-ll, ll + 1, 1))}
    #         #     elif pord == 'xyz':
    #         #         d = {ll: [1, -1, 0]}
    #         #     else:
    #         #         d = {ll: [1, -1, 0]}

    #         # else:
    #             d = {ll: [1, -1, 0]}
    #     else:
    #         d = {ll: []}
    #     return d

    def construct_bf(self, atoms, coordinates):
        """Construct basis."""
        if atoms is None or coordinates is None:
            return {}

        x, y, z = symbols('x y z')
        a = symbols('a', positive=True)

        order = []
        basis_functions = []

        atypes = sorted(set(atoms))
        ao_prototype = {}
        for atom in atypes:
            ao_prototype[atom] = None
            atom_str = str(periodictable.to_Z(atom))
            shells = self.basis_dict['elements'][atom_str]['electron_shells']
            shells = [shells] if not isinstance(shells, list) else shells
            bf = []
            for shell in shells:
                exponents = shell['exponents']
                for _, ll in enumerate(shell['angular_momentum']):
                    if shell['function_type'] == 'gto':
                        prototypes = self.gtos_cartesian[ll]
                        # d = self.gen_angmom_order_cartesian(ll)
                    elif shell['function_type'] == 'gto_spherical':
                        prototypes = self.gtos_spherical[ll]
                        # d = {ll: list(range(-ll, ll + 1, 1))}
                    # shlist.append(d)
                    for coefficients in shell['coefficients']:
                        for _, m in enumerate(prototypes):
                            function = 0
                            # prototype = prototypes[m].subs([(x, x - rx),
                            #                                 (y, y - ry),
                            #                                 (z, z - rz)])
                            for j, alpha in enumerate(exponents):
                                coeff = float(coefficients[j])
                                term = m.subs(a, alpha)
                                function += coeff * term

                            bf.append(function)
            # order.append((atom, shlist))
            ao_prototype[atom] = bf

        for i, atom in enumerate(atoms):
            rx, ry, rz = coordinates[i]
            bf = ao_prototype[atom]
            for f in bf:
                basis_functions.append(
                    f.subs([(x, x - rx), (y, y - ry), (z, z - rz)]))

            # atom_str = str(periodictable.to_Z(atom))
            # shells = self.basis_dict['elements'][atom_str]['electron_shells']
            # shells = [shells] if not isinstance(shells, list) else shells
            # shlist = []
            # for shell in shells:
            #     for ii, ll in enumerate(shell['angular_momentum']):
            #         if shell['function_type'] == 'gto':
            #             prototypes = self.gtos_cartesian[ll]
            #             d = self.gen_angmom_order_cartesian(ll)
            #         elif shell['function_type'] == 'gto_spherical':
            #             prototypes = self.gtos_spherical[ll]
            #             d = {ll: list(range(-ll, ll + 1, 1))}

            #         shlist.append(d)
            #         exponents = shell['exponents']
            #         coefficients = shell['coefficients'][ii]
            #         for m in range(len(prototypes)):
            #             function = 0
            #             prototype = prototypes[m].subs([(x, x - rx),
            #                                             (y, y - ry),
            #                                             (z, z - rz)])

            #             for j, alpha in enumerate(exponents):
            #                 coeff = float(coefficients[j])
            #                 term = prototype.subs(a, alpha)
            #                 function += coeff * term

            #             basis_functions.append(function)
            # order.append((atom, shlist))

        output = {
            'basis_functions_symbolic':
            basis_functions,
            'basis_functions_numeric':
            [lambdify([x, y, z], func, 'numpy') for func in basis_functions],
            'ordering':
            order,
        }

        return output

    def get_lmax(self):
        """Find maximum l quantum number."""
        lmax_cartesian = 0
        lmax_spherical = 0
        for atom in self.atoms:
            for func in self.basis_dict['elements'][str(
                    periodictable.to_Z(atom))]['electron_shells']:
                ll = max(func['angular_momentum'])
                if func['function_type'] == 'gto_spherical':
                    lmax_spherical = max(ll, lmax_spherical)
                elif func['function_type'] == 'gto':
                    lmax_cartesian = max(ll, lmax_cartesian)

        return lmax_cartesian, lmax_spherical

    def gen_permutations_from_lmax(self, lmax: int) -> list:
        """Generate all permutations of angular momentum up to lmax.

        Parameters
        ----------
        lmax : int
            Maximum angular momentum.

        Returns
        -------
        permutations : list
            List of lists of permutations.

        """
        permutations = []
        for _ in range(lmax + 1):
            lst = list(range(lmax + 1))
            c = [
                pair for pair in combinations_with_replacement(lst, 3)
                if sum(list(pair)) == lmax
            ]
            p = [list(distinct_permutations(list(pair))) for pair in c]
            lst = list(chain.from_iterable(p))
            permutations.append(lst)
        return permutations

    def construct_gtos_cartesian(self, lmax):
        """Construct all cartesian gtos up to lmax."""
        perms = [[(0, 0, 0)], [(0, 0, 1), (0, 1, 0), (1, 0, 0)],
                 [(0, 0, 2), (0, 2, 0), (2, 0, 0), (0, 1, 1), (1, 0, 1),
                  (1, 1, 0)],
                 [(0, 0, 3), (0, 3, 0), (3, 0, 0), (0, 1, 2), (0, 2, 1),
                  (1, 0, 2), (1, 2, 0), (2, 0, 1), (2, 1, 0), (1, 1, 1)],
                 [(0, 0, 4), (0, 4, 0), (4, 0, 0), (0, 1, 3), (0, 3, 1),
                  (1, 0, 3), (1, 3, 0), (3, 0, 1), (3, 1, 0), (0, 2, 2),
                  (2, 0, 2), (2, 2, 0), (1, 1, 2), (1, 2, 1), (2, 1, 1)],
                 [(0, 0, 5), (0, 5, 0), (5, 0, 0), (0, 1, 4), (0, 4, 1),
                  (1, 0, 4), (1, 4, 0), (4, 0, 1), (4, 1, 0), (0, 2, 3),
                  (0, 3, 2), (2, 0, 3), (2, 3, 0), (3, 0, 2), (3, 2, 0),
                  (1, 1, 3), (1, 3, 1), (3, 1, 1), (1, 2, 2), (2, 1, 2),
                  (2, 2, 1)],
                 [(0, 0, 6), (0, 6, 0), (6, 0, 0), (0, 1, 5), (0, 5, 1),
                  (1, 0, 5), (1, 5, 0), (5, 0, 1), (5, 1, 0), (0, 2, 4),
                  (0, 4, 2), (2, 0, 4), (2, 4, 0), (4, 0, 2), (4, 2, 0),
                  (0, 3, 3), (3, 0, 3), (3, 3, 0), (1, 1, 4), (1, 4, 1),
                  (4, 1, 1), (1, 2, 3), (1, 3, 2), (2, 1, 3), (2, 3, 1),
                  (3, 1, 2), (3, 2, 1), (2, 2, 2)]]
        if lmax > len(perms):
            perms = self.gen_permutations_from_lmax(lmax)
        gtos_cartesian = []
        for ll in range(lmax + 1):
            p = perms[ll]
            # if ll == 1 and self.options['p-order'] == 'yzx':
            #     gtos =  [self.gto_cartesian(j, i, k) for i, j, k in p]
            gtos_cartesian.append(
                [self.gto_cartesian(k, j, i) for i, j, k in p])
        # for ll in range(0, lmax + 1):
        #     gtos_cartesian_l = []
        #     c =  [pair for pair in combinations_with_replacement
        # (list(range(ll)), 3) if sum(list(pair)) == ll]
        #     p = [list(distinct_permutations(list(pair))) for pair in c]
        #     lst = list(chain.from_iterable(p))
        #   gtos_cartesian_l = [self.gto_cartesian(i, j, k) for i, j, k in lst]
        #     gtos_cartesian.append(gtos_cartesian_l)
        # return [self.gto_cartesian(i, j, k) for i, j, k in lst]

        return gtos_cartesian

        # for ll in range(0, lmax + 1):
        #     gtos_cartesian_l = []

        #     for i in range(0, ll + 1):
        #         for j in range(0, ll + 1):
        #             k = ll - i - j
        #             # for k in range(0, ll + 1):
        #             #     if i + j + k == ll:

        #                    # if ll == 1 and self.options['p-order'] == 'yzx':
        #                     #     gto = self.gto_cartesian(i, k, j)
        #                     # else:
        #                     #     # xyz
        #                     #     gto = self.gto_cartesian(k, j, i)

        #                     # gtos_cartesian_l.append(gto)

        #             if ll == 1 and self.options['p-order'] == 'yzx':
        #                 gto = self.gto_cartesian(i, k, j)
        #             else:
        #                 # xyz
        #                 gto = self.gto_cartesian(k, j, i)

        #             gtos_cartesian_l.append(gto)

        #     gtos_cartesian.append(gtos_cartesian_l)
        # return gtos_cartesian

    def gto_cartesian(self, i, j, k):
        """Construct cartesian gto."""
        x, y, z = symbols('x,y,z')
        a = symbols('a', positive=True)
        n = (8 * a)**(i + j + k)
        n *= factorial(i) / factorial(2 * i)
        n *= factorial(j) / factorial(2 * j)
        n *= factorial(k) / factorial(2 * k)
        n = sqrt(n)
        n *= (2 * a / np.pi)**(3 / 4)
        gto = x**i * y**j * z**k * exp(-a * (x**2 + y**2 + z**2))
        return (n * gto)

    def construct_gtos_spherical(self, lmax):
        """Construct all spherical gtos up to lmax."""
        gtos_spherical = []
        for ll in range(0, lmax + 1):
            gtos_spherical_m = []
            for m in range(-ll, ll + 1):
                gto = self.gto_spherical(ll, m)
                gtos_spherical_m.append(gto)

            gtos_spherical.append(gtos_spherical_m)
        return gtos_spherical

    def gto_spherical_normalization_factor(self, gto, ll, m):
        """Get gto normalization prefactor."""
        lmax = 7
        nmat = [[0 for i in range(-lmax, lmax + 1)]
                for j in range(0, lmax + 1)]
        a = symbols('a', positive=True)

        nmat[0][0] = 0.5 * 2**(1 / 4) * pi**(3 / 4) / a**(3 / 4)
        nmat[1][0] = 0.39580837177154 * sqrt(pi) / a**1.25
        nmat[1][1] = 0.25 * 2**(1 / 4) * pi**(3 / 4) / a**(5 / 4)
        nmat[1][2] = 0.39580837177154 * sqrt(pi) / a**1.25
        nmat[2][0] = (0.383747515479933 * 2**(1 / 4) * pi**(1 / 4) / a**1.75)
        nmat[2][1] = 0.342780104984709 * sqrt(pi) / a**1.75
        nmat[2][2] = 0.21650635094611 * sqrt(
            -0.417771379105167 * pi / a**3.5 + 0.523598775598299 * sqrt(2) *
            sqrt(pi) / a**3.5 + sqrt(2) * pi**(3 / 2) / a**(7 / 2))
        nmat[2][3] = 0.342780104984709 * sqrt(pi) / a**1.75
        nmat[2][4] = 0.419818175595093 * sqrt(-0.417771379105167 * sqrt(2) *
                                              sqrt(pi) / a**3.5 + pi / a**3.5)
        nmat[3][0] = 0.302977670863156 * sqrt(0.7519884823893 * sqrt(2) *
                                              sqrt(pi) / a**4.5 + pi / a**4.5)
        nmat[3][1] = (0.429042765404892 * 2**(1 / 4) * pi**(1 / 4) / a**2.25)
        nmat[3][2] = 0.378418989619963 * sqrt(0.0321362599311666 * sqrt(2) *
                                              sqrt(pi) / a**4.5 + pi / a**4.5)
        nmat[3][3] = 0.242061459137964 * sqrt(
            -0.7519884823893 * pi / a**4.5 + 0.942477796076938 * sqrt(2) *
            sqrt(pi) / a**4.5 + sqrt(2) * pi**(3 / 2) / a**(9 / 2))
        nmat[3][4] = 0.378418989619963 * sqrt(0.0321362599311666 * sqrt(2) *
                                              sqrt(pi) / a**4.5 + pi / a**4.5)
        nmat[3][5] = 0.469370989410285 * sqrt(-0.417771379105167 * sqrt(2) *
                                              sqrt(pi) / a**4.5 + pi / a**4.5)
        nmat[3][6] = 0.302977670863156 * sqrt(0.7519884823893 * sqrt(2) *
                                              sqrt(pi) / a**4.5 + pi / a**4.5)
        nmat[4][0] = (0.567570229536385 * 2**(1 / 4) * pi**(1 / 4) / a**2.75)
        nmat[4][1] = 0.400801784954745 * sqrt(0.7519884823893 * sqrt(2) *
                                              sqrt(pi) / a**5.5 + pi / a**5.5)
        nmat[4][2] = (0.567570229536385 * 2**(1 / 4) * pi**(1 / 4) / a**2.75)
        nmat[4][3] = 0.493211914860545 * sqrt(0.0709423096593678 * sqrt(2) *
                                              sqrt(pi) / a**5.5 + pi / a**5.5)
        nmat[4][4] = 0.360960514697988 * sqrt(
            -0.797884560802866 * pi / a**5.5 + sqrt(2) * sqrt(pi) / a**5.5 +
            0.786991330079129 * sqrt(2) * pi**(3 / 2) / a**(11 / 2))
        nmat[4][5] = 0.493211914860545 * sqrt(0.0709423096593678 * sqrt(2) *
                                              sqrt(pi) / a**5.5 + pi / a**5.5)
        nmat[4][6] = 0.617212440350079 * sqrt(-0.407704598885765 * sqrt(2) *
                                              sqrt(pi) / a**5.5 + pi / a**5.5)
        nmat[4][7] = 0.400801784954745 * sqrt(0.7519884823893 * sqrt(2) *
                                              sqrt(pi) / a**5.5 + pi / a**5.5)
        nmat[4][8] = 0.530210924010522 * sqrt(-0.107426926055614 * sqrt(2) *
                                              sqrt(pi) / a**5.5 + pi / a**5.5)
        nmat[5][0] = 0.606684026001809 * sqrt(0.773334266624316 * pi / a**6.5 +
                                              sqrt(2) * sqrt(pi) / a**6.5)
        nmat[5][1] = (0.851355344304577 * 2**(1 / 4) * pi**(1 / 4) / a**3.25)
        nmat[5][2] = 0.599111531348662 * sqrt(0.766011577631992 * sqrt(2) *
                                              sqrt(pi) / a**6.5 + pi / a**6.5)
        nmat[5][3] = (0.851355344304578 * 2**(1 / 4) * pi**(1 / 4) / a**3.25)
        nmat[5][4] = 0.728978537063334 * sqrt(0.110616389447279 * sqrt(2) *
                                              sqrt(pi) / a**6.5 + pi / a**6.5)
        nmat[5][5] = 0.595091460158257 * sqrt(
            -0.797884560802865 * pi / a**6.5 + sqrt(2) * sqrt(pi) / a**6.5 +
            0.651485005630987 * sqrt(2) * pi**(3 / 2) / a**(13 / 2))
        nmat[5][6] = 0.728978537063334 * sqrt(0.110616389447279 * sqrt(2) *
                                              sqrt(pi) / a**6.5 + pi / a**6.5)
        nmat[5][7] = 0.918352259114266 * sqrt(-0.393898728870586 * sqrt(2) *
                                              sqrt(pi) / a**6.5 + pi / a**6.5)
        nmat[5][8] = 0.599111531348662 * sqrt(0.766011577631992 * sqrt(2) *
                                              sqrt(pi) / a**6.5 + pi / a**6.5)
        nmat[5][9] = 0.795316386015783 * sqrt(-0.107426926055614 * sqrt(2) *
                                              sqrt(pi) / a**6.5 + pi / a**6.5)
        nmat[5][10] = 0.60668402600181 * sqrt(0.773334266624314 * pi / a**6.5 +
                                              sqrt(2) * sqrt(pi) / a**6.5)
        nmat[6][0] = (1.41181312016106 * 2**(1 / 4) * pi**(1 / 4) / a**3.75)
        nmat[6][1] = 1.00607164027511 * sqrt(0.773334266624317 * pi / a**7.5 +
                                             sqrt(2) * sqrt(pi) / a**7.5)
        nmat[6][2] = (1.41181312016106 * 2**(1 / 4) * pi**(1 / 4) / a**3.75)
        nmat[6][3] = 0.988448326828292 * sqrt(0.786762516257013 * sqrt(2) *
                                              sqrt(pi) / a**7.5 + pi / a**7.5)
        nmat[6][4] = (1.41181312016106 * 2**(1 / 4) * pi**(1 / 4) / a**3.75)
        nmat[6][5] = 1.19202739778739 * sqrt(0.149441272334917 * sqrt(2) *
                                             sqrt(pi) / a**7.5 + pi / a**7.5)
        nmat[6][6] = 1.05480942845325 * sqrt(
            -0.797884560802866 * pi / a**7.5 + sqrt(2) * sqrt(pi) / a**7.5 +
            0.570238516490901 * sqrt(2) * pi**(3 / 2) / a**(15 / 2))
        nmat[6][7] = 1.19202739778739 * sqrt(0.149441272334916 * sqrt(2) *
                                             sqrt(pi) / a**7.5 + pi / a**7.5)
        nmat[6][8] = 1.50956802620997 * sqrt(-0.378634432066319 * sqrt(2) *
                                             sqrt(pi) / a**7.5 + pi / a**7.5)
        nmat[6][9] = 0.988448326828292 * sqrt(0.786762516257013 * sqrt(2) *
                                              sqrt(pi) / a**7.5 + pi / a**7.5)
        nmat[6][10] = 1.31588214563691 * sqrt(-0.102194564360363 * sqrt(2) *
                                              sqrt(pi) / a**7.5 + pi / a**7.5)
        nmat[6][11] = 1.00607164027511 * sqrt(0.773334266624316 * pi / a**7.5 +
                                              sqrt(2) * sqrt(pi) / a**7.5)
        nmat[6][12] = 1.19793499367941 * sqrt(0.135640058151028 * sqrt(2) *
                                              sqrt(pi) / a**7.5 + pi / a**7.5)

        try:
            fac = nmat[ll][m + ll]
        except Exception:
            warnings.warn('recalulating normalization coefficients for l,m=',
                          ll, m)
            x, y, z = symbols('x,y,z')
            overlap = integrate(gto * gto, (x, -oo, oo), (y, -oo, oo),
                                (z, -oo, oo))
            fac = sqrt(overlap)

        return fac

    def gto_spherical(self, ll, m):
        """Construct spherical gto."""
        x, y, z = symbols('x,y,z')
        a = symbols('a', positive=True)
        gto = self.solid_harmonics(ll, m) * exp(-a * (x**2 + y**2 + z**2))
        n = self.gto_spherical_normalization_factor(gto, ll, m)
        # return simplify(gto / n)
        return gto / n

    def solid_harmonics(self, ll, m):
        """Construct solid harmonics S(lm)."""
        slm = 0
        nslm = (2 * factorial(ll + abs(m)) * factorial(ll - abs(m)) /
                2**KroneckerDelta(0, m))
        nslm = sqrt(nslm) / (2**abs(m) * factorial(ll))
        vm = 0 if m >= 0 else 0.5
        limit_t = (ll - abs(m)) // 2
        limit_f = floor(abs(m) / 2 - vm) + vm
        x, y, z = symbols('x,y,z')
        for t in range(0, limit_t + 1):
            for u in range(0, t + 1):
                for v in range(int(2 * vm), int(2 * limit_f) + 1, 2):
                    c = (-1)**(t + v / 2 - vm) * (1 / 4)**t
                    c *= binomial(ll, t)
                    c *= binomial(ll - t, abs(m) + t)
                    c *= binomial(t, u)
                    c *= binomial(abs(m), 2 * v / 2)
                    term = c
                    term *= x**(2 * t + abs(m) - 2 * (u + v / 2))
                    term *= y**(2 * (u + v / 2))
                    term *= z**(ll - 2 * t - abs(m))
                    slm += term
        slm *= nslm
        return slm


# if __name__ == '__main__':

#     water_xyz='''3

# H     0.0000000    0.0000000   -1.0000000
# O     0.0000000    0.0000000    0.0000000
# H     0.0000000   -0.5000000    0.5000000
# '''
#     water_mos= '''
# $lmo     localised orbitals (AO basis,sorted)     format(4d20.14)
# #eigenvalue:  diagonal element of the fock matrix in the LMO basis
#      1  a      eigenvalue=-.20366583972256D+02   nsaos=   18
# 0.48361169516109D-02-.46784691821652D-020.10105833295464D+010.13363234047105D-01
# 0.45697398629220D-01-.52777422131667D-120.15493382281153D-010.93330855086317D-02
# -.43408947000371D-120.15934225318958D-010.22490745663947D-020.15113729510458D-02
# -.91789641264231D-15-.96452413314444D-030.19544334991257D-130.22534514011117D-03
# 0.15361427961054D-01-.46559644435461D-02
#      2  a      eigenvalue=-.11137412811218D+01   nsaos=   18
# -.72517402588826D-01-.13271148008442D-010.12248436437707D+000.30977581676854D+00
# 0.24373467894018D-020.12434897147746D-11-.41165728898151D+000.24628727872491D+00
# 0.10225581426972D-11-.17004030342294D+000.97927959622831D-02-.11082873056250D-02
# 0.29283703822677D-14-.28843836614243D-01-.44107098927025D-13-.23319460974531D-01
# 0.44646331939346D+000.15169671669320D-01
#      3  a      eigenvalue=-.89808620273369D+00   nsaos=   18
# 0.36163544555445D+000.37985781416699D-010.10836696950064D+000.25722397774706D+00
# 0.19223376661289D+000.91431692818339D-12-.10221528994774D+00-.42960343797710D+00
# 0.75197960656087D-12-.12696812038240D+00-.23327866691503D+000.18610366342566D-01
# 0.19181629996258D-140.11012215981963D-01-.32944745104939D-13-.51679167660227D-02
# -.97912727029325D-01-.20750448337413D-01
#      4  a      eigenvalue=-.68134335357927D+00   nsaos=   18
# 0.16200686595441D-010.27073736713997D-01-.96191895896604D-01-.29495912083362D+00
# -.24922571602881D+000.44683070307697D+00-.30640043803085D+00-.11684029553914D+00
# 0.36760018550816D+00-.25527707830386D+00-.90138733785903D-01-.10734658126869D-01
# 0.10337656775074D-020.89518471076240D-02-.15906083072374D-01-.85253567244727D-02
# 0.31399045571442D-020.35694741582882D-02
#      5  a      eigenvalue=-.68134335357661D+00   nsaos=   18
# -.16200686596053D-01-.27073736714177D-010.96191895897711D-010.29495912083370D+00
# 0.24922571602970D+000.44683070307476D+000.30640043803374D+000.11684029553992D+00
# 0.36760018550634D+000.25527707830587D+000.90138733786801D-010.10734658126889D-01
# 0.10337656775020D-02-.89518471076118D-02-.15906083072296D-010.85253567245905D-02
# -.31399045581831D-02-.35694741583109D-02
# $end
# '''

#     import hylleraas as hsp
#     import numpy as np
#     import plotly.graph_objects as go
#     import itertools
#     import time

#     mymol = hsp.Molecule(water_xyz)

#     # for basis_str in 'cc-pVDZ', 'aug-cc-pV6Z':

#     #     start = time.time()
# #     #     mybas = Basis(basis_str, molecule=mymol)
# #     #     end = time.time()
# #     #     print(f'\n\n\n analytic basis functions generated for basis
# {mybas.name} with {mybas.nbas} basis functions generated in {end-start}
# seconds: \n',mybas.basis_functions_symbolic, f'\n\n {dir(mybas)}')

# #     mybas = Basis('def2-SV(P)', molecule=mymol)
# #     # print(f'\n\n\n analytic basis functions generated for basis
# {mybas.name}: \n',mybas.basis_functions_symbolic)

#     # for m in range(-2,3):
#     #     print('m=',m)
#     #     print(mybas.gto_spherical(2,m))
#     # myorb = mybas.read_orbital_tm(water_mos)[4]['coefficients']
#     # print('\n\n\n analytic MO expr:\n', mybas.gen_lcao_symbolic(myorb))
#     # mo = mybas.gen_lcao_numeric(myorb)
#     # print('\n\n\n lambdified representation of mo', mo)

#     # def regular_grid(nx=50, ny=50, nz=50, xmin=-4, xmax=4, ymin=-4,
#      ymax=4, zmin=-4, zmax=4):
#     #     dim_box = nx*ny*nz
#     #     xvals = np.linspace(xmin,xmax,nx)
#     #     yvals = np.linspace(ymin,ymax,ny)
#     #     zvals = np.linspace(zmin,zmax,nz)
#     #     grid= list(itertools.product(xvals,yvals,zvals))
#     #     xvals = []
#     #     yvals =[]
#     #     zvals=[]
#     #     for p in grid:
#     #         xvals.append(p[0])
#     #         yvals.append(p[1])
#     #         zvals.append(p[2])

#     #     return np.array(xvals), np.array(yvals), np.array(zvals)

#     # xvals, yvals, zvals = regular_grid()
#     # start = time.time()
#     # movals = mo(xvals, yvals, zvals)
# #     # end = time.time()
# #     # print(f'\n\n\n{len(movals)} function values computed in
# {end-start} seconds')
# #     # fig = go.Figure(data=go.Isosurface(x=xvals, y=yvals, z=zvals,
# #  value=movals, flatshading=True, colorscale='Blues', isomin=-0.01,
# isomax = 0.01,  opacity=0.5, caps=dict(x_show=False, y_show=False,
#     z_show=False)))

#     # blackaxis= dict(backgroundcolor="rgba(0, 0, 0,0)",gridcolor="black",
# # showbackground=True,title='',showticklabels=False,zerolinecolor="black")
# # #     # fig.update_layout(plot_bgcolor='rgba(1,1,1,1)',
# paper_bgcolor='rgb(1,1,1,1)',scene = dict(xaxis = blackaxis, yaxis=blackaxis,
#  zaxis=blackaxis))

# # #     # fig.show()
