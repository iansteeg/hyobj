from typing import Optional

import numpy as np

try:
    import plotly.graph_objects as go
except ImportError:
    go = None

try:
    import pyvista as pv
except ImportError:
    pv = None


class Visualizer:
    """Methods for visualizing scalar fields."""

    def __init__(self, options: dict = None):
        # TODO set options for visualizer like background color etc.
        # self.options = options
        # try:
        #     import plotly.graph_objects as go
        # except ImportError:
        #     raise ImportError('could not find package plotly')

        # try:
        #     import pyvista as pv
        # except ImportError:
        #     raise ImportError('could not find package pyvista')

        # self.go = go
        # self.pv = pv
        if go is None:
            raise ImportError('could not find package plotly')
        if pv is None:
            raise ImportError('could not find package pyvista')

    def surface(self,
                field: np.ndarray,
                grid: np.ndarray,
                axes: list,
                options: dict = {},
                return_frame: bool = False):
        """Generate a surface plot.

        Parameters
        ----------
        field : np.ndarray
            A 2D array representing the z-values of the surface plot.

        grid : np.ndarray
            A 2D array representing the grid on which the field values are
            defined.

        axes : list
            A list containing the x and y axes data.

        options : dict, optional (default = {})
            A dictionary containing options for the plot. Currently
            supports:
            - 'color': A list of two RGB colors to define the colorscale
                       of the surface. Defaults to the Hylleraas colors
                       ['rgb(255,255,255)', 'rgb(43,56,255)'] if not provided.

        return_frame : bool, optional (default = False)
            If True, returns the trace as a Frame. If False, creates and
            returns a Figure with the trace.

        Returns
        -------
        go.Figure or go.Frame
            If return_frame is True, returns a go.Frame with the surface
            trace. Otherwise, returns a go.Figure with the surface trace and
            customized layout.

        Notes
        -----
        - The grid parameter is currently unused.

        """
        # Extract options
        color = options.get('color', ['rgb(255,255,255)', 'rgb(43,56,255)'])
        colorscale = [[0, color[0]], [1, color[1]]]

        # Create Surface trace
        surface_trace = go.Surface(
            x=axes[0],
            y=axes[1],
            z=field,
            colorscale=colorscale,
        )

        # If return_frame is True, return the trace as a Frame
        if return_frame:
            return go.Frame(data=[surface_trace])

        # Otherwise, create and return a Figure with the trace
        fig = go.Figure(data=[surface_trace])

        blackaxis = dict(
            backgroundcolor='rgba(0, 0, 0,0)',
            gridcolor='black',
            showbackground=True,
            title='',
            showticklabels=False,
            zerolinecolor='black',
        )

        fig.update_layout(
            plot_bgcolor='rgba(1,1,1,1)',
            paper_bgcolor='rgb(1,1,1,1)',
            scene=dict(xaxis=blackaxis, yaxis=blackaxis, zaxis=blackaxis),
        )

        return fig

    def contour(self,
                field: np.ndarray,
                grid: np.ndarray,
                axes: list,
                options: dict = {},
                return_frame: bool = False):
        """Generate a contour plot.

        Parameters
        ----------
        field : np.ndarray
            A 2D array representing the z-values of the contour plot.

        grid : np.ndarray
            A 2D array representing the grid on which the field values are
            defined.

        axes : list
            A list containing the x and y axes data.

        options : dict, optional (default = {})
            A dictionary containing options for the plot. Currently
            supports:
            - 'color': A list of two RGB colors to define the colorscale
                       of the surface. Defaults to the Hylleraas colors
                       ['rgb(255,255,255)', 'rgb(43,56,255)'] if not provided.

        return_frame : bool, optional (default = False)
            If True, returns the trace as a Frame. If False, creates and
            returns a Figure with the trace.

        Returns
        -------
        go.Figure or go.Frame
            If return_frame is True, returns a go.Frame with the surface
            trace. Otherwise, returns a go.Figure with the surface trace and
            customized layout.

        Notes
        -----
        - The grid parameter is currently unused.

        """
        # Extract options
        color = options.get('color', ['rgb(255,255,255)', 'rgb(43,56,255)'])
        colorscale = [[0, color[0]], [1, color[1]]]

        # Create Contour trace
        contour_trace = go.Contour(
            x=axes[0],
            y=axes[1],
            z=field,
            colorscale=colorscale,
        )

        # If return_frame is True, return the trace as a Frame
        if return_frame:
            return go.Frame(data=[contour_trace])

        # Otherwise, create and return a Figure with the trace
        fig = go.Figure(data=[contour_trace])

        blackaxis = dict(
            backgroundcolor='rgba(0, 0, 0,0)',
            gridcolor='black',
            showbackground=True,
            title='',
            showticklabels=False,
            zerolinecolor='black',
        )

        fig.update_layout(
            plot_bgcolor='rgba(1,1,1,1)',
            paper_bgcolor='rgb(1,1,1,1)',
            # Note: may need to adapt this for a 2D contour
            scene=dict(xaxis=blackaxis, yaxis=blackaxis, zaxis=blackaxis),
        )

        return fig

    def isosurface(self,
                   field: np.ndarray,
                   grid: np.ndarray,
                   axes: Optional[list] = None,
                   options={},
                   return_frame=False):
        """Visualize isosurfaces using the marching cubes algorithm.

        The function accepts a 3D field and a grid of the same shape. The
        function then computes and plots two isosurfaces, one for positive
        and one for negative iso values.

        Parameters
        ----------
        field : np.ndarray
            A 3D field represented as a numpy array that the isosurfaces
            will be computed from.

        grid : np.ndarray
            A 3D grid represented as a numpy array that defines the space
            of the field. Should have the same shape as the 'field' array.

        axes: list, optional
            not used.

        options : dict, optional
            A dictionary that can contain various optional parameters:
                'iso': list or float, default 0.05
                    Isovalue to use for creating the isosurfaces.
                    If a list is given, the length of the list must fit the
                    length of color
                'opacity': float, default 0.5
                    The opacity of the isosurfaces, must be between 0
                    (completely transparent) and 1 (completely opaque).
                'color': list of str,
                    default ['rgb(43,56,255)', 'rgb(23,217,255)']
                    A list of two RGB color strings to use for the
                    positive and negative isosurfaces.
        return_frame : bool
            Return a go.Frame rather than a go.Figure (for videos)

        Returns
        -------
        go.Figure
            A Plotly Figure object containing the isosurface plots.

        Notes
        -----
        The marching cubes algorithm is a computer graphics algorithm
        for extracting a polygonal mesh of an isosurface from a
        three-dimensional discrete scalar field (the 'field' parameter).
        The algorithm works by dividing the field into a grid of cubes
        (determined by the 'grid' parameter). For each cube, the algorithm
        determines which vertices are above the isosurface and which are below.
        Then, it draws a set of polygons that divide the cube into two regions,
        one above the isosurface and one below.

        """
        iso = options.get('iso', 0.05)
        opacity = options.get('opacity', 0.5)
        hylleraas_colors = ['rgb(43,56,255)', 'rgb(23,217,255)']
        color = options.get('color', hylleraas_colors)
        intensity = options.get('intensity', None)
        intensitymode = options.get('intensitymode', 'vertex')
        colorscale = options.get('colorscale', None)
        colorbar_title = options.get('colorbar_title', None)

        # Create the UniformGrid object
        # _grid = pv.UniformGrid()  # deprecated since v.0.40.0
        _grid = pv.ImageData()
        # Set the dimensions
        _grid.dimensions = np.array(field.shape)

        # Set the origin (the bottom south-west corner)
        _grid.origin = grid.min(axis=(1, 2, 3))

        # Set the spacing (these are the cell sizes along each axis)
        _grid.spacing = (grid.max(axis=(1, 2, 3)) - grid.min(axis=(1, 2, 3)))
        _grid.spacing = _grid.spacing / np.array(field.shape)

        # Reshape the field to match the grid dimensions
        reshaped_field = field.reshape(_grid.dimensions, order='F')

        # Add the reshaped field data to the grid
        _grid.point_data['values'] = reshaped_field.flatten(order='F')

        # Compute isosurfaces for positive and negative iso
        if isinstance(iso, float):
            isos = [iso, -iso]
        elif isinstance(iso, list):
            isos = iso
            if len(isos) != len(color):
                raise ValueError('length of iso and color must be equal')
        else:
            raise TypeError('iso must be a float or a list of floats')

        traces = []
        for ii, val in enumerate(isos):
            contour = _grid.contour(isosurfaces=[val])
            x, y, z = contour.points.T
            i, j, k = contour.faces.reshape(-1, 4)[:, 1:].T
            _trace = go.Mesh3d(x=x,
                               y=y,
                               z=z,
                               i=i,
                               j=j,
                               k=k,
                               color=color[ii],
                               opacity=opacity,
                               intensity=intensity,
                               intensitymode=intensitymode,
                               colorscale=colorscale,
                               colorbar_title=colorbar_title,
                               name=f'isosurface at {val}')
            traces.append(_trace)

        if return_frame:  # For videos, simply update the data for each point
            return go.Frame(data=traces)

        fig = go.Figure(data=traces)
        # Add labels and title
        scene_axis = dict(showgrid=False,
                          zeroline=False,
                          showticklabels=False,
                          showbackground=False,
                          showline=False,
                          title='')
        fig.update_layout(scene=dict(xaxis=scene_axis,
                                     yaxis=scene_axis,
                                     zaxis=scene_axis),
                          title_x=0.5)
        return fig
