import itertools
from abc import ABC, abstractmethod

import numpy as np


class GridBase(ABC):
    """Base class for grid-type objects."""

    @property
    @abstractmethod
    def shape(self) -> tuple:
        """Return grid dimension."""


class CubicGrid(GridBase):
    """Tools for generating rectangular grids."""

    def __init__(self, axes, options: dict = None):
        options = options or {}
        # self.options = self.set_default_options(options)
        # todo set min/max value dependend on molecule dimension¨

        self.dim = len(axes)
        self.n = [a[2] for a in axes]
        self.axes = [np.linspace(*a) for a in axes]
        self.grid = self.gen_grid(self.axes)

        # xmin = self.options.get('xmin')
        # xmax = self.options.get('xmax')
        # nx = self.options.get('nx')
        # ymin = self.options.get('ymin')
        # ymax = self.options.get('ymax')
        # ny = self.options.get('ny')
        # zmin = self.options.get('zmin')
        # zmax = self.options.get('zmax')
        # nz = self.options.get('nz')

        # self.nx = nx
        # self.ny = ny
        # self.nz = nz

        # self.x = np.linspace(xmin, xmax, nx)
        # self.y = np.linspace(ymin, ymax, ny)
        # self.z = np.linspace(zmin, zmax, nz)

    @property
    def shape(self):
        """Get grid dimensions."""
        return tuple(self.n)
        # return (self.nx, self.ny, self.nz)

    # def set_default_options(self, options: dict):
    #     """Set default options."""
    #     defaults = {
    #         'xmin': -4,
    #         'xmax': 4,
    #         'ymin': -4,
    #         'ymax': 4,
    #         'zmin': -4,
    #         'zmax': 4,
    #         'nx': 50,
    #         'ny': 50,
    #         'nz': 50
    #     }
    #     defaults.update(options)
    #     return defaults

    def gen_grid(self, axes) -> np.ndarray:
        """Generate cubic grid."""
        grid = list(itertools.product(*axes))
        return np.array(grid).T
    # def gen_cubic_grid(self, coords: list) -> np.array:
    #     """Generate a cubic grid of arbitrary dimension."""
    #     grid = list(itertools.product(*coords))
    #     return np.array(grid).T


# class CubicGrid2d(CubicGrid, GridBase):
#     """Tools for 2d cubic grids."""

#     def __init__(self, options: dict = {}):
#         """Initialize 2d grid."""
#         CubicGrid.__init__(self, options)
#         self.grid = self.gen_grid()

#     @property
#     def shape(self):
#         """Get shape of grid."""
#         return (self.nx, self.ny)

#     def gen_grid(self) -> np.array:
#         """Generate grid."""
#         return self.gen_cubic_grid([self.x, self.y])

#     @property
#     def axes(self):
#         """Get axes."""
#         return [self.x, self.y]


# class CubicGrid3d(CubicGrid, GridBase):
#     """Tools for 3d cubic grids."""

#     def __init__(self, options: dict = {}):
#         """Initialize 3d grid."""
#         CubicGrid.__init__(self, options)
#         self.grid = self.gen_grid()

#     @property
#     def shape(self):
#         """Get shape of grid."""
#         return (self.nx, self.ny, self.nz)

#     @property
#     def axes(self):
#         """Get axes of grid."""
#         return [self.x, self.y, self.z]

#     def gen_grid(self) -> np.array:
#         """Generate grid."""
#         return self.gen_cubic_grid([self.x, self.y, self.z])


# if __name__ == '__main__':


#     g = CubicGrid([ (-1, 1, 10), (-1, 1, 5), (-2, 2, 10)])
#     assert g.dim == 3
#     assert len(g.grid.ravel()) == g.dim*10*5*10

#     print(dir(g))
#     print(g.n, g.axes, g.dim)
#     print(len(g.grid.ravel()))
