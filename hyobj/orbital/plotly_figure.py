from functools import wraps
from pathlib import Path
from typing import Callable, List, Optional, Union

import numpy as np

try:
    from plotly.graph_objects import Figure
except ImportError:
    Figure = None

try:
    import io

    from PIL import Image
except ImportError:
    movie_imports = False
else:
    movie_imports = True


class PlotlyFigure:
    """Plotly figure class."""

    @staticmethod
    def save_figure(func: Callable):
        """Save figure to file."""
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            """Wrap function."""
            opt = kwargs.pop('options', {})
            save_figure = opt.pop('save_figure', False)
            fig: Optional[Figure] = func(self, *args,  # type: ignore
                                         options=opt, **kwargs)
            if fig is None:
                return
            if save_figure:
                self.plotly_save_figure(save_figure, fig)
                return
            else:
                fig.show()
        return wrapper

    def plotly_save_figure(self,
                           filename: Union[bool, str, Path],
                           fig: Optional[Figure] = None,
                           **kwargs) -> None:
        """Save figure to file."""
        if fig is None:
            raise ValueError('fig must be specified')
        if not isinstance(filename, (str, Path)):
            filename = 'figure.png'
        fig.write_image(filename, **kwargs)

    def plotly_get_bounds_axis_coords(self, fig: Figure,
                                      coord: str) -> List[float]:
        """Get bounds of a figure."""
        val_min, val_max = 0.0, 0.0
        try:
            val_min, val_max = getattr(fig.layout, coord + 'axis').range
        except (AttributeError, TypeError):
            pass
        return [val_min, val_max]

    def plotly_get_bounds_axis(self, fig: Figure) -> List[float]:
        """Get bounds of a figure."""
        vals = []
        for c in ['x', 'y', 'z']:
            vals.extend(self.plotly_get_bounds_axis_coords(fig, c))
        return vals

    def plotly_get_bounds_data_coords(self, fig: Figure,
                                      coord: str) -> List[float]:
        """Get bounds of a figure."""
        val_min, val_max = 0.0, 0.0
        for plot in fig.data:
            try:
                val_min = min(val_min, np.min(plot[coord]))
                val_max = max(val_max, np.max(plot[coord]))
            except ValueError:
                pass
        return [val_min, val_max]

    def plotly_get_bounds_data(self, fig: Figure) -> List[float]:
        """Get bounds of a figure."""
        vals = []
        for c in ['x', 'y', 'z']:
            vals.extend(self.plotly_get_bounds_data_coords(fig, c))
        return vals

    def plotly_update_frame_bounds(self, fig: Figure, style: str, x_min: float,
                                   x_max: float, y_min: float, y_max: float,
                                   z_min: float, z_max: float) -> Figure:
        """Update frame bounds."""
        fig.update_layout(scene=dict(xaxis=dict(range=[x_min, x_max]),
                                     yaxis=dict(range=[y_min, y_max]),
                                     zaxis=dict(range=[z_min, z_max])))
        if style == 'surface':
            for trace in fig['data']:
                trace['cmin'] = z_min
                trace['cmax'] = z_max
        elif style == 'contour':
            for trace in fig['data']:
                trace['zmin'] = z_min
                trace['zmax'] = z_max
        return fig

    def plotly_to_array(self, fig: Figure) -> np.ndarray:
        """Convert plotly figure to numpy array."""
        if not movie_imports:
            raise ImportError('io and/or PIL is not installed')

        fig_bytes = fig.to_image(format='png')
        buf = io.BytesIO(fig_bytes)
        img = Image.open(buf)
        return np.asarray(img)
