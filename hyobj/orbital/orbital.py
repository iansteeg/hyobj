# import warnings
from pathlib import Path
from typing import Any, List, Optional, Tuple, Union

import numpy as np

try:
    import plotly.graph_objects as go
    fig_type = go.Figure
except ImportError:
    go = None
    fig_type = type(None)
from multimethod import multimethod as singledispatchmethod
from qcelemental import covalentradii

from ..basis import Basis
from ..molecule import Molecule, MoleculeLike
from ..molecule.bonds import Bonds
from ..units import Units
from .grids import CubicGrid
from .interfaces import Interface
from .plotly_figure import PlotlyFigure
from .plotly_movies import PlotlyMovie
from .visualizer import Visualizer

# from sympy import FunctionClass, conjugate, lambdify, symbols


class Orbital(Bonds, PlotlyMovie):
    """Hylleraas Orbital Class.

    Paramters
    ---------
    basis: str or hylleraas.Basis
        basis set name or object
    program: str program
        program name used in orbital generation (important for ordering)
    molecule: hylleraas/daltonproject/pyscf molecule
        molecule object
    orbital: filename, str, or numpy array
        orbital to process
    options: dict
        options

    """

    def __init__(
        self,
        basis: Union[str, Basis],
        program: Optional[str] = 'pyscf',
        molecule: Optional[Any] = None,
        orbital: Optional[Any] = None,
        options: Optional[dict] = None,
    ):
        # try:
        #     import plotly.graph_objects as go
        # except ImportError:
        #     raise ImportError('could not find package plotly')
        options = options or {}
        # self.go = go
        if go is None:
            raise ImportError('could not find package plotly')
        self.options = options
        self.program = program
        self._atoms, self._coordinates = self.extract_mol(molecule)
        self.basis = self.get_basis(basis, atoms=self._atoms)
        self.basis_set = self.basis.name

        self.units = self.options.get('units',
                                      getattr(molecule, 'units',
                                              Units('atomic')))
        self._properties = {'units': self.units}

        # Generate sympy expressions for the AO basis functions.
        # The default ordering of the normalized real-solid harmonical atom
        # oribtals is that of PySCF and Dalton:
        #
        #     1. ordered atom by atom (outer loop)
        #     2. for each atom, angular momenta l first
        #     3. then ml components for each shell batch
        #        (shared set of exponents and angular momentum)
        #     4. and finally contracted functions (inner loop)
        #
        # where the angular ordering is:
        #
        #     (x,y,z), for p-orbitals and ml=(-l, -l+1, ..., l)
        #
        self.basis_functions = self.basis.construct_bf(self._atoms,
                                                       self._coordinates)

        self.nbas = len(self.basis_functions['basis_functions_symbolic'])

        # extract the MO coefficients (based on the format of orbital)
        self.orbitals = self.get_orbitals(orbital)
        self.grid_options: list = []

    def get_orbitals(self,
                     orbital_input: Union[str, Path, np.ndarray, list]
                     ) -> np.ndarray:
        """Generate orbitals."""
        if orbital_input is None:
            raise ValueError('orbital input not found')

        if isinstance(orbital_input, (str, Path)):
            parser = self.options.get('parser', Interface.Plain())
            try:
                path = Path(orbital_input)
                if path.exists():
                    mos = parser.read_orbital_from_file(Path(orbital_input))
                else:
                    mos = parser.read_orbital_from_string(orbital_input)
            except Exception:
                mos = parser.read_orbital_from_string(orbital_input)
        elif isinstance(orbital_input, np.ndarray):
            if len(orbital_input.shape) == 1:
                mos = np.array([orbital_input])
            elif len(orbital_input.shape) == 2:
                ordering = self.options.get('ordering', 'column')
                if ordering.lower() == 'column':
                    mos = orbital_input.T
                elif ordering.lower() == 'row':
                    mos = orbital_input
                else:
                    raise Exception(f'Unrecogniced ordering: {ordering}')
            else:
                raise TypeError('orbital rank > 2')

        elif isinstance(orbital_input, list):
            raise TypeError('list not implemented, did you mean np.ndarray?')

        else:
            mos = [[]]

        return np.array(mos).T

    @singledispatchmethod
    def get_basis(self, basis: Union[Basis, str],
                  atoms: Optional[list] = None) -> Basis:
        """Get basis."""
        raise Exception('error basis input {type(basis)}')

    @get_basis.register
    def _(self, basis: Basis, atoms: Optional[list] = None) -> Basis:
        return basis

    @get_basis.register
    def _(self, basis: str, atoms: Optional[list] = None) -> Basis:
        return Basis(basis, atoms=atoms,
                     options={'parser': self.options.get('parser')})

    def extract_mol(self, molecule: Any) -> Tuple[List[str], np.ndarray]:
        """Extract atoms, coords from molecule object."""
        if isinstance(molecule,
                      MoleculeLike) or (hasattr(molecule, 'atoms')
                                        and hasattr(molecule, 'coordinates')):
            # hsp
            atoms = molecule.atoms
            coords = molecule.coordinates
        elif isinstance(molecule, dict):
            atoms = molecule['atoms']
            coords = molecule['coordinates']
        elif hasattr(molecule, 'elements') and \
                hasattr(molecule, 'coordinates'):
            # daltonproject
            atoms = molecule.elements
            coords = molecule.coordinates
        elif hasattr(molecule, 'elements') and \
                hasattr(molecule, 'atom_coords'):
            # pyscf
            atoms = molecule.elements
            coords = molecule.atom_coords()
        else:
            try:
                mol = Molecule(molecule)
            except Exception:
                atoms = None
                coords = None
            else:
                atoms = mol.atoms
                coords = mol.coordinates
        return atoms, coords

    def gen_mos_on_grid(self, grid: np.ndarray) -> np.ndarray:
        """Generate MOs on grid.

        Parameters
        ----------
        grid : np.ndarray
            Grid on which to evaluate the MOs.

        Returns
        -------
        np.ndarray
            MOs on grid.

        """
        aos = np.array([
            ao(*grid) for ao in self.basis_functions['basis_functions_numeric']
        ])
        mos = np.einsum('i..., ji -> j...', aos, self.orbitals, optimize=True)
        return mos

    def visualize_density(self,
                          density_matrix: Union[np.ndarray, List[np.ndarray]],
                          style: str,
                          options: Optional[dict] = None
                          ) -> Optional[fig_type]:  # type: ignore
        """Visualize the electron density of a molecule.

        Parameters
        ----------
        density_matrix : Union[np.ndarray, list[np.ndarray]]
            the AO density matrix (list: time series) of the molecular system.
        style : str
            The style of the visualization. Accepted values are 'surface',
            'contour', and 'isosurface'.
        options : dict, optional
            Additional options for the visualization. If 'grid' is specified
            in the options, it is used as the grid for the visualization,
            otherwise a cubic grid is generated. Other options include 'plane'
            and 'isovalue', which are used when generating a 2D plane in a 3D
            grid (only used if style is 'surface' or 'contour')

        Returns
        -------
        Optional[go.Figure]
            A figure object that has been displayed or saved. The figure will
            contain a visualization of the electron density in the style
            specified by the 'style' parameter.

        Notes
        -----
        This function generates molecular orbitals on a grid, then constructs
        the electron density from these orbitals using the density matrix. The
        electron density is then visualized using the method specified by the
        'style' parameter.

        """
        options = options or {}
        return self.generate_density(density_matrix, style, options=options)

    @PlotlyFigure.save_figure
    @PlotlyMovie.moviemaker
    def generate_density(self,
                         density_matrix: Union[np.ndarray, List[np.ndarray]],
                         style: str,
                         options: Optional[dict] = None
                         ) -> Optional[fig_type]:  # type: ignore
        """Generate a visualization of the electron density of a molecule.

        Parameters
        ----------
        density_matrix : Union[np.ndarray, list[np.ndarray]]
            The MO density matrix (list: time series) of the molecular system.
        style : str
            The style of the visualization. accepted values are 'surface',
            'contour', and 'isosurface'.
        options : dict, optional
            Additional options for the visualization. If 'grid' is specified
            in the options, it is used as the grid for the visualization,
            otherwise a cubic grid is generated. Other options include 'plane'
            and 'isovalue', which are used when generating a 2D plane in a 3D
            grid (only used if style is 'surface' or 'contour')

        Returns
        -------
        figure : Plotly Figure object
            A figure object that can be displayed or saved. The figure will
            contain a visualization of the electron density in the style
            specified by the 'style' parameter.

        Notes
        -----
        This function generates molecular orbitals on a grid, then constructs
        the electron density from these orbitals using the density matrix. The
        electron density is then visualized using the method specified by the
        'style' parameter.

        """
        options = options or {}
        dim_map = {'surface': 2, 'contour': 2, 'isosurface': 3}
        dim = dim_map[style]
        resolution = options.get('resolution', 10)
        padding = options.get('padding', 4.0)
        log_plot = options.get('log_plot', False)
        camera = options.pop('camera', self.default_camera)
        plot_imag = options.get('plot_imag', False)
        plot_real = options.get('plot_real', False)

        if dim == 2:
            # 2d systems default is xz plane through origin
            axis = self.options.get('axis', [True, False, True])
        else:
            axis = self.options.get('axis', [True, True, True])

        v = Visualizer(options)

        if options.get('grid') is None:
            # set up the grid parameters
            box_min, box_max = self.get_bounding_box(padding)
            params = self.grid_params(box_min, box_max, resolution, axis=axis)
            ngrid = tuple(params[i][2] for i in range(3))
            # set up grid, unless a user-specified grid is provided
            grid = options.get('grid', CubicGrid(params))
            g = grid.grid[axis]
            a = [item for i, item in enumerate(grid.axes) if axis[i]]

        else:
            grid = options.get('grid')
            ngrid = options.get('ngrid')
            try:
                g = grid.grid[axis]
                a = [item for i, item in enumerate(grid.axes) if axis[i]]
            except AttributeError:
                a = None

        x_min = np.min(a[0])
        x_max = np.max(a[0])
        y_min = np.min(a[1])
        y_max = np.max(a[1])

        mos = self.gen_mos_on_grid(grid.grid)
        viewer = getattr(v, style)
        density = density_matrix

        field = np.einsum('i..., ij, j...->...',
                          np.conjugate(mos),
                          density,
                          mos,
                          optimize=True)

        if np.iscomplexobj(field):
            if not any([plot_imag, plot_real]):
                raise ValueError(
                    'plot_imag or plot_real must be set to True')
            if plot_imag*plot_real:
                raise ValueError(
                    'plot_imag and plot_real cannot both be True')
            if plot_imag:
                field = np.imag(field)
            elif plot_real:
                field = np.real(field)

        z_min = 0.0
        z_max = np.max(field)
        if log_plot:
            field = np.log(abs(field))

        s = tuple(arr.shape[0] for arr in a)
        if dim == 2:
            field = field.reshape(s)
        else:
            field = field.reshape(ngrid)
            g = g.reshape((3, ) + ngrid)

        fig = viewer(field=field, grid=g, axes=a, options=options)
        fig.update_layout(scene_camera=camera)

        fig = self.plotly_update_frame_bounds(fig, style, x_min, x_max, y_min,
                                              y_max, z_min, z_max)
        return fig

    def fig_add_molecule(self, fig: fig_type,  # type: ignore
                         options: Optional[dict] = {}):
        """Add a molecular structure to a plotly Figure object.

        The function generates a 3D scatter plot for the atoms and lines for
        the bonds in the molecule.  The size of the atoms and bonds can be
        specified in the options.

        Parameters
        ----------
        fig : plotly.graph_objects.Figure
            The Figure object to which the molecular structure will be added.

        options : dict, optional
            A dictionary that can contain various optional parameters:
                'scale_factor': int, default 20
                    Adjusts the size of the atom markers in the scatter plot.
                'bond_size': int, default 4
                    The width of the lines representing the bonds.
                'molecule_opacity': float, default 0.7
                    The opacity of the molecule from 0 to 1

        Returns
        -------
        None

        """
        scale_factor = options.get('molecule_scale_factor', 1.0)
        molecule_opacity = options.get('molecule_opacity', 0.7)

        # Generate atom sizes based on covalent radii
        atom_sizes = [
            covalentradii.get(atom, missing=4.0) * scale_factor * 20
            for atom in self._atoms
        ]

        # Add the atoms
        marker_dict = dict(size=atom_sizes,
                           color=Molecule.atomic_colors(self._atoms),
                           line=dict(width=2, color='black'),
                           opacity=molecule_opacity,
                           showscale=False)
        fig.add_trace(  # type: ignore
            go.Scatter3d(x=self._coordinates[:, 0],
                         y=self._coordinates[:, 1],
                         z=self._coordinates[:, 2],
                         mode='markers',
                         marker=marker_dict,
                         showlegend=False))

        # Add the bonds
        for bond in self.bonds:
            bond_coords = self._coordinates[[bond[0], bond[1]]]
            fig.add_trace(  # type: ignore
                go.Scatter3d(x=bond_coords[:, 0],
                             y=bond_coords[:, 1],
                             z=bond_coords[:, 2],
                             mode='lines',
                             line=dict(color='gray', width=scale_factor * 4),
                             showlegend=False,
                             opacity=molecule_opacity))

    def get_bounding_box(self, padding: Optional[float] = 0.0
                         ) -> Tuple[tuple, tuple]:
        """Get the bounding box of the system.

        Parameters
        ----------
        padding: float
            Extra space to add to the bounding box. Default is 0.

        Returns
        -------
        tuple
            Minimum and maximum corners of the bounding box

        """
        box_min = tuple(np.min(self._coordinates, axis=0) - padding)
        box_max = tuple(np.max(self._coordinates, axis=0) + padding)
        return box_min, box_max

    def grid_params(self,
                    box_min: tuple,
                    box_max: tuple,
                    resolution: float,
                    axis: Optional[List[bool]] = [True, True, True]
                    ) -> List[tuple]:
        """Generate grid parameters.

        Parameters
        ----------
        box_min: tuple
            Minimum bounding box corner coordinates.
        box_max: tuple
            Maximum bounding box corner coordinates.
        resolution: float
            Grid resolution.
        axis: list[bool]
            List of booleans indicating which axes to include.

        Returns
        -------
        list[tuple]
            List of tuples, each containing min, max coordinates and grid
            resolution for an axis.

        """
        grid_params: List[tuple] = []
        for i in range(3):
            if axis[i]:
                ngrid = int((box_max[i] - box_min[i]) * resolution)
                grid_params.append((box_min[i], box_max[i], ngrid))
            else:
                grid_params.append((0.0, 0.0, 1))
        return grid_params

    @PlotlyFigure.save_figure
    @PlotlyMovie.moviemaker
    def generate_orbital(self, orbital_index: int,
                         options: Optional[dict] = {}, **kwargs
                         ) -> Optional[fig_type]:  # type: ignore
        """Generate the molecular orbital figure.

        The function generates a 3D plot of the isosurface of the molecular
        orbital. It uses the pyvista library for creating the 3D plot and
        Plotly for visualizing it. The size of the orbital is calculated on a
        grid that can be specified in the options.  If no grid is provided, a
        default cubic grid is generated based on the molecule's coordinates,
        with a padding and a resolution that can also be specified in the
        options.

        Parameters
        ----------
        orbital_index : int
            The index of the molecular orbital to be visualized.

        options : dict, optional
            A dictionary that can contain various optional parameters:
                'resolution': int, default 10
                    The resolution of the grid on which the orbital is
                    calculated.
                'padding': float, default 4.0
                    The padding around the molecule's coordinates for the grid.
                'grid': numpy.ndarray
                    User-specified grid on which the orbital is calculated.
                Additional parameters for the 'isosurface' and
                'fig_add_molecule' methods can also be included.

        Returns
        -------
        go.Figure
            A Plotly Figure object containing the isosurface plots.

        """
        resolution = options.get('resolution', 10)
        padding = options.get('padding', 4.0)
        camera = options.pop('camera', self.default_camera)

        v = Visualizer(options)

        # Set up the grid parameters
        box_min, box_max = self.get_bounding_box(padding)
        params = self.grid_params(box_min, box_max, resolution)

        ngrid = tuple(params[i][2] for i in range(3))

        # Set up a rectangular grid, unless a user specified grid is provided
        g = options.get('grid', CubicGrid(params).grid)

        # Evaluate the MO's on the grid
        if self.grid_options != [resolution, padding] or 'grid' in options:
            self.mos_on_grid = self.gen_mos_on_grid(g)
            self.grid_options = [resolution, padding]

        field: np.ndarray = self.mos_on_grid[orbital_index, :].reshape(ngrid)
        g = g.reshape((3, ) + ngrid)

        if np.iscomplexobj(field):
            if options.get('plot_separate', False):
                fig = self.gen_cmplx_iso_colors(v, field, g, options=options)
            else:
                fig = self.gen_cmplx_iso_phase(v, field, g, options=options)
        else:
            fig = v.isosurface(field, g, options=options)

        self.fig_add_molecule(fig, options=options)

        fig.update_layout(scene_camera=camera)  # type: ignore

        return fig

    def gen_cmplx_iso_colors(self, visualizer: Visualizer,
                             field: np.ndarray, g: np.ndarray,
                             options: Optional[dict] = {}
                             ) -> fig_type:  # type: ignore
        """Generate complex isosurface colors."""
        opt = options.copy()
        color = options.get('color')
        iso = options.get('iso')
        if len(color) != len(iso):
            raise ValueError('color and iso must have same length')
        iso_isreal = np.isreal(iso)
        iso_isimag = np.invert(iso_isreal)
        # real part
        opt['iso'] = np.real(np.array(iso))[iso_isreal].tolist()
        opt['color'] = np.array(color)[iso_isreal].tolist()
        fig_real = visualizer.isosurface(np.real(field),
                                         g,
                                         axes=None,
                                         options=opt)

        # imaginary part
        opt['iso'] = np.imag(np.array(iso))[iso_isimag].tolist()
        opt['color'] = np.array(color)[iso_isimag].tolist()

        fig_imag = visualizer.isosurface(np.imag(field),
                                         g,
                                         axes=None,
                                         options=opt)
        return go.Figure(data=fig_real.data + fig_imag.data)

    def gen_cmplx_iso_phase(self,
                            visualizer: Visualizer,
                            field: np.ndarray,
                            g: np.ndarray,
                            options: Optional[dict] = {}
                            ) -> fig_type:  # type: ignore
        """Generate complex isosurface based on phase."""
        intensity = np.angle(field).ravel()
        field = np.abs(field)
        options['intensity'] = intensity
        # note: cyclic color scale
        options['colorscale'] = 'twilight'
        fig = visualizer.isosurface(field, g, axes=None, options=options)
        for f in fig.data:
            f.colorbar.tickmode = 'array'
            f.colorbar.tickvals = [-np.pi, -np.pi / 2, 0, np.pi / 2, np.pi]
            f.colorbar.ticktext = ['-π', '-π/2', '0', 'π/2', 'π']
            f.colorbar.tickfont.family = 'Times New Roman'
            f.colorbar.tickfont.size = 20
            f.colorbar.title = ''
        return fig

    def visualize_orbital(self,
                          orbital_index: Union[int, List[int]],
                          options: Optional[dict] = {},
                          **kwargs
                          ) -> Optional[fig_type]:  # type: ignore
        """Visualize the molecular orbital specified by the given index.

        The function generates a 3D plot of the isosurface of the molecular
        orbital. It uses the pyvista library for creating the 3D plot and
        Plotly for visualizing it. The size of the orbital is calculated on a
        grid that can be specified in the options.  If no grid is provided, a
        default cubic grid is generated based on the molecule's coordinates,
        with a padding and a resolution that can also be specified in the
        options.

        Parameters
        ----------
        orbital_index : int
            The index of the molecular orbital to be visualized.

        options : dict, optional
            A dictionary that can contain various optional parameters:
                'resolution': int, default 10
                    The resolution of the grid on which the orbital is
                    calculated.
                'padding': float, default 4.0
                    The padding around the molecule's coordinates for the grid.
                'grid': numpy.ndarray
                    User-specified grid on which the orbital is calculated.
                Additional parameters for the 'isosurface' and
                'fig_add_molecule' methods can also be included.

        Returns
        -------
        None

        """
        return self.generate_orbital(orbital_index, options=options, **kwargs)

    @singledispatchmethod
    def visualize(self, arg: Union[list, int, np.ndarray], *args,
                  **kwargs) -> Optional[fig_type]:  # type: ignore
        """Visualize either orbitals or electron density.

        The method checks the type of the first argument, and depending on its
        type, it calls either the 'visualize_orbital' or the
        'visualize_density' method.

        Parameters
        ----------
        arg: Union[list, int, np.ndarray]
            The first argument should be either an integer (representing the
            index of the orbital to visualize) or a numpy array (representing
            the electron density to visualize).
        *args :
            A variable-length argument list. The first argument should be
            either an integer (representing the index of the orbital to
            visualize) or a numpy array (representing the electron density
            to visualize).

        **kwargs :
            Keyword arguments passed to the 'visualize_orbital' or
            'visualize_density' method.

        Returns
        -------
        go.Figure
            A Plotly Figure object containing the visualization.

        """
        raise NotImplementedError(f'Cannot visualize {type(arg)}')

    @visualize.register
    def _(self, arg: int, *args, **kwargs):
        return self.visualize_orbital(arg, *args, **kwargs)

    @visualize.register
    def _from_int_list(self, arg: List[int], *args, **kwargs):
        return self.visualize_orbital(arg, *args, **kwargs)

    @visualize.register
    def _(self, arg: np.ndarray, *args, **kwargs):
        return self.visualize_density(arg, *args, **kwargs)

    @visualize.register
    def _from_array_list(self, arg: List[np.ndarray], *args, **kwargs):
        return self.visualize_density(arg, *args, **kwargs)
