from pathlib import Path

import numpy as np


class Interface:
    """Interfaces for orbital stuff."""

    class Plain:
        """Interface to plain text/ascii orbital files."""

        def read_orbital_from_file(self, file):
            """Read orbital from file."""
            path = Path(file)
            if not path.exists():
                raise Exception(f'could not find orbitalfile {path}')
            with open(path, 'rt') as f:
                lines = f.readlines()

            orbital = []
            for line in lines:
                mo = [float(ll) for ll in line.split()]
                orbital.append(mo)

            return orbital

        def read_orbital_from_string(self, orbital):
            """Parse orbital string."""
            return {'coefficients': np.array(orbital)}

    class Numpy:
        """Numpy interface."""

        def read_orbital_from_file(self, file):
            """Read orbital from file."""
            path = Path(file)
            if not path.exists():
                raise Exception(f'could not find orbitalfile {path}')
            return np.load(path)

        def read_orbital_from_string(self, orbital):
            """Parse orbital string."""
            return {'coefficients': np.array(orbital)}

    class Turbomole:
        """Turbomole interface."""

        def read_orbital_from_file(self, file):
            """Read orbital from file."""
            path = Path(file)
            if not path.exists():
                raise Exception(f'could not find orbitalfile {path}')
            with open(path, 'rt') as f:
                orb = f.read()
            return self.read_orbital_from_string(orb)

        def read_orbital_from_string(self, input_str):
            """Parse orbital string."""
            try:
                import fortranformat as ff
            except ImportError:
                raise ImportError('could not find package fortranformat')

            reader = ff.FortranRecordReader('(4d20.14)')
            orbitals = []
            lines = input_str.split('\n')
            for i, line in enumerate(lines):

                if 'nsaos' in line:
                    orbital = {}
                    nsaos = int(line.split('nsaos=')[1])
                    evstr = line.split('eigenvalue=')[1].split()[0]
                    ev = reader.read(evstr)[0]
                    orbital['eigenvalue'] = ev
                    orbital['nsasos'] = nsaos
                    nread = nsaos // 4
                    if nsaos % 4 > 0:
                        nread = nread + 1
                    mo = []
                    for iread in range(i + 1, nread + i + 1):
                        line_read = reader.read(lines[iread])
                        cleaned = [
                            elem for elem in line_read if elem is not None
                        ]
                        mo.extend(cleaned)

                    orbital['coefficients'] = np.array(mo)

                    orbitals.append(orbital)

            return orbitals
