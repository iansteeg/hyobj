# hyobj.units
The units module provides tools for handling units and quantities on the 
hylleraas software platform.

## Quantity
Quantities are objects that store a value, a unit, and a symbol. They are
used to represent physical quantities, such as energy, length, and force.
Quantities are used to define units, which are used to define the
`Units` class.

Note: value should always be stored wrt atomic units.

Example usage:
```python

>>> from hyobj import Quantity
>>> energy = Quantity('hartree', 1.0, 'Eh')
>>> energy
Quantity(unit='hartree', value=1.0, symbol='Eh')
>>> energy.unit
'hartree'
>>> energy.value
1.0
>>> energy.symbol
'Eh'
>>> length = Quantity.from_dict({'unit': 'bohr', 'value': 1.0, 'symbol': 'a0'})
>>> length
Quantity(unit='bohr', value=1.0, symbol='a0')
```

## Units

Units is a flexible object for storing and interacting with quantities. 

Note: value should always be stored wrt atomic units.

Example usage:
```python
>>> from hyobj import Units, Quantity
>>> u = Units(); print(u)
Units({'debug': False, 'style': 'atomic',
       'mass': Quantity(unit='electron_mass', value=1.0, symbol='me'),
       'energy': Quantity(unit='hartree', value=1.0, symbol='Eh'),
       'length': Quantity(unit='bohr', value=1.0, symbol='a0'),
       'time': Quantity(unit='femtoseconds', value=1.0, symbol='fs'),
       'force': Quantity(unit='hartree/bohr', value=1.0, symbol='Eh/a0'), 
       'hessian': Quantity(unit='hartree/bohr**2', value=1.0, symbol='Eh/a0**2')})
>>> u = Units('atomic'); print(u)
Units({'debug': False, 'style': 'atomic',
       'mass': Quantity(unit='electron_mass', value=1.0, symbol='me'),
       'energy': Quantity(unit='hartree', value=1.0, symbol='Eh'),
       'length': Quantity(unit='bohr', value=1.0, symbol='a0'),
       'time': Quantity(unit='femtoseconds', value=1.0, symbol='fs'),
       'force': Quantity(unit='hartree/bohr', value=1.0, symbol='Eh/a0'),
       'hessian': Quantity(unit='hartree/bohr**2', value=1.0, symbol='Eh/a0**2')})
>>> u = Units('real'); print(u)
Units({'debug': False, 'style': 'real',
       'mass': Quantity(unit='grams/mol', value=0.9999999996544214, symbol='g/mol'),
       'energy': Quantity(unit='kcal/mol', value=627.5094740630558, symbol='kcal/mol'),
       'length': Quantity(unit='angstrom', value=0.529177210903, symbol='angstrom'),
       'time': Quantity(unit='ps', value=0.01, symbol='ps'),
       'force': Quantity(unit='kcal/mol/angstrom', value=1185.8210465871337, symbol='kcal/mol/angstrom'),
       'hessian': Quantity(unit='kcal/mol/angstrom**2', value=2240.877010866779, symbol='kcal/mol/angstrom**2')})
>>> u = Units('metal'); print(u)
Units({'debug': False, 'style': 'metal',
       'mass': Quantity(unit='grams/mol', value=0.9999999996544214, symbol='g/mol'),
       'energy': Quantity(unit='eV', value=27.211386245988, symbol='eV'),
       'length': Quantity(unit='angstrom', value=0.529177210903, symbol='angstrom'),
       'time': Quantity(unit='fs', value=1.0, symbol='fs'),
       'force': Quantity(unit='eV/angstrom', value=51.422067476325886, symbol='eV/angstrom'),
       'hessian': Quantity(unit='eV/angstrom**2', value=97.17362429228217, symbol='eV/angstrom**2')})
>>> u.energy = Quantity('hartree', 1.0, 'Eh'); print(u.energy)
Quantity(unit='hartree', value=1.0, symbol='Eh')
>>> v = u.replace(energy={'unit': 'kcal/mol', 'value': 627.509}); print(u.energy, v.energy)
Quantity(unit='hartree', value=1.0, symbol='Eh') Quantity(unit='kcal/mol', value=627.509, symbol='kcal/mol')
>>> u.update(length={'unit': 'pm', 'value': 52.917721092})
Quantity(unit='pm', value=52.917721092, symbol='pm')
>>> u = Units('metal', length=('angstrom', 0.52917721092, 'AA')); print(u.length)
Quantity(unit='angstrom', value=0.52917721092, symbol='AA')
>>> u = Units(dipole=('Debye', 0.3934303, 'D')); print(u)
Units({'debug': False, 'style': None, 'dipole': Quantity(unit='Debye', value=0.3934303, symbol='D')})
>>> u = Units.from_dict({'energy': {'unit': 'hartree', 'value': 1.0, 'symbol': 'hartree'},
                         'dipole': ('Debye', 0.3934303, 'D')}); print(u)
Units({'debug': False, 'style': None,
       'energy': Quantity(unit='hartree', value=1.0, symbol='hartree'),
       'dipole': Quantity(unit='Debye', value=0.3934303, symbol='D')})
````
Produces the following unit systems:


## UnitConverter

Class providing a method self.convert_units(unit_to) that converts 
self from self.units to unit_to.

Example usage:
```python
    from hyobj import UnitConverter, Units, Constants
    class Myclass(UnitConverter):
        def __init__(self, a, b):
            self.energy = a
            self.length = b
    myclass = Myclass(1, 2)
    myclass.units = Units('atomic')
    myclass2 = myclass.convert_units('metal', overwrite=False)
    assert myclass2.energy == 1 * Constants.hartree2ev
````

## convert_units

Function that converts a quantity or Units object from one unit system to
another.

Example Usage:
```python
    from hyobj import Units, convert_units, Constants
    units_old = Units('atomic')
    units_new = Units('real')
    en_a = {'energy': 1.0, 'gradient': 2.0, 'hessian': 3.0,
            'errors': 'string'}
    en_b = convert_units(en_a, units_to=units_new, units_from=units_old,
                         record_success=True)
    assert en_b['energy'] == en_a['energy'] * Constants.hartree2kcalmol
    assert all([x in en_b['converted'] for x in ['energy', 'gradient',
                                                 'hessian']])
```
