# from functools import wraps
from copy import deepcopy
from functools import singledispatch
from typing import Any, Callable, List, Optional, Union

import numpy as np
import pandas as pd

from .units import Units


def convert_units_base(obj: Any,
                       name: str,
                       units_to: Units,
                       units_from: Units,
                       name_mapping: Optional[dict] = None,
                       conversion_function: Optional[Callable] = None,
                       **kwargs
                       ) -> Optional[Any]:
    """Convert units of an object.

    Parameters
    ----------
    obj: Any
        Object to convert units of
    name: str
        Name of object
    units_to: Units
        New units
    units_from: Units
        Old units
    name_mapping: dict
        Mapping of names to units
    conversion_function: Callable
        Function to convert units

    Returns
    -------
    Any
        Object with new units or None if conversion failed

    """
    name_q = name_mapping.get(name, name) if name_mapping else name
    q_from = Units.get_quantity(name_q, units_from)
    q_to = Units.get_quantity(name_q, units_to)
    if q_from is None or q_to is None:
        return None

    if conversion_function:
        return conversion_function(obj, q_from.value, q_to.value)

    fac = q_to.value / q_from.value
    try:
        obj *= fac
    except TypeError:  # pragma: no cover
        try:
            obj = np.array(obj) * fac
        except TypeError as e:
            if isinstance(obj, pd.Series):
                for i, v in enumerate(obj):
                    if isinstance(v, list):
                        val = np.array(v) * fac
                        obj._set_value(i, val.tolist())
                return obj
            raise NotImplementedError(f'Cannot convert units of {obj} with ' +
                                      f'type {type(obj)}') from e
    return obj


@singledispatch
def convert_units(obj: Any,
                  units_to: Union[Units, dict, str],
                  units_from: Optional[Union[Units, dict, str]] = None,
                  overwrite: Optional[bool] = False,
                  name_mapping: Optional[dict] = None,
                  conversion_function: Optional[Callable] = None,
                  record_success: Optional[bool] = False,
                  only: Optional[Union[str, List[str]]] = None,
                  **kwargs
                  ) -> Any:
    """Convert units of a dict.

    Parameters
    ----------
    obj: Any
        Object to convert units of
    units_to: Units
        New units
    units_from: Units
        Old units
    overwrite: bool
        Overwrite object
    name_mapping: dict
        Mapping of names to units
    conversion_function: Callable
        Function to convert units
    record_success: bool
        Record successful conversions
    only: Union[str, List[str]]
        Only convert these quantities

    Returns
    -------
    Any
        Object with new units

    """
    _obj = obj if overwrite else deepcopy(obj)

    if units_from is None:
        units_from = _obj.units

    units_from = Units(units_from)
    units_to = Units(units_to)

    try:
        d = convert_units(_obj.__dict__, units_to=units_to,
                          units_from=units_from, overwrite=True,
                          name_mapping=name_mapping,
                          conversion_function=conversion_function,
                          record_success=record_success,
                          only=only, **kwargs)

    except AttributeError as e:  # pragma: no cover
        raise NotImplementedError(f'Cannot convert units of {obj} with ' +
                                  f'type {type(obj)}') from e
    else:
        _obj.__dict__.update(d)
        if record_success:
            _obj.converted = d['converted']
        _obj.units = units_to

    return _obj


@convert_units.register(dict)
def _(obj: dict,
      units_to: Union[Units, dict, str],
      units_from: Optional[Union[Units, dict, str]] = None,
      overwrite: Optional[bool] = False,
      name_mapping: Optional[dict] = None,
      conversion_function: Optional[Callable] = None,
      record_success: Optional[bool] = False,
      only: Optional[Union[str, List[str]]] = None,
      **kwargs) -> dict:
    """Convert units of a dict.

    Parameters
    ----------
    obj: dict
        Object to convert units of
    units_to: Units
        New units
    units_from: Units
        Old units
    overwrite: bool
        Overwrite object
    name_mapping: dict
        Mapping of names to units
    conversion_function: Callable
        Function to convert units
    record_success: bool
        Record successful conversions
    only: Union[str, List[str]]
        Only convert these quantities

    Returns
    -------
    dict
        Object with new units

    """
    _obj = obj if overwrite else deepcopy(obj)
    units_from = Units(units_from)
    units_to = Units(units_to)
    if record_success:
        sucessfull: List[str] = []
    for k, v in _obj.items():
        if only is not None and k not in only:
            continue
        o = convert_units_base(obj=v, name=k, units_to=units_to,
                               units_from=units_from,
                               name_mapping=name_mapping,
                               conversion_function=conversion_function)
        if o is None:
            continue
        if record_success:
            sucessfull.append(k)
        _obj[k] = o
    if record_success:
        _obj['converted'] = sucessfull
    return _obj


@convert_units.register(list)
def _(obj: list,
      units_to: Union[Units, dict, str],
      units_from: Optional[Union[Units, dict, str]] = None,
      name: Optional[str] = None,
      overwrite: Optional[bool] = False,
      name_mapping: Optional[dict] = None,
      conversion_function: Optional[Callable] = None,
      only: Optional[Union[str, List[str]]] = None,
      **kwargs) -> list:
    """Convert units of a list.

    Parameters
    ----------
    obj: list
        Object to convert units of
    units_to: Units
        New units
    units_from: Units
        Old units
    name: Optional[str] = None,
        Name of object
    overwrite: bool
        Overwrite object
    name_mapping: dict
        Mapping of names to units
    conversion_function: Callable
        Function to convert units
    only: Union[str, List[str]]
        Only convert these quantities

    Returns
    -------
    list
        Object with new units

    """
    if name is None:  # pragma: no cover
        raise ValueError('Name must be provided for list conversion')
    if only is not None and name_mapping.get(name, name) not in only:
        return obj

    _obj = obj if overwrite else deepcopy(obj)
    units_from = Units(units_from)
    units_to = Units(units_to)
    converted_list = []
    for v in _obj:
        o = convert_units_base(obj=v, name=name, units_to=units_to,
                               units_from=units_from,
                               name_mapping=name_mapping,
                               conversion_function=conversion_function)
        val = o if o is not None else v
        converted_list.append(val)
    return converted_list


@convert_units.register(tuple)
def _(obj: tuple,
      units_to: Union[Units, dict, str],
      units_from: Optional[Union[Units, dict, str]] = None,
      name: Optional[str] = None,
      name_mapping: Optional[dict] = None,
      overwrite: Optional[bool] = False,
      conversion_function: Optional[Callable] = None,
      only: Optional[Union[str, List[str]]] = None,
      **kwargs) -> tuple:
    """Convert units of a tuple.

    Parameters
    ----------
    obj: tuple
        Object to convert units of
    units_to: Units
        New units
    units_from: Units
        Old units
    name: Optional[str] = None,
        Name of object
    name_mapping: dict
        Mapping of names to units
    overwrite: bool
        Overwrite object
    conversion_function: Callable
        Function to convert units
    only: Union[str, List[str]]
        Only convert these quantities

    Returns
    -------
    tuple
        Object with new units

    """
    return tuple(convert_units(list(obj), units_to, units_from=units_from,
                               name=name, name_mapping=name_mapping,
                               overwrite=overwrite,
                               conversion_function=conversion_function,
                               only=only,
                               **kwargs))


@convert_units.register(np.ndarray)
def _(obj: np.ndarray,
      units_to: Union[Units, dict, str],
      units_from: Optional[Union[Units, dict, str]] = None,
      name: Optional[str] = None,
      name_mapping: Optional[dict] = None,
      overwrite: Optional[bool] = False,
      conversion_function: Optional[Callable] = None,
      only: Optional[Union[str, List[str]]] = None,
      **kwargs) -> np.ndarray:
    """Convert units of a np.ndarray.

    Parameters
    ----------
    obj: np.ndarray
        Object to convert units of
    units_to: Units
        New units
    units_from: Units
        Old units
    name: Optional[str] = None,
        Name of object
    name_mapping: dict
        Mapping of names to units
    overwrite: bool
        Overwrite object
    conversion_function: Callable
        Function to convert units
    only: Union[str, List[str]]
        Only convert these quantities

    Returns
    -------
    np.ndarray
        Object with new units

    """
    return convert_units(obj.tolist(), units_to, units_from=units_from,
                         name=name, name_mapping=name_mapping,
                         overwrite=overwrite,
                         conversion_function=conversion_function,
                         only=only, **kwargs)


@convert_units.register(pd.DataFrame)
def _(obj: pd.DataFrame,
      units_to: Union[Units, dict, str],
      units_from: Optional[Union[Units, dict, str]] = None,
      name_mapping: Optional[dict] = None,
      overwrite: Optional[bool] = False,
      conversion_function: Optional[Callable] = None,
      only: Optional[Union[str, List[str]]] = None,
      **kwargs) -> pd.DataFrame:
    """Convert units of a pd.DataFrame.

    Parameters
    ----------
    obj: pd.DataFrame
        Object to convert units of
    units_to: Units
        New units
    units_from: Units
        Old units
    name_mapping: dict
        Mapping of names to units
    overwrite: bool
        Overwrite object
    conversion_function: Callable
        Function to convert units
    only: Union[str, List[str]]
        Only convert these quantities

    Returns
    -------
    pd.DataFrame
        Object with new units

    """
    _obj = obj if overwrite else deepcopy(obj)
    # todo, extract units from attrs (?)
    units_from = Units(units_from)
    units_to = Units(units_to)
    for col in _obj.columns:
        if only is not None and col not in only:
            continue
        val = convert_units_base(obj=_obj[col],
                                 name=col, units_to=units_to,
                                 units_from=units_from,
                                 name_mapping=name_mapping,
                                 conversion_function=conversion_function)
        _obj[col] = val if val is not None else _obj[col]
    return _obj


@convert_units.register(pd.Series)
def _(obj: pd.Series,
      units_to: Union[Units, dict, str],
      units_from: Optional[Union[Units, dict, str]] = None,
      name: Optional[str] = None,
      name_mapping: Optional[dict] = None,
      overwrite: Optional[bool] = False,
      conversion_function: Optional[Callable] = None,
      only: Optional[Union[str, List[str]]] = None,
      **kwargs) -> pd.Series:
    """Convert units of a pd.Series.

    Parameters
    ----------
    obj: pd.Series
        Object to convert units of
    units_to: Units
        New units
    units_from: Units
        Old units
    name: Optional[str] = None,
        Name of object
    name_mapping: dict
        Mapping of names to units
    overwrite: bool
        Overwrite object
    conversion_function: Callable
        Function to convert units
    only: Union[str, List[str]]
        Only convert these quantities

    Returns
    -------
    pd.Series
        Object with new units

    """
    if only is not None and name_mapping.get(name, name) not in only:
        return obj
    _obj = obj if overwrite else deepcopy(obj)
    units_from = Units(units_from)
    units_to = Units(units_to)
    _obj[:] = convert_units_base(obj=_obj,
                                 name=name,
                                 units_to=units_to,
                                 units_from=units_from,
                                 name_mapping=name_mapping,
                                 conversion_function=conversion_function)
    return _obj


class UnitConverter:
    """Unit converter."""

    def convert_units(self, *args, **kwargs):
        """Convert units."""
        return convert_units(self, *args, **kwargs)

    def change_units(self, *args, **kwargs):
        """Change units."""
        kwargs['overwrite'] = True
        return self.convert_units(*args, **kwargs)

    def in_units(self, *args, **kwargs):
        """In units."""
        kwargs['overwrite'] = False
        return self.convert_units(*args, **kwargs)
